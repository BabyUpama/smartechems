
package com.smarttech.mittals.updatemodel.powerprocured;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Axisval implements Parcelable {

    @SerializedName("ttl_pc")
    @Expose
    private List<String> ttlPc = null;
    @SerializedName("iex_clr")
    @Expose
    private List<String> iexClr = null;
    @SerializedName("cd")
    @Expose
    private List<String> cd = null;
    @SerializedName("block")
    @Expose
    private List<String> block = null;
    @SerializedName("ttl_rec")
    @Expose
    private List<String> ttlRec = null;
    @SerializedName("clr_quant")
    @Expose
    private List<String> clrQuant = null;
    @SerializedName("bid_qunt")
    @Expose
    private List<String> bidQunt = null;
    @SerializedName("bid_price")
    @Expose
    private List<String> bidPrice = null;

    public List<String> getTtlPc() {
        return ttlPc;
    }

    public void setTtlPc(List<String> ttlPc) {
        this.ttlPc = ttlPc;
    }

    public List<String> getIexClr() {
        return iexClr;
    }

    public void setIexClr(List<String> iexClr) {
        this.iexClr = iexClr;
    }

    public List<String> getCd() {
        return cd;
    }

    public void setCd(List<String> cd) {
        this.cd = cd;
    }

    public List<String> getBlock() {
        return block;
    }

    public void setBlock(List<String> block) {
        this.block = block;
    }

    public List<String> getTtlRec() {
        return ttlRec;
    }

    public void setTtlRec(List<String> ttlRec) {
        this.ttlRec = ttlRec;
    }

    public List<String> getClrQuant() {
        return clrQuant;
    }

    public void setClrQuant(List<String> clrQuant) {
        this.clrQuant = clrQuant;
    }

    public List<String> getBidQunt() {
        return bidQunt;
    }

    public void setBidQunt(List<String> bidQunt) {
        this.bidQunt = bidQunt;
    }

    public List<String> getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(List<String> bidPrice) {
        this.bidPrice = bidPrice;
    }


    protected Axisval(Parcel in) {
        if (in.readByte() == 0x01) {
            ttlPc = new ArrayList<String>();
            in.readList(ttlPc, String.class.getClassLoader());
        } else {
            ttlPc = null;
        }
        if (in.readByte() == 0x01) {
            iexClr = new ArrayList<String>();
            in.readList(iexClr, String.class.getClassLoader());
        } else {
            iexClr = null;
        }
        if (in.readByte() == 0x01) {
            cd = new ArrayList<String>();
            in.readList(cd, String.class.getClassLoader());
        } else {
            cd = null;
        }
        if (in.readByte() == 0x01) {
            block = new ArrayList<String>();
            in.readList(block, String.class.getClassLoader());
        } else {
            block = null;
        }
        if (in.readByte() == 0x01) {
            ttlRec = new ArrayList<String>();
            in.readList(ttlRec, String.class.getClassLoader());
        } else {
            ttlRec = null;
        }
        if (in.readByte() == 0x01) {
            clrQuant = new ArrayList<String>();
            in.readList(clrQuant, String.class.getClassLoader());
        } else {
            clrQuant = null;
        }
        if (in.readByte() == 0x01) {
            bidQunt = new ArrayList<String>();
            in.readList(bidQunt, String.class.getClassLoader());
        } else {
            bidQunt = null;
        }
        if (in.readByte() == 0x01) {
            bidPrice = new ArrayList<String>();
            in.readList(bidPrice, String.class.getClassLoader());
        } else {
            bidPrice = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (ttlPc == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ttlPc);
        }
        if (iexClr == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(iexClr);
        }
        if (cd == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(cd);
        }
        if (block == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(block);
        }
        if (ttlRec == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ttlRec);
        }
        if (clrQuant == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(clrQuant);
        }
        if (bidQunt == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(bidQunt);
        }
        if (bidPrice == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(bidPrice);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Axisval> CREATOR = new Parcelable.Creator<Axisval>() {
        @Override
        public Axisval createFromParcel(Parcel in) {
            return new Axisval(in);
        }

        @Override
        public Axisval[] newArray(int size) {
            return new Axisval[size];
        }
    };
}