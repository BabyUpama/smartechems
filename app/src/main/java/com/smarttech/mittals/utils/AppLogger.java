package com.smarttech.mittals.utils;

import android.content.Context;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import com.smartech.mittals.invetech.R;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by upama on 4/1/17.
 */

public class AppLogger {

    public static void showError(String tag,String msg){
        Log.e(tag, msg);
    }

    public static void showWarning(String tag,String msg){
        Log.w(tag, msg);
    }

    public static void showDebug(String tag,String msg){
        Log.d(tag, msg);
    }

    public static void showToastSmall(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    public static void showToastLarge(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }

    public static void showSnakBarIndefinite(View view,String msg){
        Snackbar snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });

        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

}
