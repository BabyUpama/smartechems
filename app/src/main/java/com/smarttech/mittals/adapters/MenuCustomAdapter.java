package com.smarttech.mittals.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.appbase.AppBaseActivity;
import com.smarttech.mittals.model.login.Menu;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Ashish on 21-07-2016.
 */
public class MenuCustomAdapter extends BaseAdapter {

    private AppBaseActivity context;
    private List<Menu> menuList;

    public MenuCustomAdapter(AppBaseActivity _context, List<Menu> _list) {
        this.context = _context;
        menuList=_list;
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Menu getItem(int i) {
        return menuList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if(view==null){
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.drawer_list_item, null, true);
        }

        Menu menu=getItem(position);

        TextView txtTitle = (TextView) view.findViewById(R.id.title);
        txtTitle.setText(menu.getGraphName());


        ImageView imageView = (ImageView) view.findViewById(R.id.icon);

        Picasso.with(context)
                .load(menu.getAppIcon())
                .into(imageView);

        return view;
    }
}