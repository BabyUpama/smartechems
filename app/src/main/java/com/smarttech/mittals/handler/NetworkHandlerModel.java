package com.smarttech.mittals.handler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.smarttech.mittals.appbase.AppBaseActivity;
import com.smarttech.mittals.utils.AppLogger;

public class NetworkHandlerModel extends AsyncTask<String, Void, Object> {

    private INetworkCallback mActivity = null;
    private ProgressDialog prgDialog = null;
    private int currentId = -1;
    private Class currentClass;
    private Context mcontext;

    public NetworkHandlerModel(Context context, INetworkCallback splsh, Class currentClass, int id) {
        currentId = id;
        mActivity = splsh;
        mcontext = context;
        this.currentClass = currentClass;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        prgDialog = new ProgressDialog(mcontext);
        prgDialog.setMessage("Loading please wait...");
        prgDialog.setCancelable(false);
        prgDialog.show();

    }

    @Override
    protected Object doInBackground(String... params) {

        AppLogger.showError(params[0] + "     ", "data=" + params[1]);

        Object reader = networkCall(params[0], params[1]);
        return reader;
    }

    @Override
    protected void onPostExecute(Object res) {
        if (prgDialog != null) {
            prgDialog.dismiss();
            prgDialog=null;
        }
        mActivity.onUpdateResult(res,currentId);

    }

    private Object networkCall(String currentUrl, String jsonParam) {
        URL url = null;
        HttpURLConnection connection = null;
        Object mObject = null;

        try {
            url = new URL(currentUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("connection", "close");

            if (jsonParam != null) {
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes("data=" + jsonParam);
                dStream.flush();
                dStream.close();
            }
            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream content = connection.getInputStream();
                Reader reader = new InputStreamReader(content);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();

                try {
                    mObject = gson.fromJson(reader, currentClass);
                }catch (Exception ex){
                    AppLogger.showError("ERROORR  ",""+ex.getLocalizedMessage());
                }
                content.close();
            }
        } catch (Exception e) {
            AppLogger.showError("Error in Network Handler Model", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        if(mObject==null){
            AppLogger.showError("TAG",jsonParam.toString()+"");
        }

        return mObject;
    }
}