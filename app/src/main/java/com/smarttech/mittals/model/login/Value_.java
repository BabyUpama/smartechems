
package com.smarttech.mittals.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value_ implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("login_username")
    @Expose
    private String loginUsername;
    @SerializedName("logintype")
    @Expose
    private String logintype;
    @SerializedName("loginsubtype")
    @Expose
    private String loginsubtype;
    @SerializedName("clients")
    @Expose
    private List<Client> clients = null;
    @SerializedName("access_key")
    @Expose
    private String accessKey;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getLogintype() {
        return logintype;
    }

    public void setLogintype(String logintype) {
        this.logintype = logintype;
    }

    public String getLoginsubtype() {
        return loginsubtype;
    }

    public void setLoginsubtype(String loginsubtype) {
        this.loginsubtype = loginsubtype;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    protected Value_(Parcel in) {
        userId = in.readString();
        loginUsername = in.readString();
        logintype = in.readString();
        loginsubtype = in.readString();
        if (in.readByte() == 0x01) {
            clients = new ArrayList<Client>();
            in.readList(clients, Client.class.getClassLoader());
        } else {
            clients = null;
        }
        accessKey = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(loginUsername);
        dest.writeString(logintype);
        dest.writeString(loginsubtype);
        if (clients == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(clients);
        }
        dest.writeString(accessKey);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Value_> CREATOR = new Parcelable.Creator<Value_>() {
        @Override
        public Value_ createFromParcel(Parcel in) {
            return new Value_(in);
        }

        @Override
        public Value_[] newArray(int size) {
            return new Value_[size];
        }
    };

}
