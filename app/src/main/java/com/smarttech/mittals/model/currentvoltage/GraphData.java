
package com.smarttech.mittals.model.currentvoltage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GraphData implements Parcelable {

    @SerializedName("stats")
    @Expose
    private List<Stat> stats = null;
    @SerializedName("axis_data")
    @Expose
    private AxisData axisData;

    public List<Stat> getStats() {
        return stats;
    }

    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }

    public AxisData getAxisData() {
        return axisData;
    }

    public void setAxisData(AxisData axisData) {
        this.axisData = axisData;
    }


    protected GraphData(Parcel in) {
        if (in.readByte() == 0x01) {
            stats = new ArrayList<Stat>();
            in.readList(stats, Stat.class.getClassLoader());
        } else {
            stats = null;
        }
        axisData = (AxisData) in.readValue(AxisData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (stats == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(stats);
        }
        dest.writeValue(axisData);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GraphData> CREATOR = new Parcelable.Creator<GraphData>() {
        @Override
        public GraphData createFromParcel(Parcel in) {
            return new GraphData(in);
        }

        @Override
        public GraphData[] newArray(int size) {
            return new GraphData[size];
        }
    };
}