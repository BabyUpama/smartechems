package com.smarttech.mittals.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartech.mittals.invetech.R;


/**
 * Created by babyu on 17-10-2016.
 */

public class Notification_Fragment extends Fragment {

    View rowView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rowView= inflater.inflate(R.layout.notification_fragment, container, false);
        return rowView;
    }

}
