package com.smarttech.mittals.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.profitability.AxisData;
import com.smarttech.mittals.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by upama on 26/12/16.
 */

public class Profitability_landscape_activity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout llprofitbackarrow;
    private LineChart graph_landscapeprofit;
    private String modeSend;
    private LinearLayout lvprofitabilitydata,lvprofitabilitydataone;
    private TextView tvdatattlpowerdrwn,tvdatatlandedcost,tvdatatoa,tvdatadiscom;
    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] profitGraphBoolean = {true,true,true,true,true,true,true};
    String[] colors_array = {"#ffafaf", "#8000ff", "#00ffff", "#8b008b", "#808080","#008000","#ff0000"};
    private LineData globalLinedata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profitability_landscape_activity);

        AxisData axisval=null;

        Intent in = this.getIntent();
        if (in != null) {
            modeSend = in.getStringExtra("modeSend");
            axisval = in.getParcelableExtra(AppConstants.APP_DATA);
        }

        graph_landscapeprofit=(LineChart) findViewById(R.id.graph_landscapeprofit);

        llprofitbackarrow = (LinearLayout)findViewById(R.id.llprofitbackarrow);
        llprofitbackarrow.setOnClickListener(this);

        lvprofitabilitydata = (LinearLayout) findViewById(R.id.lvprofitabilitydata);
        LinearLayout lvprofitabilityttlpowerdrwn = (LinearLayout) findViewById(R.id.lvprofitabilityttlpowerdrwn);
        TextView tvcolorttlpowerdrwn = (TextView) findViewById(R.id.tvcolorttlpowerdrwn);
        tvdatattlpowerdrwn = (TextView) findViewById(R.id.tvdatattlpowerdrwn);

        LinearLayout lvprofitabilitylandedcost = (LinearLayout) findViewById(R.id.lvprofitabilitylandedcost);
        TextView tvcolortlandedcost = (TextView) findViewById(R.id.tvcolortlandedcost);
        tvdatatlandedcost = (TextView) findViewById(R.id.tvdatatlandedcost);

        LinearLayout lvprofitability = (LinearLayout) findViewById(R.id.lvprofitability);
        TextView tvcolorprofit = (TextView) findViewById(R.id.tvcolorprofit);
        TextView tvdataprofit = (TextView) findViewById(R.id.tvdataprofit);

        LinearLayout lvprofitabilitybreakevn = (LinearLayout) findViewById(R.id.lvprofitabilitybreakevn);
        TextView tvcolorbreakevn = (TextView) findViewById(R.id.tvcolorbreakevn);
        TextView tvdatabreakevn = (TextView) findViewById(R.id.tvdatabreakevn);

        lvprofitabilitydataone = (LinearLayout) findViewById(R.id.lvprofitabilitydataone);
        LinearLayout lvprofitabilitytoa = (LinearLayout) findViewById(R.id.lvprofitabilitytoa);
        TextView tvcolortoa = (TextView) findViewById(R.id.tvcolortoa);
        tvdatatoa = (TextView) findViewById(R.id.tvdatatoa);

        LinearLayout lvprofitabilitydiscom = (LinearLayout) findViewById(R.id.lvprofitabilitydiscom);
        TextView tvcolordiscom = (TextView) findViewById(R.id.tvcolordiscom);
        tvdatadiscom = (TextView) findViewById(R.id.tvdatadiscom);

        LinearLayout lvprofitabilitylcb = (LinearLayout) findViewById(R.id.lvprofitabilitylcb);
        TextView tvcolorlcb = (TextView) findViewById(R.id.tvcolorlcb);
        TextView tvdatalcb = (TextView) findViewById(R.id.tvdatalcb);

        graph_legendLayout.add(0, lvprofitabilityttlpowerdrwn);
        graph_legendLayout.add(1, lvprofitabilitylandedcost);
        graph_legendLayout.add(2, lvprofitability);
        graph_legendLayout.add(3, lvprofitabilitybreakevn);
        graph_legendLayout.add(4, lvprofitabilitytoa);
        graph_legendLayout.add(5, lvprofitabilitydiscom);
        graph_legendLayout.add(6, lvprofitabilitylcb);

        tv_legendarray.add(0, tvcolorttlpowerdrwn);
        tv_legendarray.add(1, tvcolortlandedcost);
        tv_legendarray.add(2, tvcolorprofit);
        tv_legendarray.add(3, tvcolorbreakevn);
        tv_legendarray.add(4, tvcolortoa);
        tv_legendarray.add(5, tvcolordiscom);
        tv_legendarray.add(6, tvcolorlcb);

        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profitGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        updateViewAfterFetch(axisval);
    }

    private void updateViewAfterFetch(AxisData axisval) {

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
        tvdatattlpowerdrwn.setText("Total MeterDetail Drawn ("+ modeSend +")");
        tvdatatlandedcost.setText("Landed Cost of Open Access power(Rs./"+ modeSend +")");
        tvdatatoa.setText("Total Open Access MeterDetail Scheduled ("+ modeSend +")");
        tvdatadiscom.setText("Landed cost of DISCOM (Rs./"+ modeSend +")" );
        updategraph(axisval);
    }

    private void updategraph(AxisData axisval) {
        ArrayList<Entry> ttl_powerEntries = new ArrayList<>();
        ArrayList<Entry> landed_oaEntries = new ArrayList<>();
        ArrayList<Entry> profitEntries = new ArrayList<>();
        ArrayList<Entry> breakeven_priceEntries = new ArrayList<>();
        ArrayList<Entry> ttl_oaEntries = new ArrayList<>();
        ArrayList<Entry> landed_discEntries = new ArrayList<>();
        ArrayList<Entry> landed_bilEntries = new ArrayList<>();


        int blockLen = axisval.getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(axisval.getAxisval().getBlock().get(index));
            ttl_powerEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getTtlPower().get(index))), index));
            landed_oaEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getLandedOa().get(index))), index));
            profitEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getProfit().get(index))), index));
            breakeven_priceEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getBreakevenPrice().get(index))), index));
            ttl_oaEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getTtlOa().get(index))), index));
            landed_discEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getLandedDisc().get(index))), index));
            landed_bilEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getLandedBil().get(index))), index));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, ttl_powerEntries, "", AppConstants.PROFIT_COLOR_TOTALPOWER, true);
        setGraphVales(dataSets, landed_oaEntries, "", AppConstants.PROFIT_COLOR_LANDEDOA, false);
        setGraphVales(dataSets, profitEntries, "", AppConstants.PROFIT_COLOR_PROFIT, true);
        setGraphVales(dataSets, breakeven_priceEntries, "", AppConstants.PROFIT_COLOR_BREAKEVEN, false);
        setGraphVales(dataSets, ttl_oaEntries, " ", AppConstants.PROFIT_COLOR_TOTALOA, false);
        setGraphVales(dataSets, landed_discEntries, "", AppConstants.PROFIT_COLOR_LANDEDDISC, false);
        setGraphVales(dataSets, landed_bilEntries, " ", AppConstants.PROFIT_COLOR_LANDEDBIL, false);

        globalLinedata = new LineData(labels, dataSets);


        globalLinedata.setHighlightEnabled(true);
        graph_landscapeprofit.setData(globalLinedata);
        graph_landscapeprofit.animateY(5000);
        graph_landscapeprofit.setDrawGridBackground(false);
        graph_landscapeprofit.setDescription(axisval.getAxisname());
        graph_landscapeprofit.getLegend().setEnabled(false);
        YAxis axisRight = graph_landscapeprofit.getAxisRight();
        axisRight.setDrawLabels(false);

        YAxis axisLeft = graph_landscapeprofit.getAxisLeft();
        axisLeft.setDrawLabels(false);

        XAxis axis = graph_landscapeprofit.getXAxis();
        axis.setDrawLabels(false);

        lvprofitabilitydata.setVisibility(View.VISIBLE);
        lvprofitabilitydataone.setVisibility(View.VISIBLE);
    }
    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }
    private void updateGraphValues(int position) {

        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < profitGraphBoolean.length; index++) {
            if (index == position) {
                profitGraphBoolean[index] = !profitGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < profitGraphBoolean.length; index++) {
            if (profitGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_landscapeprofit.setData(temp);
        graph_landscapeprofit.animateY(5000);


    }
    private void resetGraphValues() {
        for(int index=0;index<profitGraphBoolean.length;index++){
            profitGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<profitGraphBoolean.length;index++){
            if(profitGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }
    @Override
    public void onClick(View view) {
        if(view == llprofitbackarrow){
            finish();
        }
    }
}
