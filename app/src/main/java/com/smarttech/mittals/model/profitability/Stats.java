
package com.smarttech.mittals.model.profitability;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stats implements Parcelable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("profit")
    @Expose
    private String profit;
    @SerializedName("cumulativeprofit")
    @Expose
    private String cumulativeprofit;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getCumulativeprofit() {
        return cumulativeprofit;
    }

    public void setCumulativeprofit(String cumulativeprofit) {
        this.cumulativeprofit = cumulativeprofit;
    }


    protected Stats(Parcel in) {
        date = in.readString();
        profit = in.readString();
        cumulativeprofit = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(profit);
        dest.writeString(cumulativeprofit);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Stats> CREATOR = new Parcelable.Creator<Stats>() {
        @Override
        public Stats createFromParcel(Parcel in) {
            return new Stats(in);
        }

        @Override
        public Stats[] newArray(int size) {
            return new Stats[size];
        }
    };
}