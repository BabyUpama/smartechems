package com.smarttech.mittals.adapters;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.library.AutoTypeTextView;
import com.smarttech.mittals.updatemodel.dashboard.Stat;

import java.util.List;
import java.util.Random;


/**
 * Created by Ashish Karn on 28-11-2016.
 */

public class HorizontalStatsAdapter extends RecyclerView.Adapter<HorizontalStatsAdapter.MyViewHolder> {

    private List<Stat> statsList;
    private boolean isData=true;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvtitle,tvcolor,tvunit;
        public AutoTypeTextView lblTextDecryption;

        public MyViewHolder(View view) {
            super(view);
            tvtitle = (TextView) view.findViewById(R.id.tvtitle);
            lblTextDecryption = (AutoTypeTextView) view.findViewById(R.id.lblTextDecryption);
            tvcolor = (TextView)view.findViewById(R.id.tvcolor);
            tvunit = (TextView) view.findViewById(R.id.tvunit);

        }
    }

    public HorizontalStatsAdapter(List<Stat> _stats){
        this.statsList = _stats;
        if(statsList== null){
            isData=false;
        }


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_recycler_demand, parent, false);

        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder,int position) {

        if(isData) {
            // position = position % statList.size();
            MyViewHolder myHolder= (MyViewHolder) holder;
            Stat stats=statsList.get(position);
            myHolder.tvtitle.setText(stats.getName());
            myHolder.lblTextDecryption.setText(stats.getVal());
            myHolder.lblTextDecryption.animateDecryption(stats.getVal());
            myHolder.tvunit.setText("("+stats.getUnit()+")");
            Random rnd = new Random();
            int bgcolor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            myHolder.tvcolor.setBackgroundColor(bgcolor);

        }

    }

    @Override
    public int getItemCount() {
        return statsList.size();
    }


}