
package com.smarttech.mittals.model.profitability;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GraphData implements Parcelable {

    @SerializedName("stats")
    @Expose
    private Stats stats;
    @SerializedName("axis_data")
    @Expose
    private AxisData axisData;

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public AxisData getAxisData() {
        return axisData;
    }

    public void setAxisData(AxisData axisData) {
        this.axisData = axisData;
    }


    protected GraphData(Parcel in) {
        stats = (Stats) in.readValue(Stats.class.getClassLoader());
        axisData = (AxisData) in.readValue(AxisData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(stats);
        dest.writeValue(axisData);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GraphData> CREATOR = new Parcelable.Creator<GraphData>() {
        @Override
        public GraphData createFromParcel(Parcel in) {
            return new GraphData(in);
        }

        @Override
        public GraphData[] newArray(int size) {
            return new GraphData[size];
        }
    };
}