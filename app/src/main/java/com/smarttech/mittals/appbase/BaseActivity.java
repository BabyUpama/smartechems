package com.smarttech.mittals.appbase;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by upama on 13/1/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    public abstract void passDataFromNetworkGetHandler(Object object);
}
