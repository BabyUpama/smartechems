package com.smarttech.mittals.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.underdrawl.AxisData;
import com.smarttech.mittals.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by upama on 26/12/16.
 */

public class Underdrawl_landscape_activity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout llunderbackarrow;
    private LineChart graph_landscapeunderdrawl;
    private LineData globalLinedata;
    private LinearLayout lvunderdrawldata;
    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] underdrawlGraphBoolean = {true,true,true,true};
    String[] colors_array = {"#7cb5ec", "#424247", "#90ed7d", "#f7a35c"};
    private TextView tvdataunderdrawl,tvdatattl_power,tvdataoa_power,tvdatattl_oa;
    private String modeSend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.underdrawl_landscape_activity);
        AxisData axisval=null;

        Intent in = this.getIntent();
        if (in != null) {
            modeSend = in.getStringExtra("modeSend");
            axisval = in.getParcelableExtra(AppConstants.APP_DATA);
        }
        llunderbackarrow = (LinearLayout)findViewById(R.id.llunderbackarrow);
        llunderbackarrow.setOnClickListener(this);

        graph_landscapeunderdrawl=(LineChart) findViewById(R.id.graph_landscapeunderdrawl);

        lvunderdrawldata = (LinearLayout) findViewById(R.id.lvunderdrawldata);
        LinearLayout lvunderdrawl = (LinearLayout) findViewById(R.id.lvunderdrawl);
        TextView tvcolorunderdrawl = (TextView) findViewById(R.id.tvcolorunderdrawl);
        tvdataunderdrawl = (TextView) findViewById(R.id.tvdataunderdrawl);

        LinearLayout lvunderdrawlttl_power = (LinearLayout) findViewById(R.id.lvunderdrawlttl_power);
        TextView tvcolorttl_power = (TextView) findViewById(R.id.tvcolorttl_power);
        tvdatattl_power = (TextView) findViewById(R.id.tvdatattl_power);

        LinearLayout lvunderdrawloa_power = (LinearLayout) findViewById(R.id.lvunderdrawloa_power);
        TextView tvcoloroa_power = (TextView) findViewById(R.id.tvcoloroa_power);
        tvdataoa_power = (TextView) findViewById(R.id.tvdataoa_power);

        LinearLayout lvunderdrawlttl_oa = (LinearLayout) findViewById(R.id.lvunderdrawlttl_oa);
        TextView tvcolorttl_oa = (TextView) findViewById(R.id.tvcolorttl_oa);
        tvdatattl_oa = (TextView) findViewById(R.id.tvdatattl_oa);

        graph_legendLayout.add(0, lvunderdrawl);
        graph_legendLayout.add(1, lvunderdrawlttl_power);
        graph_legendLayout.add(2, lvunderdrawloa_power);
        graph_legendLayout.add(3, lvunderdrawlttl_oa);
        tv_legendarray.add(0, tvcolorunderdrawl);
        tv_legendarray.add(1, tvcolorttl_power);
        tv_legendarray.add(2, tvcoloroa_power);
        tv_legendarray.add(3, tvcolorttl_oa);


        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (underdrawlGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        updateViewAfterFetch(axisval);

    }



    private void updateViewAfterFetch(AxisData axisval) {

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }

        tvdataunderdrawl.setText("Underdrawl ("+ modeSend +")");
        tvdatattl_power.setText("Total MeterDetail Consumption ("+ modeSend +")");
        tvdataoa_power.setText("Open Access MeterDetail Recieved at meter periphery ("+ modeSend +")");
        tvdatattl_oa.setText("Total Open Access MeterDetail Scheduled ("+ modeSend +")");
        updategraph(axisval);
    }

    private void updategraph(AxisData axisval) {
        ArrayList<Entry> underdrawlEntries = new ArrayList<>();
        ArrayList<Entry> ttl_powerEntries = new ArrayList<>();
        ArrayList<Entry> oa_powerEntries = new ArrayList<>();
        ArrayList<Entry> ttl_oaEntries = new ArrayList<>();


        int blockLen = axisval.getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(axisval.getAxisval().getBlock().get(index));
            underdrawlEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getUnderdrawl().get(index))), index));
            ttl_powerEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getTtlPower().get(index))), index));
            oa_powerEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getOaPower().get(index))), index));
            ttl_oaEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getTtlOa().get(index))), index));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, underdrawlEntries, "", AppConstants.UNDERDRAWL, false);
        setGraphVales(dataSets, ttl_powerEntries, "", AppConstants.UNDERDRAWL_TTLPOWER, false);
        setGraphVales(dataSets, oa_powerEntries, "", AppConstants.UNDERDRAWL_OAPOWER, false);
        setGraphVales(dataSets, ttl_oaEntries, "", AppConstants.UNDERDRAWL_TTLOA, false);

        globalLinedata = new LineData(labels, dataSets);

        globalLinedata.setHighlightEnabled(true);
        graph_landscapeunderdrawl.setData(globalLinedata);
        graph_landscapeunderdrawl.animateY(5000);
        graph_landscapeunderdrawl.setDrawGridBackground(false);
        graph_landscapeunderdrawl.setDescription(axisval.getAxisname());
        graph_landscapeunderdrawl.getLegend().setEnabled(false);

        lvunderdrawldata.setVisibility(View.VISIBLE);

    }
    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }
    private void updateGraphValues(int position) {
        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < underdrawlGraphBoolean.length; index++) {
            if (index == position) {
                underdrawlGraphBoolean[index] = !underdrawlGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < underdrawlGraphBoolean.length; index++) {
            if (underdrawlGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_landscapeunderdrawl.setData(temp);
        graph_landscapeunderdrawl.animateY(5000);

    }
    private void resetGraphValues() {
        for(int index=0;index<underdrawlGraphBoolean.length;index++){
            underdrawlGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<underdrawlGraphBoolean.length;index++){
            if(underdrawlGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {

        if (view == llunderbackarrow){
            finish();
        }
    }
}