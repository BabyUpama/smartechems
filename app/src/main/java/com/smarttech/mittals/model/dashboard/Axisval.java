
package com.smarttech.mittals.model.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Axisval implements Parcelable {

    @SerializedName("realtime")
    @Expose
    private List<String> realtime = null;
    @SerializedName("cd")
    @Expose
    private List<String> cd = null;
    @SerializedName("min")
    @Expose
    private List<String> min = null;
    @SerializedName("block")
    @Expose
    private List<String> block = null;
    @SerializedName("max")
    @Expose
    private List<String> max = null;
    @SerializedName("avg")
    @Expose
    private List<String> avg = null;

    public List<String> getRealtime() {
        return realtime;
    }

    public void setRealtime(List<String> realtime) {
        this.realtime = realtime;
    }

    public List<String> getCd() {
        return cd;
    }

    public void setCd(List<String> cd) {
        this.cd = cd;
    }

    public List<String> getMin() {
        return min;
    }

    public void setMin(List<String> min) {
        this.min = min;
    }

    public List<String> getBlock() {
        return block;
    }

    public void setBlock(List<String> block) {
        this.block = block;
    }

    public List<String> getMax() {
        return max;
    }

    public void setMax(List<String> max) {
        this.max = max;
    }

    public List<String> getAvg() {
        return avg;
    }

    public void setAvg(List<String> avg) {
        this.avg = avg;
    }


    protected Axisval(Parcel in) {
        if (in.readByte() == 0x01) {
            realtime = new ArrayList<String>();
            in.readList(realtime, String.class.getClassLoader());
        } else {
            realtime = null;
        }
        if (in.readByte() == 0x01) {
            cd = new ArrayList<String>();
            in.readList(cd, String.class.getClassLoader());
        } else {
            cd = null;
        }
        if (in.readByte() == 0x01) {
            min = new ArrayList<String>();
            in.readList(min, String.class.getClassLoader());
        } else {
            min = null;
        }
        if (in.readByte() == 0x01) {
            block = new ArrayList<String>();
            in.readList(block, String.class.getClassLoader());
        } else {
            block = null;
        }
        if (in.readByte() == 0x01) {
            max = new ArrayList<String>();
            in.readList(max, String.class.getClassLoader());
        } else {
            max = null;
        }
        if (in.readByte() == 0x01) {
            avg = new ArrayList<String>();
            in.readList(avg, String.class.getClassLoader());
        } else {
            avg = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (realtime == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(realtime);
        }
        if (cd == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(cd);
        }
        if (min == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(min);
        }
        if (block == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(block);
        }
        if (max == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(max);
        }
        if (avg == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(avg);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Axisval> CREATOR = new Parcelable.Creator<Axisval>() {
        @Override
        public Axisval createFromParcel(Parcel in) {
            return new Axisval(in);
        }

        @Override
        public Axisval[] newArray(int size) {
            return new Axisval[size];
        }
    };
}