package com.smarttech.mittals.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.gkravas.meterview.MeterView;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.MyLayoutManager;
import com.smarttech.mittals.activities.DashboardLandscapeActivity;
import com.smarttech.mittals.adapters.Adapter_DashboardMeters;
import com.smarttech.mittals.adapters.HorizontalStatsAdapter;
import com.smarttech.mittals.adapters.NonSolar_Adapter;
import com.smarttech.mittals.adapters.Solar_Adapter;
import com.smarttech.mittals.adapters.StatsListAdapter;
import com.smarttech.mittals.appbase.AppBaseFragment;
import com.smarttech.mittals.handler.IDataPasser;
import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.updatemodel.dashboard.Dashboarddata;
import com.smarttech.mittals.updatemodel.dashboard.MeterDetail;
import com.smarttech.mittals.updatemodel.dashboard.Meters;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;


public class DashboardTab extends AppBaseFragment implements View.OnClickListener {

    //    View rowView;
    private Solar_Adapter solarAdapter;
    private NonSolar_Adapter nonsolarAdapter;
    HorizontalStatsAdapter horizontalAdptrDemandPowerDrawl;
    StatsListAdapter statslistadapter;

    private CheckBox cbx1, cbx2, cbx3, cbx4;

    private MeterView meterView;

    TextView tvdate;
    private LineData globalLinedata;
    private int fragPosition = -1;
    private LoginData mData = null;
    private RelativeLayout solorrecyclerview;
    private RelativeLayout nonsolorrecyclerview;
    private RecyclerView recyclrview_iex;
    private RecyclerView recyclrview_Demand;
    private LineChart graph_dashboard;

    private LinearLayout lvdashboarddata;

    private ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] dashboardGraphBoolean = {true, true, true, true, true};
    String[] colors_array = {"#7cb5ec", "#424247", "#90ed7d", "#f7a35c", "#8085e9"};
    private RecyclerView mRecyclerView = null;
    private RecyclerView nonmRecyclerView = null;
    private Dashboarddata data = null;
    private String currentHospitalId = null;
    private Button viewGraph_dashboard;

//    private Meters currentMeters;

    private RecyclerView rv_dashboardmeters;
    private LinearLayoutManager mLayoutManager;
    private Adapter_DashboardMeters mAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        currentHospitalId = args.getString(AppConstants.DATA_CURRENTID);
        fragPosition = args.getInt(AppConstants.DATA_POSITION);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }

        if (currentHospitalId == null) {
            currentHospitalId = mData.getValue().getValue().getUserId();
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rowView = inflater.inflate(R.layout.fragment_dashboard_tab, container, false);

        tvdate = (TextView) rowView.findViewById(R.id.tvdate);

//        cbx1 = (CheckBox) rowView.findViewById(R.id.cbx1);
//        cbx2 = (CheckBox) rowView.findViewById(R.id.cbx2);
//        cbx3 = (CheckBox) rowView.findViewById(R.id.cbx3);
//        cbx4 = (CheckBox) rowView.findViewById(R.id.cbx4);

        viewGraph_dashboard = (Button) rowView.findViewById(R.id.viewGraph_dashboard);
        viewGraph_dashboard.setOnClickListener(this);

//        cbx1.setOnClickListener(this);
//        cbx2.setOnClickListener(this);
//        cbx3.setOnClickListener(this);
//        cbx4.setOnClickListener(this);

        solorrecyclerview = (RelativeLayout) rowView.findViewById(R.id.solorrecyclerview);
        nonsolorrecyclerview = (RelativeLayout) rowView.findViewById(R.id.nonsolorrecyclerview);

        recyclrview_iex = (RecyclerView) rowView.findViewById(R.id.recyclrview_iex_ui_etc);
        recyclrview_Demand = (RecyclerView) rowView.findViewById(R.id.recyclrview_Demand);

        meterView = (MeterView) rowView.findViewById(R.id.meter_view);

        lvdashboarddata = (LinearLayout) rowView.findViewById(R.id.lvdashboarddata);
        LinearLayout lvdashboardreal = (LinearLayout) rowView.findViewById(R.id.lvdashboardreal);
        TextView tvcolorreal = (TextView) rowView.findViewById(R.id.tvcolorreal);
        TextView tvdatareal = (TextView) rowView.findViewById(R.id.tvdatareal);

        LinearLayout lvdashboardcd = (LinearLayout) rowView.findViewById(R.id.lvdashboardcd);
        TextView tvcolorcd = (TextView) rowView.findViewById(R.id.tvcolorcd);
        TextView tvdatacd = (TextView) rowView.findViewById(R.id.tvdatacd);

        LinearLayout lvdashboardmin = (LinearLayout) rowView.findViewById(R.id.lvdashboardmin);
        TextView tvcolormin = (TextView) rowView.findViewById(R.id.tvcolormin);
        TextView tvdatamin = (TextView) rowView.findViewById(R.id.tvdatamin);

        LinearLayout lvdashboardmax = (LinearLayout) rowView.findViewById(R.id.lvdashboardmax);
        TextView tvcolormax = (TextView) rowView.findViewById(R.id.tvcolormax);
        TextView tvdatamax = (TextView) rowView.findViewById(R.id.tvdatamax);

        LinearLayout lvdashboardavg = (LinearLayout) rowView.findViewById(R.id.lvdashboardavg);
        TextView tvcoloravg = (TextView) rowView.findViewById(R.id.tvcoloravg);
        TextView tvdataavg = (TextView) rowView.findViewById(R.id.tvdataavg);

        graph_dashboard = (LineChart) rowView.findViewById(R.id.graph_dashboard);

        rv_dashboardmeters = (RecyclerView) rowView.findViewById(R.id.rv_dashboardmeters);

        graph_legendLayout.add(0, lvdashboardreal);
        graph_legendLayout.add(1, lvdashboardcd);
        graph_legendLayout.add(2, lvdashboardmin);
        graph_legendLayout.add(3, lvdashboardmax);
        graph_legendLayout.add(4, lvdashboardavg);
        tv_legendarray.add(0, tvcolorreal);
        tv_legendarray.add(1, tvcolorcd);
        tv_legendarray.add(2, tvcolormin);
        tv_legendarray.add(3, tvcolormax);
        tv_legendarray.add(4, tvcoloravg);


        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dashboardGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        setDashBoard(null);

//        recyclerListCheckbox();

        return rowView;
    }
//
//    private void recyclerListCheckbox() {
//        tv_state = new ArrayList<>(Arrays.asList("54875625", "9853244147",
//                "4758532145", "65252545", "653221145", "32323214",
//                "457554585254", "65247412"));
//        // Calling the RecyclerView
//
//        rv_realtime.setHasFixedSize(true);
//        // The number of Columns
//        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//        rv_realtime.setLayoutManager(mLayoutManager);
//
//        mAdapter = new Adapter_DashboardMeters(getActivity(), tv_state);
//        rv_realtime.setAdapter(mAdapter);
//
//
//    }

//    private void recyclerListCheckbox() {
//
//        tv_state = new ArrayList<>(Arrays.asList("54875625", "9853244147",
//                "4758532145", "65252545", "653221145", "32323214",
//                "457554585254", "65247412"));
//
//        // Calling the RecyclerView
//        rv_realtime = (RecyclerView) rowView.findViewById(R.id.rv_real_time_tab);
//        rv_realtime.setHasFixedSize(true);
//
//        // The number of Columns
//        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//        rv_realtime.setLayoutManager(mLayoutManager);
//
//        mAdapter = new Adapter_DashboardMeters(getActivity(), tv_state);
//        rv_realtime.setAdapter(mAdapter);
//
//    }

    private void setDashBoard(JSONObject object) {

        JSONObject param = new JSONObject();
        try {
            param.put("graph_id", mData.getValue().getMenu().get(fragPosition).getGraphId());
            param.put("user_id", currentHospitalId);
            param.put("access_key", mData.getValue().getValue().getAccessKey());
            if (object == null) {
                param.put("meters_id", "");
            } else {
                param.put("meters_id", object);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = AppConstants.URL_BASE + AppConstants.SUBURL_DASHBOARD;
        str[1] = param.toString();

        new NetworkHandlerModel(getActivity(), callback, Dashboarddata.class, 1).execute(str);

    }

    private void setDashBoard() throws JSONException {

        JSONObject paramObject = new JSONObject();

        Meters currentMeters=data.getMeters();

        if (currentMeters != null) {
            JSONArray powerArray = new JSONArray();

            if (currentMeters.getPower() != null && !currentMeters.getPower().isEmpty()) {

                List<MeterDetail> pList = currentMeters.getPower();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId().replaceAll("&", ""));
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }


                    powerArray.put(tmpObject);
                }
                paramObject.put("power", powerArray);
            }

            if (currentMeters.getSolar() != null && !currentMeters.getSolar().isEmpty()) {
                List<MeterDetail> pList = currentMeters.getSolar();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();
                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId().replaceAll("&", ""));
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }

                    powerArray.put(tmpObject);
                }
                paramObject.put("solar", powerArray);
            }

            setDashBoard(paramObject);

        } else {
            AppLogger.showToastSmall(getActivity().getApplicationContext(), "Current Meter is NULL.");
        }
    }

    private INetworkCallback callback = new INetworkCallback() {
        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getActivity(), "Some Error Occurred");
                return;
            }

           /* Dashboarddata tmpData=(Dashboarddata) obj;
            if(!tmpData.isType()){
                AppLogger.showToastLarge(getActivity(),"Some Error Occurred.");
                return;
            }*/

            data = (Dashboarddata) obj;
            updateViewAfterFetch(data);

            setCheckBox(data);
        }

    };

    private void setCheckBox(Dashboarddata data) {

//        CheckBox[] checkArr = {cbx1, cbx2, cbx3, cbx4};
//
//        Meters meters = data.getMeters();
//        if (meters.getPower() != null) {
//            List<MeterDetail> meterDetail = meters.getPower();
//            int siz = Math.min(meterDetail.size(), checkArr.length);
//
//            for (int i = 0; i < siz; i++) {
//
//                MeterDetail mtr = meterDetail.get(i);
//
//                if (mtr.getChecked()) {
//                    checkArr[i].setText(mtr.getMeterId());
//                    checkArr[i].setBackgroundColor(AppConstants.CHECK_COLOR);
//                    checkArr[i].setTextColor(Color.WHITE);
//                    checkArr[i].setChecked(true);
//                    checkArr[i].setVisibility(View.VISIBLE);
//                } else if (!mtr.getChecked()) {
//                    checkArr[i].setBackgroundColor(Color.WHITE);
//                    //checkArr[i].setTextColor(Color.parseColor("#737272"));
//                    checkArr[i].setText(mtr.getMeterId());
//                    checkArr[i].setChecked(false);
//                    checkArr[i].setVisibility(View.VISIBLE);
//                } else {
//                    checkArr[i].setVisibility(View.GONE);
//                }
//            }
//
//        }

        rv_dashboardmeters.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_dashboardmeters.setLayoutManager(mLayoutManager);

        if(!data.getMeters().getPower().isEmpty()){
            mAdapter = new Adapter_DashboardMeters(dataPasser, data.getMeters().getPower());
            rv_dashboardmeters.setAdapter(mAdapter);
        }
        else if(!data.getMeters().getSolar().isEmpty()){
            mAdapter = new Adapter_DashboardMeters(dataPasser, data.getMeters().getSolar());
            rv_dashboardmeters.setAdapter(mAdapter);
        }


//        rv_realtime.addOnItemTouchListener(
//                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//
//                        CheckedTextView ctv = (CheckedTextView)view;
//                        if(ctv.isChecked()){
//                            try {
//                                setDashBoard();
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                })
//        );


    }


//        List<Meter> variable = data.getMeters();


    private void updateViewAfterFetch(Dashboarddata data) {

        tvdate.setText(data.getRec().getDate());
        tvdate.setVisibility(View.VISIBLE);

        mRecyclerView = new RecyclerView(getActivity());
        mRecyclerView.setHasFixedSize(true);
        solarAdapter = new Solar_Adapter(data.getRec().getSolar());
        mRecyclerView.setAdapter(solarAdapter);
        mRecyclerView.setOverScrollMode(RecyclerView.SCROLL_STATE_SETTLING);
        mLayoutManager = new MyLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.smoothScrollToPosition(Integer.MAX_VALUE);
        if (solorrecyclerview.getChildCount() > 0) {
            solorrecyclerview.removeAllViews();
        }
        solorrecyclerview.addView(mRecyclerView);
        solarautoScroll();


        nonmRecyclerView = new RecyclerView(getActivity());
        nonmRecyclerView.setHasFixedSize(true);
        nonsolarAdapter = new NonSolar_Adapter(data.getRec().getNonSolar());
        nonmRecyclerView.setAdapter(nonsolarAdapter);
        nonmRecyclerView.setOverScrollMode(RecyclerView.SCROLL_STATE_SETTLING);
        mLayoutManager = new MyLayoutManager(getActivity());
        nonmRecyclerView.setLayoutManager(mLayoutManager);
        nonmRecyclerView.smoothScrollToPosition(Integer.MAX_VALUE);

        if (nonsolorrecyclerview.getChildCount() > 0) {
            nonsolorrecyclerview.removeAllViews();
        }
        nonsolorrecyclerview.addView(nonmRecyclerView);
        nonsolarautoScroll();

        statslistadapter = new StatsListAdapter(data.getStats());
        LinearLayoutManager horizLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclrview_iex.setLayoutManager(horizLayoutManagaer);
        recyclrview_iex.setAdapter(statslistadapter);

        horizontalAdptrDemandPowerDrawl = new HorizontalStatsAdapter(data.getGraphData().getStats());
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclrview_Demand.setLayoutManager(horizontalLayoutManagaer);
        recyclrview_Demand.setAdapter(horizontalAdptrDemandPowerDrawl);

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }

        updategraph(data);
//        currentMeters = data.getMeters();
    }


    private void solarautoScroll() {
        final int speedScroll = 1000;
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            int count = 0;

            @Override
            public void run() {
                if (count == solarAdapter.getItemCount())
                    count = 0;
                if (count < solarAdapter.getItemCount()) {
                    mRecyclerView.smoothScrollToPosition(++count);
                    handler.postDelayed(this, speedScroll);
                }

            }
        };
        handler.postDelayed(runnable, speedScroll);
    }

    private void nonsolarautoScroll() {
        final int speedScroll = 1000;
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            int count = 0;

            @Override
            public void run() {
                if (count == nonsolarAdapter.getItemCount())
                    count = 0;
                if (count < nonsolarAdapter.getItemCount()) {
                    nonmRecyclerView.smoothScrollToPosition(++count);
                    handler.postDelayed(this, speedScroll);
                }

            }
        };
        handler.postDelayed(runnable, speedScroll);

    }


    private void updategraph(Dashboarddata data) {
        ArrayList<Entry> totalconsumptionEntries = new ArrayList<>();
        ArrayList<Entry> cdEntries = new ArrayList<>();
        ArrayList<Entry> minEntries = new ArrayList<>();
        ArrayList<Entry> maxEntries = new ArrayList<>();
        ArrayList<Entry> avgEntries = new ArrayList<>();


        int blockLen = data.getGraphData().getAxisData().getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(data.getGraphData().getAxisData().getAxisval().getBlock().get(index));
            totalconsumptionEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getRealtime().get(index))), index));
            cdEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getCd().get(index))), index));
            minEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getMin().get(index))), index));
            maxEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getMax().get(index))), index));
            avgEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getAvg().get(index))), index));


        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, totalconsumptionEntries, "", AppConstants.DASHBOARD_COLOR_REALTIME, true);
        setGraphVales(dataSets, cdEntries, "", AppConstants.DASHBOARD_COLOR_CD, false);
        setGraphVales(dataSets, minEntries, "", AppConstants.DASHBOARD_COLOR_MIN, false);
        setGraphVales(dataSets, maxEntries, "", AppConstants.DASHBOARD_COLOR_MAX, false);
        setGraphVales(dataSets, avgEntries, " ", AppConstants.DASHBOARD_COLOR_AVG, false);

        globalLinedata = new LineData(labels, dataSets);

        globalLinedata.setHighlightEnabled(true);
        graph_dashboard.setData(globalLinedata);
        graph_dashboard.animateY(5000);
        graph_dashboard.setDrawGridBackground(false);
        graph_dashboard.setDescription(data.getGraphData().getAxisData().getAxisname());
        graph_dashboard.getLegend().setEnabled(false);
        YAxis axisRight = graph_dashboard.getAxisRight();
        axisRight.setDrawLabels(false);

        /*YAxis axisLeft = graph_dashboard.getAxisLeft();
        axisLeft.setDrawLabels(false);

        XAxis axis = graph_dashboard.getXAxis();
        axis.setDrawLabels(false);*/

        lvdashboarddata.setVisibility(View.VISIBLE);

    }

    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(Color.parseColor("#000000"));
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }

    private void updateGraphValues(int position) {

        LineData tempLineData = globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < dashboardGraphBoolean.length; index++) {
            if (index == position) {
                dashboardGraphBoolean[index] = !dashboardGraphBoolean[index];
                break;
            }
        }

        if (isEmpty()) {
            resetGraphValues();
        }

        for (int index = 0; index < dashboardGraphBoolean.length; index++) {
            if (dashboardGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_dashboard.setData(temp);
        graph_dashboard.animateY(5000);
    }

    private void resetGraphValues() {
        for (int index = 0; index < dashboardGraphBoolean.length; index++) {
            dashboardGraphBoolean[index] = true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }

    private boolean isEmpty() {
        int counter = 0;
        for (int index = 0; index < dashboardGraphBoolean.length; index++) {
            if (dashboardGraphBoolean[index]) {
                counter = counter + 1;
            }
        }
        if (counter > 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        final float min = 0;
        final float max = 54;
        final Random random = new Random();

        Observable.interval(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<Long, Float>() {
                    @Override
                    public Float call(Long aLong) {
                        return min + (random.nextFloat() * (max - min));
                    }
                })
                .subscribe(new Action1<Float>() {
                    @Override
                    public void call(Float value) {
                        meterView.setValue(value);
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();


    }


    @Override
    public void onClick(View view) {
//        if (view == rv_realtime) {
//            try {
//                setDashBoard();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        if (view == cbx2) {
//            if (cbx2.isChecked()) {
////                List<Meter> meters=data.getMeters();
////                meters.get(1).setChecked(true);
////
////                setCheckBox(data);
//
//                try {
//                    setDashBoard();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        if (view == cbx3) {
//            if (cbx3.isChecked()) {
//
////                List<Meter> meters=data.getMeters();
////                meters.get(1).setChecked(true);
////
////                setCheckBox(data);
//
//                try {
//                    setDashBoard();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        if (view == cbx4) {
//            if (cbx4.isChecked()) {
//                try {
//                    setDashBoard();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
        if (view == viewGraph_dashboard) {
            Intent i = new Intent(getActivity(), DashboardLandscapeActivity.class);

            i.putExtra(AppConstants.APP_DATA, data.getGraphData().getAxisData());
            startActivity(i);

        }
    }

    private IDataPasser dataPasser=new IDataPasser() {

        @Override
        public void callApi(Object obj) {
            try {
                setDashBoard();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}






