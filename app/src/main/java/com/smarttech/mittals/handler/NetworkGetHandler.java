package com.smarttech.mittals.handler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.smarttech.mittals.appbase.BaseActivity;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by babyu on 29-07-2016.
 */
public class NetworkGetHandler extends AsyncTask<String,Void,String> {

    private String currentUrl="";
    private Context mContext=null;
    private BaseActivity mactivity=null;

    public NetworkGetHandler(Context context, String url, BaseActivity activity){
        currentUrl=url;
        this.mContext=context;
        this.mactivity=activity;
    }
    private ProgressDialog dialog;
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        dialog = new ProgressDialog(mContext);
        dialog.setMessage("Loading please wait...");
        dialog.setCancelable(false);
        //dialog.show();
    }
    @Override
    protected String doInBackground(String... params) {
        String mResult=networkCall();
        return mResult;
    }
    @Override
    protected void onPostExecute(String str) {
        Object dataObject= null;
        /*try {
            if(str!=null){
                dataObject = new JsonParserHandler().getDataFromJson(str,1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        this.mactivity.passDataFromNetworkGetHandler(str);
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        super.onPostExecute(str);
    }
    private String networkCall(){
        String str="";

        URL url=null;
        HttpURLConnection connection=null;

        try {
            url=new URL(currentUrl);

            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            dStream.writeBytes(toString());
            dStream.flush();
            dStream.close();

            //int responseCode = connection.getResponseCode();

            //Log.e("response code",""+responseCode);
            Log.e("URL",""+connection.getURL().getAuthority());

            int HttpResult = connection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                }
                br.close();

                str = responseOutput.toString();
            }else {
                System.out.println(connection.getResponseMessage());
            }

        } catch (Exception e) {
            Log.e("Error in async",e.getLocalizedMessage());
            e.printStackTrace();
        }  finally {
            if(connection!=null){
                connection.disconnect();
            }
        }
        return str;
    }
}
