
package com.smarttech.mittals.updatemodel.currentvoltage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Axisval implements Parcelable {

    @SerializedName("freq")
    @Expose
    private List<String> freq = null;
    @SerializedName("pf")
    @Expose
    private List<String> pf = null;
    @SerializedName("phase_a")
    @Expose
    private List<String> phaseA = null;
    @SerializedName("phase_b")
    @Expose
    private List<String> phaseB = null;
    @SerializedName("phase_c")
    @Expose
    private List<String> phaseC = null;
    @SerializedName("block")
    @Expose
    private List<String> block = null;

    public List<String> getFreq() {
        return freq;
    }

    public void setFreq(List<String> freq) {
        this.freq = freq;
    }

    public List<String> getPf() {
        return pf;
    }

    public void setPf(List<String> pf) {
        this.pf = pf;
    }

    public List<String> getPhaseA() {
        return phaseA;
    }

    public void setPhaseA(List<String> phaseA) {
        this.phaseA = phaseA;
    }

    public List<String> getPhaseB() {
        return phaseB;
    }

    public void setPhaseB(List<String> phaseB) {
        this.phaseB = phaseB;
    }

    public List<String> getPhaseC() {
        return phaseC;
    }

    public void setPhaseC(List<String> phaseC) {
        this.phaseC = phaseC;
    }

    public List<String> getBlock() {
        return block;
    }

    public void setBlock(List<String> block) {
        this.block = block;
    }


    protected Axisval(Parcel in) {
        if (in.readByte() == 0x01) {
            freq = new ArrayList<String>();
            in.readList(freq, String.class.getClassLoader());
        } else {
            freq = null;
        }
        if (in.readByte() == 0x01) {
            pf = new ArrayList<String>();
            in.readList(pf, String.class.getClassLoader());
        } else {
            pf = null;
        }
        if (in.readByte() == 0x01) {
            phaseA = new ArrayList<String>();
            in.readList(phaseA, String.class.getClassLoader());
        } else {
            phaseA = null;
        }
        if (in.readByte() == 0x01) {
            phaseB = new ArrayList<String>();
            in.readList(phaseB, String.class.getClassLoader());
        } else {
            phaseB = null;
        }
        if (in.readByte() == 0x01) {
            phaseC = new ArrayList<String>();
            in.readList(phaseC, String.class.getClassLoader());
        } else {
            phaseC = null;
        }
        if (in.readByte() == 0x01) {
            block = new ArrayList<String>();
            in.readList(block, String.class.getClassLoader());
        } else {
            block = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (freq == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(freq);
        }
        if (pf == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(pf);
        }
        if (phaseA == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(phaseA);
        }
        if (phaseB == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(phaseB);
        }
        if (phaseC == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(phaseC);
        }
        if (block == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(block);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Axisval> CREATOR = new Parcelable.Creator<Axisval>() {
        @Override
        public Axisval createFromParcel(Parcel in) {
            return new Axisval(in);
        }

        @Override
        public Axisval[] newArray(int size) {
            return new Axisval[size];
        }
    };
}