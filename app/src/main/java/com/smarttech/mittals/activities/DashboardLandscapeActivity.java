package com.smarttech.mittals.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.dashboard.AxisData;
import com.smarttech.mittals.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by upama on 11/4/17.
 */

public class DashboardLandscapeActivity extends AppCompatActivity implements View.OnClickListener{

    LinearLayout lldashboardbackarrow;
    private LineChart graph_landscape;

    private LinearLayout lvdashboarddata;

    private ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] dashboardGraphBoolean = {true, true, true, true, true};
    String[] colors_array = {"#7cb5ec", "#424247", "#90ed7d", "#f7a35c", "#8085e9"};
    private LineData globalLinedata;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_landscape_activity);


        AxisData axisval=null;

        Intent in = this.getIntent();
        if (in != null) {
            axisval = in.getParcelableExtra(AppConstants.APP_DATA);
        }
        graph_landscape = (LineChart) findViewById(R.id.graph_landscape);
        lldashboardbackarrow = (LinearLayout) findViewById(R.id.lldashboardbackarrow);
        lldashboardbackarrow.setOnClickListener(this);


        lvdashboarddata = (LinearLayout) findViewById(R.id.lvdashboarddata);
        LinearLayout lvdashboardreal = (LinearLayout)findViewById(R.id.lvdashboardreal);
        TextView tvcolorreal = (TextView)findViewById(R.id.tvcolorreal);
        TextView tvdatareal = (TextView) findViewById(R.id.tvdatareal);

        LinearLayout lvdashboardcd = (LinearLayout)findViewById(R.id.lvdashboardcd);
        TextView tvcolorcd = (TextView) findViewById(R.id.tvcolorcd);
        TextView tvdatacd = (TextView) findViewById(R.id.tvdatacd);

        LinearLayout lvdashboardmin = (LinearLayout) findViewById(R.id.lvdashboardmin);
        TextView tvcolormin = (TextView) findViewById(R.id.tvcolormin);
        TextView tvdatamin = (TextView) findViewById(R.id.tvdatamin);

        LinearLayout lvdashboardmax = (LinearLayout) findViewById(R.id.lvdashboardmax);
        TextView tvcolormax = (TextView) findViewById(R.id.tvcolormax);
        TextView tvdatamax = (TextView) findViewById(R.id.tvdatamax);

        LinearLayout lvdashboardavg = (LinearLayout) findViewById(R.id.lvdashboardavg);
        TextView tvcoloravg = (TextView) findViewById(R.id.tvcoloravg);
        TextView tvdataavg = (TextView) findViewById(R.id.tvdataavg);

        graph_legendLayout.add(0, lvdashboardreal);
        graph_legendLayout.add(1, lvdashboardcd);
        graph_legendLayout.add(2, lvdashboardmin);
        graph_legendLayout.add(3, lvdashboardmax);
        graph_legendLayout.add(4, lvdashboardavg);
        tv_legendarray.add(0, tvcolorreal);
        tv_legendarray.add(1, tvcolorcd);
        tv_legendarray.add(2, tvcolormin);
        tv_legendarray.add(3, tvcolormax);
        tv_legendarray.add(4, tvcoloravg);


        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dashboardGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        updateViewAfterFetch(axisval);
    }
    private void updateViewAfterFetch(AxisData axisval) {

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }

        updategraph(axisval);
    }
    private void updategraph(AxisData axisval) {


        ArrayList<Entry> realtimeEntries = new ArrayList<>();
        ArrayList<Entry> cdEntries = new ArrayList<>();
        ArrayList<Entry> minEntries = new ArrayList<>();
        ArrayList<Entry> maxEntries = new ArrayList<>();
        ArrayList<Entry> avgEntries = new ArrayList<>();

        int blockLen = axisval.getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(axisval.getAxisval().getBlock().get(index));
            realtimeEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getRealtime().get(index))), index));
            cdEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getCd().get(index))), index));
            minEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getMin().get(index))), index));
            maxEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getMax().get(index))), index));
            avgEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getAvg().get(index))), index));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, realtimeEntries, "", AppConstants.DASHBOARD_COLOR_REALTIME, true);
        setGraphVales(dataSets, cdEntries, "", AppConstants.DASHBOARD_COLOR_CD, false);
        setGraphVales(dataSets, minEntries, "", AppConstants.DASHBOARD_COLOR_MIN, false);
        setGraphVales(dataSets, maxEntries, "", AppConstants.DASHBOARD_COLOR_MAX, false);
        setGraphVales(dataSets, avgEntries, " ", AppConstants.DASHBOARD_COLOR_AVG, false);

        globalLinedata = new LineData(labels, dataSets);

        globalLinedata.setHighlightEnabled(true);
        graph_landscape.setData(globalLinedata);
        graph_landscape.animateY(5000);
        graph_landscape.setDrawGridBackground(false);
        graph_landscape.setDescription(axisval.getAxisname());
        graph_landscape.getLegend().setEnabled(false);

        lvdashboarddata.setVisibility(View.VISIBLE);
    }
    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(Color.parseColor("#000000"));
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }
    private void updateGraphValues(int position) {

        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < dashboardGraphBoolean.length; index++) {
            if (index == position) {
                dashboardGraphBoolean[index] = !dashboardGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < dashboardGraphBoolean.length; index++) {
            if (dashboardGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_landscape.setData(temp);
        graph_landscape.animateY(5000);
    }
    private void resetGraphValues() {
        for(int index=0;index<dashboardGraphBoolean.length;index++){
            dashboardGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<dashboardGraphBoolean.length;index++){
            if(dashboardGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {

        if(view == lldashboardbackarrow){
            finish();
        }
    }
}
