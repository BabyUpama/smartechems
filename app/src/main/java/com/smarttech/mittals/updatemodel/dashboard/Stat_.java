
package com.smarttech.mittals.updatemodel.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stat_ implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("meter_no")
    @Expose
    private String meterNo;
    @SerializedName("val")
    @Expose
    private String val;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }


    protected Stat_(Parcel in) {
        name = in.readString();
        meterNo = in.readString();
        val = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(meterNo);
        dest.writeString(val);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Stat_> CREATOR = new Parcelable.Creator<Stat_>() {
        @Override
        public Stat_ createFromParcel(Parcel in) {
            return new Stat_(in);
        }

        @Override
        public Stat_[] newArray(int size) {
            return new Stat_[size];
        }
    };
}