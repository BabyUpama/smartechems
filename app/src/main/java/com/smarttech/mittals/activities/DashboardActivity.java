package com.smarttech.mittals.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.adapters.CustomSpinnerAdapter;
import com.smarttech.mittals.adapters.MenuCustomAdapter;
import com.smarttech.mittals.appbase.AppBaseActivity;
import com.smarttech.mittals.fragments.AveragePowerCostFragments;
import com.smarttech.mittals.fragments.CurrentvoltageFragment;
import com.smarttech.mittals.fragments.DashboardTab;
import com.smarttech.mittals.fragments.PowerProcuredTab_Fragment;
import com.smarttech.mittals.fragments.ProfitabilityFragment;
import com.smarttech.mittals.fragments.RealTImeTab_Fragment;
import com.smarttech.mittals.fragments.UnderDrawlFragment;
import com.smarttech.mittals.model.login.Client;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.tmplib.FragmentPagerItemAdapter;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;

import java.util.List;


public class DashboardActivity extends AppBaseActivity implements AdapterView.OnItemClickListener,
        AdapterView.OnItemSelectedListener, View.OnClickListener {

    private Class[] fragmentArray = new Class[8];

    private DrawerLayout mDrawerLayout;
    private ListView list_slidermenu;

    private ActionBarDrawerToggle mDrawerToggle;
    private ViewPager viewPager;
    private Spinner spinnername;
    private LinearLayout ivDots;
    private LoginData mData = null;
    private FragmentPagerItemAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(this, "No Data Found");
            finish();
            return;
        }

        setDrawer();
        setViewPager();


        list_slidermenu = (ListView) findViewById(R.id.list_slidermenu);
        list_slidermenu.setAdapter(new MenuCustomAdapter(this, mData.getValue().getMenu()));
        list_slidermenu.setOnItemClickListener(this);


        LinearLayout useraccount = (LinearLayout) findViewById(R.id.layout_useraccount);
        useraccount.setOnClickListener(this);
        LinearLayout notifications = (LinearLayout) findViewById(R.id.layout_notification);
        notifications.setOnClickListener(this);

        spinnername = (Spinner) findViewById(R.id.spinner_drawer_admin);
        spinnername.setOnItemSelectedListener(this);

        List<Client> clientsvar = mData.getValue().getValue().getClients();

        spinnername.setAdapter(new CustomSpinnerAdapter(this, clientsvar));

        ivDots = (LinearLayout) findViewById(R.id.ivDots);
        ivDots.setOnClickListener(this);

    }

    private void setViewPager() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setOnTouchListener(new OnTouch());

        setAppFrags(mData.getValue().getMenu());

        FragmentPagerItems pages = new FragmentPagerItems(this);
        setFragPages(mData, pages);

        mAdapter = new FragmentPagerItemAdapter(getSupportFragmentManager(), pages, mData.getValue().getValue().getUserId());

        viewPager.setScrollContainer(false);
        viewPager.setAdapter(mAdapter);
        viewPagerTab.setViewPager(viewPager);
        //viewPager.setOffscreenPageLimit(7);

    }

    private void setDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.toggle, // nav menu toggle icon
                R.string.app_name, // nav drawer open - description for
                // accessibility
                R.string.app_name // nav drawer close - description for
                // accessibility
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);


        findViewById(R.id.openLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

    }

    private void setFragPages(LoginData data, FragmentPagerItems pages) {

        List<com.smarttech.mittals.model.login.Menu> lList = data.getValue().getMenu();

        int siz = lList.size();

        for (int index = 0; index < siz - 1; index++) {

            pages.add(FragmentPagerItem.of(lList.get(index).getGraphName(), fragmentArray[index]));
        }
    }

    private void setAppFrags(List<com.smarttech.mittals.model.login.Menu> graph_namearray) {
        int index = 0;
        for (com.smarttech.mittals.model.login.Menu clnt : graph_namearray) {
            fragmentArray[index++] = getNumberedFrag(clnt.getGraphName());
        }
    }

    private Class getNumberedFrag(String str) {
        switch (str) {
            case "Dashboard":
                return DashboardTab.class;
            case "Real Time Data":
                return RealTImeTab_Fragment.class;
            case "Power Procured Source":
                return PowerProcuredTab_Fragment.class;
            case "Under Drawl":
                return UnderDrawlFragment.class;
            case "Average Power Cost":
                return AveragePowerCostFragments.class;
            case "(Current, Voltage, Freq, PF ) / Min":
                return CurrentvoltageFragment.class;
            case "Profitability":
                return ProfitabilityFragment.class;
            default:
                return null;
        }
    }

    private boolean isFirstTime = true;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if (isFirstTime) {
            isFirstTime = false;
            return;
        }

        List<Client> clnt = mData.getValue().getValue().getClients();
        Client client = clnt.get(position);

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }

        //AppLogger.showError("Change ID",client.getId());

        mAdapter.updateBundle(client.getId());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        if (position > 6) {
            Intent in = new Intent(DashboardActivity.this, Contact_UsActivity.class);
            in.putExtra(AppConstants.LOGIN_DATA, mData);
            //in.putExtra("caller", "Contact_UsActivity");
            startActivity(in);
            overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
            finish();
        } else {
            viewPager.setCurrentItem(position);
        }
        mDrawerLayout.closeDrawer(Gravity.LEFT);

    }

    @Override
    public void onClick(View view) {
        if (view == ivDots) {
            Intent in = new Intent(DashboardActivity.this, LoginActivity.class);
            startActivity(in);
            overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
            finish();
        }

    }


    private class OnTouch implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detailsmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.userAccount) {
            return true;
        }
        if (id == R.id.Dashboard) {
            AppLogger.showToastLarge(this, "Refresh App");
        }
        if (id == R.id.notification) {
            AppLogger.showToastLarge(this, "Create Text");
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    @Override
    public void setTitle(CharSequence title) {
//        mTitle = title;
//        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

}