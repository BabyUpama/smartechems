package com.smarttech.mittals.adapters;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.dashboard.Stat_;

import java.util.List;
import java.util.Random;

/**
 * Created by Ashish Karn on 07-12-2016.
 */

public class StatsListAdapter extends RecyclerView.Adapter<StatsListAdapter.MyViewHolder> {

    private List<Stat_> statList;
    private boolean isData=true;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_iexTitle,tvdetail;


        public MyViewHolder(View view) {
            super(view);
            tv_iexTitle = (TextView) view.findViewById(R.id.tv_iexTitle);
            tvdetail = (TextView) view.findViewById(R.id.tvdetail);

        }
    }

    public StatsListAdapter(List<Stat_> _statList){
        this.statList = _statList;
        if(statList== null){
            isData=false;
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_iex_ui_etc, parent, false);

        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,int position) {
        if(isData) {
           // position = position % statList.size();
            MyViewHolder myHolder= (MyViewHolder) holder;
            Stat_ _stat=statList.get(position);
            myHolder.tv_iexTitle.setText(_stat.getName());
            myHolder.tvdetail.setText(_stat.getVal());
            Random rnd = new Random();
            int bgcolor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            myHolder.tvdetail.setBackgroundColor(bgcolor);
        }


    }

    @Override
    public int getItemCount() {
        return statList.size();
    }
}