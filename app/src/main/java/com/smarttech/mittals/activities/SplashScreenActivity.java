package com.smarttech.mittals.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.appbase.AppBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.model.SplashData;
import com.smarttech.mittals.model.VersionData;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;
import com.smarttech.mittals.utils.DeviceInfoHelper;
import com.smarttech.mittals.utils.NetworkChecker;

/**
 * Created by babyu on 20-02-2016.
 */
public class SplashScreenActivity extends AppBaseActivity {

    private final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 2800;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (DeviceInfoHelper.canMakeSmores()) {
            lookForPermissions();
        } else {
            fetchInfo();
        }

        setShimmerView();
    }

    private void setShimmerView() {
        ShimmerTextView tv = (ShimmerTextView) findViewById(R.id.shimmer_tv);
        Shimmer shimmer = new Shimmer();
        shimmer.setRepeatCount(5)
                .setDuration(1000)
                .setStartDelay(300);
        shimmer.start(tv);
    }

    public void fetchInfo() {
        if (NetworkChecker.isNetworkAvailable(this)) {
            PackageManager manager = getApplicationContext().getPackageManager();
            PackageInfo info = null;
            try {
                info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String version = info.versionName;
            JSONObject param = new JSONObject();
            try {
                param.put("app_type", "android");
                param.put("version", version);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            String[] strArr=new String[2];
            strArr[0]=AppConstants.URL_BASE + AppConstants.SUBURL_VERSION;
            strArr[1]=param.toString();

            new NetworkHandlerModel(this,callback1, VersionData.class,2).execute(strArr);
        } else {
//            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
//            AppLogger.showSnakBarIndefinite(coordinatorLayout, "No Internet Connection");
            showNetworkDialog(this, R.layout.networkpopup);
        }
    }

    private void showNetworkDialog(SplashScreenActivity splashScreenActivity, int networkpopup) {
        final Dialog dialog = new Dialog(splashScreenActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void lookForPermissions() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                 ContextCompat.checkSelfPermission(this,
                         Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CALL_PHONE,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            fetchInfo();
        }
    }

    private void setDeviceData() {

        String[] heiwidth = DeviceInfoHelper.getDeviceHeightWidth(this);
        JSONObject param = new JSONObject();
        try {
            param.put("device_company", DeviceInfoHelper.getDeviceCompany());
            param.put("device_version", DeviceInfoHelper.getDeviceVersion());
            param.put("device_name", DeviceInfoHelper.getDeviceName());
            param.put("device_uuid", DeviceInfoHelper.getDeviceId(this));
            param.put("device_width", heiwidth[1]);
            param.put("device_height", heiwidth[0]);
            param.put("device_colordepth", "");
            param.put("device_platform", DeviceInfoHelper.getDevicePlatform());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String[] strArr=new String[2];
        strArr[0]=AppConstants.URL_BASE + AppConstants.SUBURL_REGISTERDEVICE;
        strArr[1]=param.toString();

        new NetworkHandlerModel(this,callback, SplashData.class,1).execute(strArr);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {

                if (grantResults.length > 0
                        && (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED)) {
                    fetchInfo();
                } else {
//                    finish();
                    fetchInfo();
                }
            }
        }
    }


    private INetworkCallback callback=new INetworkCallback() {
        @Override
        public void onUpdateResult(Object obj,int id) {
            if(obj==null){
                AppLogger.showToastSmall(getBaseContext(),"Something is worng");
                return;
            }

            SplashData data= (SplashData) obj;
            AppLogger.showToastSmall(getBaseContext(),data.getMessage());

            if(data.getType().equalsIgnoreCase("TRUE")){
                moveToNext();
            }

        }
    };

    private INetworkCallback callback1=new INetworkCallback() {
        @Override
        public void onUpdateResult(Object obj,int id) {
            if(obj==null){
                AppLogger.showToastSmall(getBaseContext(),"Something is worng");
                return;
            }

            VersionData data= (VersionData) obj;

            if(data.getStatus().equalsIgnoreCase("TRUE")){
                setDeviceData();
            }
            else if(data.getStatus().equalsIgnoreCase("FALSE")){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        SplashScreenActivity.this);
                alertDialogBuilder
                        .setMessage("There is an Update available!.Please update to use this App.");
                alertDialogBuilder.setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

        }
    };

    private void moveToNext() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.animation,
                R.anim.animation2);
        finish();
    }

}