
package com.smarttech.mittals.updatemodel.profitability;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Axisval implements Parcelable {

    @SerializedName("profit")
    @Expose
    private List<String> profit = null;
    @SerializedName("breakeven_price")
    @Expose
    private List<String> breakevenPrice = null;
    @SerializedName("landed_oa")
    @Expose
    private List<String> landedOa = null;
    @SerializedName("block")
    @Expose
    private List<String> block = null;
    @SerializedName("landed_bil")
    @Expose
    private List<String> landedBil = null;
    @SerializedName("landed_disc")
    @Expose
    private List<String> landedDisc = null;
    @SerializedName("ttl_power")
    @Expose
    private List<String> ttlPower = null;
    @SerializedName("ttl_oa")
    @Expose
    private List<String> ttlOa = null;

    public List<String> getProfit() {
        return profit;
    }

    public void setProfit(List<String> profit) {
        this.profit = profit;
    }

    public List<String> getBreakevenPrice() {
        return breakevenPrice;
    }

    public void setBreakevenPrice(List<String> breakevenPrice) {
        this.breakevenPrice = breakevenPrice;
    }

    public List<String> getLandedOa() {
        return landedOa;
    }

    public void setLandedOa(List<String> landedOa) {
        this.landedOa = landedOa;
    }

    public List<String> getBlock() {
        return block;
    }

    public void setBlock(List<String> block) {
        this.block = block;
    }

    public List<String> getLandedBil() {
        return landedBil;
    }

    public void setLandedBil(List<String> landedBil) {
        this.landedBil = landedBil;
    }

    public List<String> getLandedDisc() {
        return landedDisc;
    }

    public void setLandedDisc(List<String> landedDisc) {
        this.landedDisc = landedDisc;
    }

    public List<String> getTtlPower() {
        return ttlPower;
    }

    public void setTtlPower(List<String> ttlPower) {
        this.ttlPower = ttlPower;
    }

    public List<String> getTtlOa() {
        return ttlOa;
    }

    public void setTtlOa(List<String> ttlOa) {
        this.ttlOa = ttlOa;
    }


    protected Axisval(Parcel in) {
        if (in.readByte() == 0x01) {
            profit = new ArrayList<String>();
            in.readList(profit, String.class.getClassLoader());
        } else {
            profit = null;
        }
        if (in.readByte() == 0x01) {
            breakevenPrice = new ArrayList<String>();
            in.readList(breakevenPrice, String.class.getClassLoader());
        } else {
            breakevenPrice = null;
        }
        if (in.readByte() == 0x01) {
            landedOa = new ArrayList<String>();
            in.readList(landedOa, String.class.getClassLoader());
        } else {
            landedOa = null;
        }
        if (in.readByte() == 0x01) {
            block = new ArrayList<String>();
            in.readList(block, String.class.getClassLoader());
        } else {
            block = null;
        }
        if (in.readByte() == 0x01) {
            landedBil = new ArrayList<String>();
            in.readList(landedBil, String.class.getClassLoader());
        } else {
            landedBil = null;
        }
        if (in.readByte() == 0x01) {
            landedDisc = new ArrayList<String>();
            in.readList(landedDisc, String.class.getClassLoader());
        } else {
            landedDisc = null;
        }
        if (in.readByte() == 0x01) {
            ttlPower = new ArrayList<String>();
            in.readList(ttlPower, String.class.getClassLoader());
        } else {
            ttlPower = null;
        }
        if (in.readByte() == 0x01) {
            ttlOa = new ArrayList<String>();
            in.readList(ttlOa, String.class.getClassLoader());
        } else {
            ttlOa = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (profit == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(profit);
        }
        if (breakevenPrice == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(breakevenPrice);
        }
        if (landedOa == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(landedOa);
        }
        if (block == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(block);
        }
        if (landedBil == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(landedBil);
        }
        if (landedDisc == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(landedDisc);
        }
        if (ttlPower == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ttlPower);
        }
        if (ttlOa == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ttlOa);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Axisval> CREATOR = new Parcelable.Creator<Axisval>() {
        @Override
        public Axisval createFromParcel(Parcel in) {
            return new Axisval(in);
        }

        @Override
        public Axisval[] newArray(int size) {
            return new Axisval[size];
        }
    };
}