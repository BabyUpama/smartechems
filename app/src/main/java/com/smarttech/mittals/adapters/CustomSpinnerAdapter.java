package com.smarttech.mittals.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.appbase.AppBaseActivity;
import com.smarttech.mittals.model.login.Client;

import java.util.List;

/**
 * Created by upama on 4/1/17.
 */

public class CustomSpinnerAdapter extends BaseAdapter {
    private List<Client> clientlist;
    private AppBaseActivity context;

    public CustomSpinnerAdapter(AppBaseActivity _context, List<Client> _clientlist) {
        super();
        this.context = _context;
        this.clientlist = _clientlist;
    }


    @Override
    public int getCount() {
        return clientlist.size();
    }

    @Override
    public Client getItem(int i) {
        return clientlist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.spinner_item, parent, false);
        }

        Client client = getItem(position);

        TextView clientlist = (TextView) convertView.findViewById(R.id.clientlist);
        clientlist.setText(client.getName());

        return convertView;
    }


}

