package com.smarttech.mittals.db;

import android.content.Context;
import android.content.SharedPreferences;

import com.smarttech.mittals.appbase.AppBaseActivity;

/**
 * Created by upama on 4/1/17.
 */

public class SharedPrefHandler {

    public static void saveBoolean(AppBaseActivity context, String column, boolean value) {
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(column, value);
        editor.commit();
    }

    public static void saveString(AppBaseActivity context, String column, String value) {
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(column, value);
        editor.commit();
    }

    public static void saveLoginCreadential(AppBaseActivity context, String[] value) {
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("username", value[0]);
        editor.putString("password", value[1]);
        editor.commit();
    }




    /*
        Get Data
     */

    public static String getString(AppBaseActivity context, String column) {
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getString(column, null);
    }

    public static boolean getBoolean(AppBaseActivity context, String column) {
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getBoolean(column, false);
    }

    /*
        Data Base Clear
     */

    public static void clearSharedPre(AppBaseActivity context){
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }



}
