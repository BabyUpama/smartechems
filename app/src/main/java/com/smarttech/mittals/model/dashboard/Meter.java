
package com.smarttech.mittals.model.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meter implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("meter_id")
    @Expose
    private String meterId;
    @SerializedName("checked")
    @Expose
    private Boolean checked;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    protected Meter(Parcel in) {
        id = in.readString();
        meterId = in.readString();
        byte checkedVal = in.readByte();
        checked = checkedVal == 0x02 ? null : checkedVal != 0x00;
        status = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(meterId);
        if (checked == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (checked ? 0x01 : 0x00));
        }
        dest.writeString(status);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Meter> CREATOR = new Parcelable.Creator<Meter>() {
        @Override
        public Meter createFromParcel(Parcel in) {
            return new Meter(in);
        }

        @Override
        public Meter[] newArray(int size) {
            return new Meter[size];
        }
    };
}