
package com.smarttech.mittals.updatemodel.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Solar implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("val")
    @Expose
    private String val;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }


    protected Solar(Parcel in) {
        name = in.readString();
        val = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(val);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Solar> CREATOR = new Parcelable.Creator<Solar>() {
        @Override
        public Solar createFromParcel(Parcel in) {
            return new Solar(in);
        }

        @Override
        public Solar[] newArray(int size) {
            return new Solar[size];
        }
    };
}