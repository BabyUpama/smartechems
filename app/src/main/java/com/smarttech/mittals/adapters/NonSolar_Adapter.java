package com.smarttech.mittals.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.dashboard.NonSolar;

import java.util.List;


/**
 * Created by upama on 5/1/17.
 */

public class NonSolar_Adapter extends RecyclerView.Adapter<NonSolar_Adapter.ViewHolder> {

    private List<NonSolar> nonsolarList;
    private boolean isData=true;
    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvpurnonsolar,tvpurnonsolarnum;

        public ViewHolder(View v) {
            super(v);
            tvpurnonsolar =  (TextView) v.findViewById(R.id.tvpurnonsolar);
            tvpurnonsolarnum =  (TextView) v.findViewById(R.id.tvpurnonsolarnum);

        }
    }

    public NonSolar_Adapter(List<NonSolar> _nonsolar){

        nonsolarList = _nonsolar;

        if(nonsolarList== null){
            isData=false;
        }
    }

    @Override
    public NonSolar_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_nonsolar, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(NonSolar_Adapter.ViewHolder holder, int position) {
        if(isData) {
             position = position % nonsolarList.size();
            ViewHolder myHolder= (ViewHolder) holder;
            NonSolar nonsolar=nonsolarList.get(position);
            myHolder.tvpurnonsolar.setText(nonsolar.getName());
            myHolder.tvpurnonsolarnum.setText(nonsolar.getVal());

        }

    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }
}
