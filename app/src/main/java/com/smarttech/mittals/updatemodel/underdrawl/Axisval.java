
package com.smarttech.mittals.updatemodel.underdrawl;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Axisval implements Parcelable {

    @SerializedName("underdrawl")
    @Expose
    private List<String> underdrawl = null;
    @SerializedName("ttl_power")
    @Expose
    private List<String> ttlPower = null;
    @SerializedName("oa_power")
    @Expose
    private List<String> oaPower = null;
    @SerializedName("block")
    @Expose
    private List<String> block = null;
    @SerializedName("ttl_oa")
    @Expose
    private List<String> ttlOa = null;

    public List<String> getUnderdrawl() {
        return underdrawl;
    }

    public void setUnderdrawl(List<String> underdrawl) {
        this.underdrawl = underdrawl;
    }

    public List<String> getTtlPower() {
        return ttlPower;
    }

    public void setTtlPower(List<String> ttlPower) {
        this.ttlPower = ttlPower;
    }

    public List<String> getOaPower() {
        return oaPower;
    }

    public void setOaPower(List<String> oaPower) {
        this.oaPower = oaPower;
    }

    public List<String> getBlock() {
        return block;
    }

    public void setBlock(List<String> block) {
        this.block = block;
    }

    public List<String> getTtlOa() {
        return ttlOa;
    }

    public void setTtlOa(List<String> ttlOa) {
        this.ttlOa = ttlOa;
    }


    protected Axisval(Parcel in) {
        if (in.readByte() == 0x01) {
            underdrawl = new ArrayList<String>();
            in.readList(underdrawl, String.class.getClassLoader());
        } else {
            underdrawl = null;
        }
        if (in.readByte() == 0x01) {
            ttlPower = new ArrayList<String>();
            in.readList(ttlPower, String.class.getClassLoader());
        } else {
            ttlPower = null;
        }
        if (in.readByte() == 0x01) {
            oaPower = new ArrayList<String>();
            in.readList(oaPower, String.class.getClassLoader());
        } else {
            oaPower = null;
        }
        if (in.readByte() == 0x01) {
            block = new ArrayList<String>();
            in.readList(block, String.class.getClassLoader());
        } else {
            block = null;
        }
        if (in.readByte() == 0x01) {
            ttlOa = new ArrayList<String>();
            in.readList(ttlOa, String.class.getClassLoader());
        } else {
            ttlOa = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (underdrawl == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(underdrawl);
        }
        if (ttlPower == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ttlPower);
        }
        if (oaPower == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(oaPower);
        }
        if (block == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(block);
        }
        if (ttlOa == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ttlOa);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Axisval> CREATOR = new Parcelable.Creator<Axisval>() {
        @Override
        public Axisval createFromParcel(Parcel in) {
            return new Axisval(in);
        }

        @Override
        public Axisval[] newArray(int size) {
            return new Axisval[size];
        }
    };
}