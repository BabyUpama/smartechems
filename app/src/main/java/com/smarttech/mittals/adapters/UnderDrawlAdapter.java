package com.smarttech.mittals.adapters;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.library.AutoTypeTextView;
import com.smarttech.mittals.updatemodel.underdrawl.Stat;

import java.util.List;
import java.util.Random;


/**
 * Created by Ashish Karn on 08-12-2016.
 */

public class UnderDrawlAdapter extends RecyclerView.Adapter<UnderDrawlAdapter.MyViewHolder> {

    private List<Stat> statslist;
    private boolean isData=true;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title,tvcolor,tvunit;
        public AutoTypeTextView lblTextDecryption;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            lblTextDecryption = (AutoTypeTextView) view.findViewById(R.id.lblTextDecryption);
            tvcolor = (TextView)view.findViewById(R.id.tvcolor);
            tvunit = (TextView) view.findViewById(R.id.tvunit);

        }
    }


    public UnderDrawlAdapter(List<Stat> _statlist) {
        this.statslist = _statlist;
        if(statslist== null){
            isData=false;
        }
    }

    @Override
    public UnderDrawlAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_recycle_underdrawl, parent, false);

        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(final UnderDrawlAdapter.MyViewHolder holder, final int position) {
        if(isData) {
            // position = position % statList.size();
            MyViewHolder myHolder= (MyViewHolder) holder;
            Stat stats=statslist.get(position);
            myHolder.tv_title.setText(stats.getName());
            myHolder.lblTextDecryption.setText(stats.getVal());
            myHolder.lblTextDecryption.animateDecryption(stats.getVal());
            myHolder.tvunit.setText(stats.getUnit());
            Random rnd = new Random();
            int bgcolor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            myHolder.tvcolor.setBackgroundColor(bgcolor);

        }
    }

    @Override
    public int getItemCount() {
        return statslist.size();
    }
}