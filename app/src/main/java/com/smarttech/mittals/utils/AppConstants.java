package com.smarttech.mittals.utils;

import android.graphics.Color;

/**
 * Created by babyu on 24-06-2016.
 */
public class AppConstants {
    /*  Url used for application http://www.smartechenergy.in/sems/client/appapi_controller/loginapi

    http://ems.smartechenergy.in/client/appapi_controller
    * */
    public static final String URL_BASE="https://portal.smartechenergy.in/client/appapi_controller";
    public static final String SUBURL_VERSION ="/checkversion";
    public static final String SUBURL_REGISTERDEVICE ="/deviceregistration";
    public static final String SUBURL_LOGIN="/loginapi";
    public static final String SUBURL_DASHBOARD="/dashboardapi";
    public static final String SUBURL_COMMON="/graphapi";
    public static final String SUBURL_CONTACT_US="/contactapi";

    // color constants for MAP
    public static final int DASHBOARD_COLOR_REALTIME= Color.parseColor("#7cb5ec");
    public static final int DASHBOARD_COLOR_CD= Color.parseColor("#424247");
    public static final int DASHBOARD_COLOR_MIN= Color.parseColor("#90ed7d");
    public static final int DASHBOARD_COLOR_MAX= Color.parseColor("#f7a35c");
    public static final int DASHBOARD_COLOR_AVG= Color.parseColor("#8085e9");

    public static final int CHECK_COLOR= Color.parseColor("#338147");

    public static final int POWER_COLOR_TOTALPOWERCONSUMPTION= Color.parseColor("#fbc1d9");
    public static final int POWER_COLOR_IEXCLEAR= Color.parseColor("#306824");
    public static final int POWER_COLOR_CD= Color.parseColor("#ff0000");
    public static final int POWER_COLOR_TOTALPOWERRECIEVED= Color.parseColor("#7cb5ec");
    public static final int POWER_COLOR_CLEAREDQUANT= Color.parseColor("#434348");
    public static final int POWER_COLOR_BIDQUANT= Color.parseColor("#90ed7d");
    public static final int POWER_COLOR_BIDPRICE= Color.parseColor("#0000ff");

    public static final int UNDERDRAWL= Color.parseColor("#7cb5ec");
    public static final int UNDERDRAWL_TTLPOWER= Color.parseColor("#434348");
    public static final int UNDERDRAWL_OAPOWER= Color.parseColor("#90ed7d");
    public static final int UNDERDRAWL_TTLOA= Color.parseColor("#f7a35c");

    public static final int PROFIT_COLOR_TOTALPOWER= Color.parseColor("#ffafaf");
    public static final int PROFIT_COLOR_LANDEDOA= Color.parseColor("#8000ff");
    public static final int PROFIT_COLOR_PROFIT= Color.parseColor("#00ffff");
    public static final int PROFIT_COLOR_BREAKEVEN= Color.parseColor("#8b008b");
    public static final int PROFIT_COLOR_TOTALOA= Color.parseColor("#808080");
    public static final int PROFIT_COLOR_LANDEDDISC= Color.parseColor("#008000");
    public static final int PROFIT_COLOR_LANDEDBIL= Color.parseColor("#ff0000");

    public static final int CURRVOLT_COLOR_PHASEA= Color.parseColor("#99ff99");
    public static final int CURRVOLT_COLOR_PHASEB= Color.parseColor("#00b050");
    public static final int CURRVOLT_COLOR_PHASEC= Color.parseColor("#047739");
    public static final int CURRVOLT_COLOR_FREQ= Color.parseColor("#ff0000");
    public static final int CURRVOLT_COLOR_PF= Color.parseColor("#8c0707");


    /*
        Data Base Constants
     */
    public static final String DB_TABLE="smartech.ems";
    public static final String DB_IS_LOGIN="ems.islogin";
    public static final String DB_CREDENTIAL="ems.credential";

    public static final String APP_DATA="axisdata";

    public static final String LOGIN_DATA="data";

    public static final String DATA_CURRENTID="currentid";
    public static final String DATA_POSITION="FragmentPagerItem:Position";

    public static final String USER_ONLINE = "useronline";



}