package com.smarttech.mittals.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.gkravas.meterview.MeterView;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.activities.Realtime_landscape_activity;
import com.smarttech.mittals.adapters.Adapter_RealtimeMeters;
import com.smarttech.mittals.adapters.RealtimeTabAdapter;
import com.smarttech.mittals.appbase.AppBaseFragment;
import com.smarttech.mittals.handler.IDataPasser;
import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.updatemodel.realtime.MeterDetail;
import com.smarttech.mittals.updatemodel.realtime.Meters;
import com.smarttech.mittals.updatemodel.realtime.Realtimedata;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;


public class RealTImeTab_Fragment extends AppBaseFragment implements View.OnClickListener {

    View rowView;
    TextView dpResult;
    RealtimeTabAdapter horizontalAdptr_realtimeTab;

    private int year;
    private int month;
    private int day;

    MeterView meterView;
    private LoginData mData = null;
    private int fragPosition = -1;

    private CheckBox cbx1, cbx2, cbx3, cbx4;

    private String datesend, modeSend;
    private RadioGroup materialradiogroup;
    private RadioButton materialradiokw, materialradiokwh, materialradiokva;
    private Button viewGraph_realtime;
    private RecyclerView recyclrview_Demand;
    private LineChart Graph_realtime;
    private LineData globalLinedata;
    private LinearLayout lvrealtimedata;
    private ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] realtimeGraphBoolean = {true, true, true, true, true};
    String[] colors_array = {"#7cb5ec", "#424247", "#90ed7d", "#f7a35c", "#8085e9"};

    private String currentHospitalId = null;
    private Realtimedata data = null;

    private RecyclerView rv_realtimemeters;
    private LinearLayoutManager mLayoutManager;
    private Adapter_RealtimeMeters mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();

        currentHospitalId = args.getString(AppConstants.DATA_CURRENTID);
        fragPosition = getArguments().getInt(AppConstants.DATA_POSITION);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }

        if (currentHospitalId == null) {
            currentHospitalId = mData.getValue().getValue().getUserId();
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rowView = inflater.inflate(R.layout.fragment_real_time_tab, container, false);
//        cbx1 = (CheckBox) rowView.findViewById(R.id.cbx1);
//        cbx2 = (CheckBox) rowView.findViewById(R.id.cbx2);
//        cbx3 = (CheckBox) rowView.findViewById(R.id.cbx3);
//        cbx4 = (CheckBox) rowView.findViewById(R.id.cbx4);

//        cbx1.setOnClickListener(this);
//        cbx2.setOnClickListener(this);
//        cbx3.setOnClickListener(this);
//        cbx4.setOnClickListener(this);

        materialradiogroup = (RadioGroup) rowView.findViewById(R.id.materialradiogroup);

        materialradiokw = (RadioButton) rowView.findViewById(R.id.materialradiokw);
        materialradiogroup.check(materialradiokw.getId());
        modeSend = "kw";

        materialradiokwh = (RadioButton) rowView.findViewById(R.id.materialradiokwh);

        materialradiokva = (RadioButton) rowView.findViewById(R.id.materialradiokva);

        materialradiokw.setOnClickListener(this);
        materialradiokwh.setOnClickListener(this);
        materialradiokva.setOnClickListener(this);

        dpResult = (TextView) rowView.findViewById(R.id.datePicker_realtime);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 0);
        Date result = c.getTime();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        datesend = df.format(c.getTime());
        dpResult.setText(datesend);
        dpResult.setOnClickListener(this);

        viewGraph_realtime = (Button) rowView.findViewById(R.id.viewGraph_realtime);
        viewGraph_realtime.setOnClickListener(this);

        recyclrview_Demand = (RecyclerView) rowView.findViewById(R.id.recyclrview_Demand);
        meterView = (MeterView) rowView.findViewById(R.id.meter_view);

        Graph_realtime = (LineChart) rowView.findViewById(R.id.Graph_realtime);

        lvrealtimedata = (LinearLayout) rowView.findViewById(R.id.lvrealtimedata);
        LinearLayout lvrealtimereal = (LinearLayout) rowView.findViewById(R.id.lvrealtimereal);
        TextView tvcolorreal = (TextView) rowView.findViewById(R.id.tvcolorreal);
        TextView tvdatareal = (TextView) rowView.findViewById(R.id.tvdatareal);

        LinearLayout lvrealtimecd = (LinearLayout) rowView.findViewById(R.id.lvrealtimecd);
        TextView tvcolorcd = (TextView) rowView.findViewById(R.id.tvcolorcd);
        TextView tvdatacd = (TextView) rowView.findViewById(R.id.tvdatacd);

        LinearLayout lvrealtimemin = (LinearLayout) rowView.findViewById(R.id.lvrealtimemin);
        TextView tvcolormin = (TextView) rowView.findViewById(R.id.tvcolormin);
        TextView tvdatamin = (TextView) rowView.findViewById(R.id.tvdatamin);

        LinearLayout lvrealtimemax = (LinearLayout) rowView.findViewById(R.id.lvrealtimemax);
        TextView tvcolormax = (TextView) rowView.findViewById(R.id.tvcolormax);
        TextView tvdatamax = (TextView) rowView.findViewById(R.id.tvdatamax);

        LinearLayout lvrealtimeavg = (LinearLayout) rowView.findViewById(R.id.lvrealtimeavg);
        TextView tvcoloravg = (TextView) rowView.findViewById(R.id.tvcoloravg);
        TextView tvdataavg = (TextView) rowView.findViewById(R.id.tvdataavg);

        rv_realtimemeters = (RecyclerView) rowView.findViewById(R.id.rv_realtimemeters);

        graph_legendLayout.add(0, lvrealtimereal);
        graph_legendLayout.add(1, lvrealtimecd);
        graph_legendLayout.add(2, lvrealtimemin);
        graph_legendLayout.add(3, lvrealtimemax);
        graph_legendLayout.add(4, lvrealtimeavg);
        tv_legendarray.add(0, tvcolorreal);
        tv_legendarray.add(1, tvcolorcd);
        tv_legendarray.add(2, tvcolormin);
        tv_legendarray.add(3, tvcolormax);
        tv_legendarray.add(4, tvcoloravg);


        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (realtimeGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        setRealTime(null);

        return rowView;


    }


    private void setRealTime(JSONObject object) {

        JSONObject param = new JSONObject();
        try {
            param.put("graph_id", mData.getValue().getMenu().get(fragPosition).getGraphId());
            param.put("user_id", currentHospitalId);
            param.put("access_key", mData.getValue().getValue().getAccessKey());
            param.put("date", datesend);
            param.put("mode", modeSend);
            if (object == null) {
                param.put("meters_id", "");
            } else {
                param.put("meters_id", object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = AppConstants.URL_BASE + AppConstants.SUBURL_COMMON;
        str[1] = param.toString();

        new NetworkHandlerModel(getActivity(), callback, Realtimedata.class, 1).execute(str);
    }

    private void setRealTime() throws JSONException {

//        JSONObject object = new JSONObject();
//        JSONArray aray = new JSONArray();
//        JSONObject meterObject = new JSONObject();
//        meterObject.put("id", "1");
//        meterObject.put("meter_id", "FMRI - 001");
//        meterObject.put("checked", true);
//        meterObject.put("status", "checked");
//        aray.put(meterObject);
//
//        object.put("meters", aray);
//
//        setRealTime(object);

        JSONObject paramObject = new JSONObject();

        Meters currentMeters = data.getMeters();

        if (currentMeters != null) {
            JSONArray powerArray = new JSONArray();

            if (currentMeters.getPower() != null && !currentMeters.getPower().isEmpty()) {

                List<MeterDetail> pList = currentMeters.getPower();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId().replaceAll("&", ""));
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if (cMeterDetail.getChecked()) {
                        tmpObject.put("status", "checked");
                    } else {
                        tmpObject.put("status", "");
                    }


                    powerArray.put(tmpObject);
                }
                paramObject.put("power", powerArray);
            }

            if (currentMeters.getSolar() != null && !currentMeters.getSolar().isEmpty()) {
                List<MeterDetail> pList = currentMeters.getSolar();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId().replaceAll("&", ""));
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if (cMeterDetail.getChecked()) {
                        tmpObject.put("status", "checked");
                    } else {
                        tmpObject.put("status", "");
                    }

                    powerArray.put(tmpObject);
                }
                paramObject.put("solar", powerArray);
            }

            setRealTime(paramObject);

        } else {
            AppLogger.showToastSmall(getActivity().getApplicationContext(), "Current Meter is NULL.");
        }
    }

    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getActivity(), "Some Error Occurred");
                return;
            }

            data = (Realtimedata) obj;

            if (data != null) {
                updateViewAfterFetch(data);
                setCheckBox(data);
            }

        }

    };


    private void setCheckBox(Realtimedata data) {

//        CheckBox[] checkArr={cbx1,cbx2,cbx3,cbx4};
//
//        List<com.smarttech.mittals.model.realtime.Meter> variable = data.getMeters();
//
//        int siz = Math.min(variable.size(),checkArr.length);
//
//        for(int i = 0;i<siz; i++){
//
//            com.smarttech.mittals.model.realtime.Meter mtr = variable.get(i);
//
//            if (mtr.getChecked() == true) {
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setBackgroundColor(AppConstants.CHECK_COLOR);
//                checkArr[i].setTextColor(Color.WHITE);
//                checkArr[i].setChecked(true);
//                checkArr[i].setVisibility(View.VISIBLE);
//            } else if(mtr.getChecked() == false) {
//                checkArr[i].setBackgroundColor(Color.WHITE);
//                //checkArr[i].setTextColor(Color.parseColor("#737272"));
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setChecked(false);
//                checkArr[i].setVisibility(View.VISIBLE);
//            }
//            else{
//                checkArr[i].setVisibility(View.GONE);
//            }
//        }

        if (data.getMeters() != null) {

            rv_realtimemeters.setHasFixedSize(true);

            // The number of Columns
            mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rv_realtimemeters.setLayoutManager(mLayoutManager);


            if (!data.getMeters().getPower().isEmpty()) {
                mAdapter = new Adapter_RealtimeMeters(dataPasser, data.getMeters().getPower());
                rv_realtimemeters.setAdapter(mAdapter);
            } else if (!data.getMeters().getSolar().isEmpty()) {
                mAdapter = new Adapter_RealtimeMeters(dataPasser, data.getMeters().getSolar());
                rv_realtimemeters.setAdapter(mAdapter);
            }
        }

    }

    private void updateViewAfterFetch(Realtimedata data) {

        if (data.getGraphData() != null) {
            horizontalAdptr_realtimeTab = new RealtimeTabAdapter(data.getGraphData().getStats());
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclrview_Demand.setLayoutManager(horizontalLayoutManagaer);
            recyclrview_Demand.setAdapter(horizontalAdptr_realtimeTab);

            for (int index = 0; index < colors_array.length; index++) {
                tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
            }

            updategraph(data);
        }


    }


    private void updategraph(Realtimedata data) {

        ArrayList<Entry> realtimeEntries = new ArrayList<>();
        ArrayList<Entry> cdEntries = new ArrayList<>();
        ArrayList<Entry> minEntries = new ArrayList<>();
        ArrayList<Entry> maxEntries = new ArrayList<>();
        ArrayList<Entry> avgEntries = new ArrayList<>();


        int blockLen = data.getGraphData().getAxisData().getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(data.getGraphData().getAxisData().getAxisval().getBlock().get(index));
            realtimeEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getRealtime().get(index))), index));
            cdEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getCd().get(index))), index));
            minEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getMin().get(index))), index));
            maxEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getMax().get(index))), index));
            avgEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getAvg().get(index))), index));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, realtimeEntries, "", AppConstants.DASHBOARD_COLOR_REALTIME, true);
        setGraphVales(dataSets, cdEntries, "", AppConstants.DASHBOARD_COLOR_CD, false);
        setGraphVales(dataSets, minEntries, "", AppConstants.DASHBOARD_COLOR_MIN, false);
        setGraphVales(dataSets, maxEntries, "", AppConstants.DASHBOARD_COLOR_MAX, false);
        setGraphVales(dataSets, avgEntries, " ", AppConstants.DASHBOARD_COLOR_AVG, false);

        globalLinedata = new LineData(labels, dataSets);

        globalLinedata.setHighlightEnabled(true);
        Graph_realtime.setData(globalLinedata);
        Graph_realtime.animateY(5000);
        Graph_realtime.setDrawGridBackground(false);
        Graph_realtime.setDescription(data.getGraphData().getAxisData().getAxisname());
        Graph_realtime.getLegend().setEnabled(false);
        YAxis axisRight = Graph_realtime.getAxisRight();
        axisRight.setDrawLabels(false);

        /*YAxis axisLeft = Graph_realtime.getAxisLeft();
        axisLeft.setDrawLabels(false);

        XAxis axis = Graph_realtime.getXAxis();
        axis.setDrawLabels(false);*/

        lvrealtimedata.setVisibility(View.VISIBLE);
    }

    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }

    private void updateGraphValues(int position) {

        LineData tempLineData = globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < realtimeGraphBoolean.length; index++) {
            if (index == position) {
                realtimeGraphBoolean[index] = !realtimeGraphBoolean[index];
                break;
            }
        }

        if (isEmpty()) {
            resetGraphValues();
        }

        for (int index = 0; index < realtimeGraphBoolean.length; index++) {
            if (realtimeGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        Graph_realtime.setData(temp);
        Graph_realtime.animateY(5000);
    }

    private void resetGraphValues() {
        for (int index = 0; index < realtimeGraphBoolean.length; index++) {
            realtimeGraphBoolean[index] = true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }

    private boolean isEmpty() {
        int counter = 0;
        for (int index = 0; index < realtimeGraphBoolean.length; index++) {
            if (realtimeGraphBoolean[index]) {
                counter = counter + 1;
            }
        }
        if (counter > 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        if (view == materialradiokw) {
            modeSend = "kw";
            try {
                setRealTime();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (view == materialradiokwh) {
            modeSend = "kwh";
            try {
                setRealTime();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (view == materialradiokva) {
            modeSend = "kva";
            try {
                setRealTime();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (view == dpResult) {

            Calendar mcurrentDate = Calendar.getInstance();
            year = mcurrentDate.get(Calendar.YEAR);
            month = mcurrentDate.get(Calendar.MONTH);
            day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    year = selectedyear;
                    month = selectedmonth;
                    day = selectedday;

                    // set selected date into textview
                    dpResult.setText(new StringBuilder().append(day)
                            .append("-").append(month + 1).append("-").append(year)
                            .append(" "));

                    // set selected date into datepicker also
                    datesend = dpResult.getText().toString();
                    try {
                        setRealTime();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();

        }

        if (view == viewGraph_realtime) {
            Intent i = new Intent(getActivity(), Realtime_landscape_activity.class);

            i.putExtra(AppConstants.APP_DATA, data.getGraphData().getAxisData());
            startActivity(i);

        }
//       if (view == cbx2) {
//           if (cbx2.isChecked()) {
//
////                List<Meter> meters=data.getMeters();
////                meters.get(1).setChecked(true);
////
////                setCheckBox(data);
//
//               try {
//                   setRealTime();
//               } catch (JSONException e) {
//                   e.printStackTrace();
//               }
//           }
//       }
//       if (view == cbx3) {
//           if (cbx3.isChecked()) {
//
////                List<Meter> meters=data.getMeters();
////                meters.get(1).setChecked(true);
////
////                setCheckBox(data);
//
//               try {
//                   setRealTime();
//               } catch (JSONException e) {
//                   e.printStackTrace();
//               }
//           }
//       }
//       if (view == cbx4) {
//           if (cbx4.isChecked()) {
//               try {
//                   setRealTime();
//               } catch (JSONException e) {
//                   e.printStackTrace();
//               }
//           }
//       }
    }


    @Override
    public void onResume() {
        super.onResume();
        final float min = 0;
        final float max = 54;
        final Random random = new Random();

        Observable.interval(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<Long, Float>() {
                    @Override
                    public Float call(Long aLong) {
                        return min + (random.nextFloat() * (max - min));
                    }
                })
                .subscribe(new Action1<Float>() {
                    @Override
                    public void call(Float value) {
                        meterView.setValue(value);
                    }
                });
    }

    private IDataPasser dataPasser = new IDataPasser() {

        @Override
        public void callApi(Object obj) {
            try {
                setRealTime();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}