package com.smarttech.mittals.handler;

/**
 * Created by upama on 28/6/17.
 */

public interface IDataPasser<T> {

    public void callApi(T data);
}
