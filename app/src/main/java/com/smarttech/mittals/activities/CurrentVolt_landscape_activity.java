package com.smarttech.mittals.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.currentvoltage.AxisData;
import com.smarttech.mittals.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by upama on 26/12/16.
 */

public class CurrentVolt_landscape_activity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout llcurrentbackarrow;
    private LineChart graph_landscapecurrvolt;
    private LinearLayout lvcurrvoltdata;
    private TextView tvdataphasea,tvdataphaseb,tvdataphasec;

    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] currvoltGraphBoolean = {true,true,true,true,true};
    String[] colors_array = {"#99ff99", "#00b050", "#047739", "#ff0000", "#8c0707"};
    private LineData globalLinedata;
    private String modeSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currentvolt_landscape_activity);

        AxisData axisval=null;

        Intent in = this.getIntent();
        if (in != null) {
            modeSend = in.getStringExtra("modeSend");
            axisval = in.getParcelableExtra(AppConstants.APP_DATA);
        }

        graph_landscapecurrvolt=(LineChart) findViewById(R.id.graph_landscapecurrvolt);

        lvcurrvoltdata = (LinearLayout)findViewById(R.id.lvcurrvoltdata);
        LinearLayout lvcurrvoltphasea = (LinearLayout)findViewById(R.id.lvcurrvoltphasea);
        TextView tvcolorphasea = (TextView) findViewById(R.id.tvcolorphasea);
        tvdataphasea = (TextView)findViewById(R.id.tvdataphasea);

        LinearLayout lvcurrvoltphaseb = (LinearLayout) findViewById(R.id.lvcurrvoltphaseb);
        TextView tvcolorphaseb = (TextView) findViewById(R.id.tvcolorphaseb);
        tvdataphaseb = (TextView) findViewById(R.id.tvdataphaseb);

        LinearLayout lvcurrvoltphasec = (LinearLayout) findViewById(R.id.lvcurrvoltphasec);
        TextView tvcolorphasec = (TextView) findViewById(R.id.tvcolorphasec);
        tvdataphasec = (TextView) findViewById(R.id.tvdataphasec);

        LinearLayout lvcurrvoltfreq = (LinearLayout) findViewById(R.id.lvcurrvoltfreq);
        TextView tvcolorfreq = (TextView) findViewById(R.id.tvcolorfreq);
        TextView tvdatafreq = (TextView) findViewById(R.id.tvdatafreq);

        LinearLayout lvcurrvoltpf = (LinearLayout) findViewById(R.id.lvcurrvoltpf);
        TextView tvcolorpf = (TextView) findViewById(R.id.tvcolorpf);
        TextView tvdatapf = (TextView) findViewById(R.id.tvdatapf);

        llcurrentbackarrow = (LinearLayout)findViewById(R.id.llcurrentbackarrow);
        llcurrentbackarrow.setOnClickListener(this);

        graph_legendLayout.add(0, lvcurrvoltphasea);
        graph_legendLayout.add(1, lvcurrvoltphaseb);
        graph_legendLayout.add(2, lvcurrvoltphasec);
        graph_legendLayout.add(3, lvcurrvoltfreq);
        graph_legendLayout.add(4, lvcurrvoltpf);
        tv_legendarray.add(0, tvcolorphasea);
        tv_legendarray.add(1, tvcolorphaseb);
        tv_legendarray.add(2, tvcolorphasec);
        tv_legendarray.add(3, tvcolorfreq);
        tv_legendarray.add(4, tvcolorpf);

        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currvoltGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        updateViewAfterFetch(axisval);
    }

    private void updateViewAfterFetch(AxisData axisval) {

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }

        tvdataphasea.setText(modeSend+" Phase-A");
        tvdataphaseb.setText(modeSend+" Phase-B");
        tvdataphasec.setText(modeSend+" Phase-C");

        updategraph(axisval);
    }

    private void updategraph(AxisData axisval) {
        ArrayList<Entry> phase_aEntries = new ArrayList<>();
        ArrayList<Entry> phase_bEntries = new ArrayList<>();
        ArrayList<Entry> phase_cEntries = new ArrayList<>();
        ArrayList<Entry> freqEntries = new ArrayList<>();
        ArrayList<Entry> pfEntries = new ArrayList<>();

        int blockLen = axisval.getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(axisval.getAxisval().getBlock().get(index));
            phase_aEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getPhaseA().get(index))), index));
            phase_bEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getPhaseB().get(index))), index));
            phase_cEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getPhaseC().get(index))), index));
            freqEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getFreq().get(index))), index));
            pfEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getPf().get(index))), index));

        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, phase_aEntries, "", AppConstants.CURRVOLT_COLOR_PHASEA, false);
        setGraphVales(dataSets, phase_bEntries, "", AppConstants.CURRVOLT_COLOR_PHASEB, false);
        setGraphVales(dataSets, phase_cEntries, "", AppConstants.CURRVOLT_COLOR_PHASEC, false);
        setGraphVales(dataSets, freqEntries, "", AppConstants.CURRVOLT_COLOR_FREQ, false);
        setGraphVales(dataSets, pfEntries, " ", AppConstants.CURRVOLT_COLOR_PF, false);


        globalLinedata = new LineData(labels, dataSets);


        globalLinedata.setHighlightEnabled(true);
        graph_landscapecurrvolt.setData(globalLinedata);
        graph_landscapecurrvolt.animateY(5000);
        graph_landscapecurrvolt.setDrawGridBackground(false);
        graph_landscapecurrvolt.setDescription(axisval.getAxisname());
        graph_landscapecurrvolt.getLegend().setEnabled(false);
        YAxis axisRight = graph_landscapecurrvolt.getAxisRight();
        axisRight.setDrawLabels(false);

       /*  YAxis axisLeft = graph_power.getAxisLeft();
        axisLeft.setDrawLabels(false);

        XAxis axis = graph_power.getXAxis();
        axis.setDrawLabels(false);*/

        lvcurrvoltdata.setVisibility(View.VISIBLE);
    }
    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }

    private void updateGraphValues(int position) {

        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < currvoltGraphBoolean.length; index++) {
            if (index == position) {
                currvoltGraphBoolean[index] = !currvoltGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < currvoltGraphBoolean.length; index++) {
            if (currvoltGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_landscapecurrvolt.setData(temp);
        graph_landscapecurrvolt.animateY(5000);
    }
    private void resetGraphValues() {
        for(int index=0;index<currvoltGraphBoolean.length;index++){
            currvoltGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<currvoltGraphBoolean.length;index++){
            if(currvoltGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }
    @Override
    public void onClick(View view) {
        if (view == llcurrentbackarrow){
            finish();
        }

    }
}
