package com.smarttech.mittals.adapters;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.handler.IDataPasser;
import com.smarttech.mittals.updatemodel.underdrawl.MeterDetail;
import com.smarttech.mittals.utils.AppConstants;

import java.util.List;

/**
 * Created by upama on 29/6/17.
 */

public class Adapter_UnderdrawlMeters extends RecyclerView.Adapter<Adapter_UnderdrawlMeters.MyViewHolder> {


    private List<MeterDetail> meterDetailList;
    private IDataPasser dataPasser;

    public Adapter_UnderdrawlMeters(IDataPasser _dashfrag, List<MeterDetail> _meterDetailList) {
        this.meterDetailList = _meterDetailList;
        dataPasser = _dashfrag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_underdrawl_meters, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MeterDetail _meter = meterDetailList.get(position);

        if (_meter.getChecked()) {
            holder.tv_state.setText(_meter.getMeterId());
            holder.tv_state.setBackgroundColor(AppConstants.CHECK_COLOR);
            holder.tv_state.setTextColor(Color.WHITE);
        } else {
            holder.tv_state.setText(_meter.getMeterId());
            holder.tv_state.setBackgroundColor(Color.WHITE);
            holder.tv_state.setTextColor(Color.BLACK);
        }

        holder.tv_state.setTag(position);
    }


    @Override
    public int getItemCount() {
        return meterDetailList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_state;

        public MyViewHolder(View view) {
            super(view);
            tv_state = (TextView) itemView.findViewById(R.id.cbx1);
            tv_state.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = (int) view.getTag();

            MeterDetail mDtl = meterDetailList.get(pos);

            mDtl.switchCheck();

            notifyDataSetChanged();

            dataPasser.callApi(null);
        }
    }

}

