
package com.smarttech.mittals.updatemodel.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rec implements Parcelable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("nonSolar")
    @Expose
    private List<NonSolar> nonSolar = null;
    @SerializedName("solar")
    @Expose
    private List<Solar> solar = null;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<NonSolar> getNonSolar() {
        return nonSolar;
    }

    public void setNonSolar(List<NonSolar> nonSolar) {
        this.nonSolar = nonSolar;
    }

    public List<Solar> getSolar() {
        return solar;
    }

    public void setSolar(List<Solar> solar) {
        this.solar = solar;
    }


    protected Rec(Parcel in) {
        date = in.readString();
        if (in.readByte() == 0x01) {
            nonSolar = new ArrayList<NonSolar>();
            in.readList(nonSolar, NonSolar.class.getClassLoader());
        } else {
            nonSolar = null;
        }
        if (in.readByte() == 0x01) {
            solar = new ArrayList<Solar>();
            in.readList(solar, Solar.class.getClassLoader());
        } else {
            solar = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        if (nonSolar == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(nonSolar);
        }
        if (solar == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(solar);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Rec> CREATOR = new Parcelable.Creator<Rec>() {
        @Override
        public Rec createFromParcel(Parcel in) {
            return new Rec(in);
        }

        @Override
        public Rec[] newArray(int size) {
            return new Rec[size];
        }
    };
}