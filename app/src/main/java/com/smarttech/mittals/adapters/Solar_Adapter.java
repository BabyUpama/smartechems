package com.smarttech.mittals.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.dashboard.Solar;

import java.util.List;


/**
 * Created by Ashish Karn on 28-11-2016.
 */

public class Solar_Adapter extends RecyclerView.Adapter<Solar_Adapter.ViewHolder> {
    private List<Solar> solarList;
    private boolean isData=true;

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvpurchased,tvpurchasednum;

        public ViewHolder(View v) {
            super(v);
            tvpurchased =  (TextView) v.findViewById(R.id.tvpurchased);
            tvpurchasednum =  (TextView) v.findViewById(R.id.tvpurchasednum);

        }
    }



    public Solar_Adapter(List<Solar> _solarlist){
        this.solarList = _solarlist;
        if(solarList== null){
            isData=false;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_solar, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(isData) {
             position = position % solarList.size();
            ViewHolder myHolder= (ViewHolder) holder;
            Solar solar=solarList.get(position);
            myHolder.tvpurchased.setText(solar.getName());
            myHolder.tvpurchasednum.setText(solar.getVal());

        }

    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }
}