package com.smarttech.mittals.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.averagepower.AxisData;
import com.smarttech.mittals.utils.AppConstants;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * Created by upama on 26/12/16.
 */

public class Average_landscape_activity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout llaveragebackarrow;
    private String modeSend;
    private CombinedChart graph_landscapeavg;
    private LinearLayout lvavgdata;
    private TextView tvdataexc,tvdatabil,tvdataseb,tvcolorexc,tvcolorbil,tvcolorseb,tvcoloravgcost;
    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] avgGraphBoolean = {true,true,true,true};
    private String[] colors_array = {"#056832", "#00b050", "#99ff99", "#ff0000"};
    private String COLOR_SEB="#056832";
    private String COLOR_BIL="#00b050";
    private String COLOR_EXC="#99ff99";
    private DecimalFormat mFormat;
    AxisData axisval=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.average_landscape_activity);

        Intent in = this.getIntent();
        if (in != null) {
            modeSend = in.getStringExtra("modeSend");
            axisval = in.getParcelableExtra(AppConstants.APP_DATA);
        }

        graph_landscapeavg = (CombinedChart) findViewById(R.id.graph_landscapeavg);

        lvavgdata = (LinearLayout) findViewById(R.id.lvavgdata);
        LinearLayout lvavgexc = (LinearLayout)findViewById(R.id.lvavgexc);
        tvcolorexc = (TextView) findViewById(R.id.tvcolorexc);
        tvdataexc = (TextView) findViewById(R.id.tvdataexc);

        LinearLayout lvavgbil = (LinearLayout) findViewById(R.id.lvavgbil);
        tvcolorbil = (TextView) findViewById(R.id.tvcolorbil);
        tvdatabil = (TextView) findViewById(R.id.tvdatabil);

        LinearLayout lvavgseb = (LinearLayout) findViewById(R.id.lvavgseb);
        tvcolorseb = (TextView) findViewById(R.id.tvcolorseb);
        tvdataseb = (TextView) findViewById(R.id.tvdataseb);

        LinearLayout lvavgcost = (LinearLayout) findViewById(R.id.lvavgcost);
        tvcoloravgcost = (TextView) findViewById(R.id.tvcoloravgcost);
        TextView tvdataavgcost = (TextView) findViewById(R.id.tvdataavgcost);
        graph_legendLayout.add(0, lvavgexc);
        graph_legendLayout.add(1, lvavgbil);
        graph_legendLayout.add(2, lvavgseb);
        graph_legendLayout.add(3, lvavgcost);
        tv_legendarray.add(0, tvcolorexc);
        tv_legendarray.add(1, tvcolorbil);
        tv_legendarray.add(2, tvcolorseb);
        tv_legendarray.add(3, tvcoloravgcost);


        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (avgGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }

        updateViewAfterFetch(axisval);

        llaveragebackarrow = (LinearLayout)findViewById(R.id.llaveragebackarrow);

        llaveragebackarrow.setOnClickListener(this);


    }

    private void updateViewAfterFetch(AxisData axisval) {

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
        tvdataexc.setText("Percentage of power source from Exchange ("+ modeSend +")");
        tvdatabil.setText("Percentage of power source from Bilateral ("+ modeSend +")");
        tvdataseb.setText("Percentage of power source from SEB ("+ modeSend +")");

        updateGraph(axisval);
    }

    private void updateGraph(AxisData axisval) {
        ArrayList<String> xAxisValues=getXAxisValues();

        BarData barData = new BarData(xAxisValues, getDataSet());

        CombinedData cdata = new CombinedData(xAxisValues);
        cdata.setData(barData);
        cdata.setData(generatelineData());

        graph_landscapeavg.setData(cdata);
        graph_landscapeavg.setDescription(axisval.getAxisname());
        graph_landscapeavg.animateXY(2000, 2000);
        graph_landscapeavg.invalidate();
        graph_landscapeavg.getLegend().setEnabled(false);
        YAxis axisRight = graph_landscapeavg.getAxisRight();
        axisRight.setDrawLabels(false);

        lvavgdata.setVisibility(View.VISIBLE);

    }

    private void updateGraphValues(int position) {

        for (int index = 0; index < avgGraphBoolean.length; index++) {
            if (index == position) {
                avgGraphBoolean[index] = !avgGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        ArrayList<String> xAxisValues=getXAxisValues();
        BarData barData;

        CombinedData cdata = new CombinedData(getXAxisValues());

        if(avgGraphBoolean[0] && avgGraphBoolean[2] && avgGraphBoolean[3]){
            barData = new BarData(xAxisValues, getDataSet());
            cdata.setData(barData);
        }else if(avgGraphBoolean[0]){
            barData = new BarData(xAxisValues, updateExchangeArray());
            cdata.setData(barData);
        }else if(avgGraphBoolean[2]){
            barData = new BarData(xAxisValues, updateBilateralArray());
            cdata.setData(barData);
        }else if(avgGraphBoolean[3]){
            barData = new BarData(xAxisValues, updateSebArray());
            cdata.setData(barData);
        }

        if(avgGraphBoolean[1]){
            cdata.setData(generatelineData());
        }
        graph_landscapeavg.setData(cdata);
        graph_landscapeavg.setDescription(axisval.getAxisname());
        graph_landscapeavg.animateXY(2000, 2000);
        graph_landscapeavg.invalidate();
        graph_landscapeavg.getLegend().setEnabled(false);


    }
    private void resetGraphValues() {
        for(int index=0;index<avgGraphBoolean.length;index++){
            avgGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }

    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<avgGraphBoolean.length;index++){
            if(avgGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }
    private ArrayList<BarDataSet> updateSebArray(){

        ArrayList<BarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        for(int index=0;index<axisval.getAxisval().getBlock().size();index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(axisval.getAxisval().getPerSeb().get(index))) }, index);
            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 3");
        barDataSet1.setColors(new int[]{Color.parseColor(COLOR_SEB)});

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);

        return dataSets;
    }
    private ArrayList<BarDataSet> updateBilateralArray(){

        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        for(int index=0;index<axisval.getAxisval().getBlock().size();index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(axisval.getAxisval().getPerBil().get(index))) }, index);
            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 2");
        barDataSet1.setColors(new int[]{Color.parseColor(COLOR_BIL)});

        dataSets.add(barDataSet1);

        return dataSets;
    }
    private ArrayList<BarDataSet> updateExchangeArray(){

        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        for(int index=0;index<axisval.getAxisval().getBlock().size();index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(axisval.getAxisval().getPerExc().get(index))) }, index);
            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 1");
        barDataSet1.setColors(new int[]{Color.parseColor(COLOR_EXC)});

        dataSets.add(barDataSet1);

        return dataSets;
    }
    private LineData generatelineData() {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        for(int index=0;index<axisval.getAxisval().getAvgCost().size();index++) {
            labels.add(axisval.getAxisval().getBlock().get(index));
            entries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getAvgCost().get(index))),index));
        }

        LineDataSet dataset = new LineDataSet(entries, "# of Calls");
        dataset.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

                mFormat = new DecimalFormat("###,###,###,##0.000" + "");
                return mFormat.format(value);
            }
        });
        dataset.setColor(Color.parseColor("#ff0000"));
        dataset.setCircleColor(Color.parseColor("#ff0000"));
        dataset.setFillColor(Color.parseColor("#ff0000"));
        dataset.setCircleColorHole(Color.parseColor("#ff0000"));
        dataset.setCircleSize(3f);

        LineData data = new LineData(labels, dataset);

        return data;
    }

    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        for(int index=0;index<axisval.getAxisval().getBlock().size();index++) {
            BarEntry v1e1 = new BarEntry(new float[]{ Float.parseFloat(String.valueOf(axisval.getAxisval().getPerSeb().get(index))),
                    Float.parseFloat(String.valueOf(axisval.getAxisval().getPerExc().get(index))),
                    Float.parseFloat(String.valueOf(axisval.getAxisval().getPerBil().get(index))) }, index);
            valueSet1.add(v1e1);
        }



        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 3");

        if(!axisval.getAxisval().getPerSeb().isEmpty() & !axisval.getAxisval().getPerBil().isEmpty() & !axisval.getAxisval().getPerExc().isEmpty()) {
            barDataSet1.setColors(new int[]{Color.parseColor("#056832"), Color.parseColor("#00b050"),Color.parseColor("#99ff99")});
        }
        else if(!axisval.getAxisval().getPerSeb().isEmpty() & axisval.getAxisval().getPerBil().isEmpty()&axisval.getAxisval().getPerExc().isEmpty()) {
            barDataSet1.setColors(new int[]{Color.parseColor("#056832")});
        }
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        for(int index=0;index<axisval.getAxisval().getBlock().size();index++) {
            xAxis.add(axisval.getAxisval().getBlock().get(index));
        }
        return xAxis;
    }


    @Override
    public void onClick(View view) {
        finish();

    }
}

