package com.smarttech.mittals.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.updatemodel.powerprocured.AxisData;
import com.smarttech.mittals.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by upama on 26/12/16.
 */

public class Powerprocured_landscape_activity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout llpowerbackarrow,lvpowerprocureddata,lvpowerprocureddataone;
    private LineChart graph_landscapepower;
    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] powerGraphBoolean = {true,true,true,true,true,true,true};
    String[] colors_array = {"#fbc1d9", "#7cb5ec", "#90ed7d", "#0000ff", "#434348","#306824","#ff0000"};
    private LineData globalLinedata;
    private String modeSend;
    private TextView tvdatatpc,tvdatatpr,tvdataclrd,tvdataiex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.powerprocured_landscape_activity);

        AxisData axisval=null;

        Intent in = this.getIntent();
        if (in != null) {
            modeSend = in.getStringExtra("modeSend");
            axisval = in.getParcelableExtra(AppConstants.APP_DATA);
        }
        graph_landscapepower=(LineChart) findViewById(R.id.graph_landscapepower);
        llpowerbackarrow = (LinearLayout)findViewById(R.id.llpowerbackarrow);

        llpowerbackarrow.setOnClickListener(this);

        lvpowerprocureddata = (LinearLayout) findViewById(R.id.lvpowerprocureddata);
        LinearLayout lvpowerprocuredtpc = (LinearLayout) findViewById(R.id.lvpowerprocuredtpc);
        TextView tvcolortpc = (TextView) findViewById(R.id.tvcolortpc);
         tvdatatpc = (TextView) findViewById(R.id.tvdatatpc);

        LinearLayout lvpowerprocuredtpr = (LinearLayout) findViewById(R.id.lvpowerprocuredtpr);
        TextView tvcolortpr = (TextView) findViewById(R.id.tvcolortpr);
         tvdatatpr = (TextView) findViewById(R.id.tvdatatpr);

        LinearLayout lvpowerprocuredbq = (LinearLayout) findViewById(R.id.lvpowerprocuredbq);
        TextView tvcolorbq = (TextView) findViewById(R.id.tvcolorbq);
        TextView tvdatabq = (TextView) findViewById(R.id.tvdatabq);

        LinearLayout lvpowerprocuredbp = (LinearLayout) findViewById(R.id.lvpowerprocuredbp);
        TextView tvcolorbp = (TextView) findViewById(R.id.tvcolorbp);
        TextView tvdatabp = (TextView) findViewById(R.id.tvdatabp);

        lvpowerprocureddataone = (LinearLayout) findViewById(R.id.lvpowerprocureddataone);
        LinearLayout lvpowerprocuredclrd = (LinearLayout) findViewById(R.id.lvpowerprocuredclrd);
        TextView tvcolorclrd = (TextView) findViewById(R.id.tvcolorclrd);
         tvdataclrd = (TextView) findViewById(R.id.tvdataclrd);

        LinearLayout lvpowerprocurediex = (LinearLayout) findViewById(R.id.lvpowerprocurediex);
        TextView tvcoloriex = (TextView) findViewById(R.id.tvcoloriex);
         tvdataiex = (TextView) findViewById(R.id.tvdataiex);

        LinearLayout lvpowerprocuredcd = (LinearLayout) findViewById(R.id.lvpowerprocuredcd);
        TextView tvcolorcd = (TextView) findViewById(R.id.tvcolorcd);
        TextView tvdatacd = (TextView) findViewById(R.id.tvdatacd);
        graph_legendLayout.add(0, lvpowerprocuredtpc);
        graph_legendLayout.add(1, lvpowerprocuredtpr);
        graph_legendLayout.add(2, lvpowerprocuredbq);
        graph_legendLayout.add(3, lvpowerprocuredbp);
        graph_legendLayout.add(4, lvpowerprocuredclrd);
        graph_legendLayout.add(5, lvpowerprocurediex);
        graph_legendLayout.add(6, lvpowerprocuredcd);

        tv_legendarray.add(0, tvcolortpc);
        tv_legendarray.add(1, tvcolortpr);
        tv_legendarray.add(2, tvcolorbq);
        tv_legendarray.add(3, tvcolorbp);
        tv_legendarray.add(4, tvcolorclrd);
        tv_legendarray.add(5, tvcoloriex);
        tv_legendarray.add(6, tvcolorcd);

        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (powerGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        updateViewAfterFetch(axisval);

    }

    private void updateViewAfterFetch(AxisData axisval) {

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
        tvdatatpc.setText("Total MeterDetail Consumption ("+ modeSend +")");
        tvdatatpr.setText("Total MeterDetail Recived at meter pherphery ("+ modeSend +")");
        tvdataclrd.setText("Cleared Quantum From Exchange ("+ modeSend +")");
        tvdataiex.setText("IEX Area Clearing Price ("+ modeSend +")" );
        updategraph(axisval);
    }

    private void updategraph(AxisData axisval) {
        ArrayList<Entry> ttl_pcEntries = new ArrayList<>();
        ArrayList<Entry> ttl_recEntries = new ArrayList<>();
        ArrayList<Entry> bid_quntEntries = new ArrayList<>();
        ArrayList<Entry> bid_priceEntries = new ArrayList<>();
        ArrayList<Entry> clr_quantEntries = new ArrayList<>();
        ArrayList<Entry> iex_clrEntries = new ArrayList<>();
        ArrayList<Entry> cdEntries = new ArrayList<>();


        int blockLen = axisval.getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(axisval.getAxisval().getBlock().get(index));
            ttl_pcEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getTtlPc().get(index))), index));
            ttl_recEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getTtlRec().get(index))), index));
            bid_quntEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getBidQunt().get(index))), index));
            bid_priceEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getBidPrice().get(index))), index));
            clr_quantEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getClrQuant().get(index))), index));
            iex_clrEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getIexClr().get(index))), index));
            cdEntries.add(new Entry(Float.parseFloat(String.valueOf(axisval.getAxisval().getCd().get(index))), index));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, ttl_pcEntries, "", AppConstants.POWER_COLOR_TOTALPOWERCONSUMPTION, true);
        setGraphVales(dataSets, ttl_recEntries, "", AppConstants.POWER_COLOR_TOTALPOWERRECIEVED, false);
        setGraphVales(dataSets, bid_quntEntries, "", AppConstants.POWER_COLOR_BIDQUANT, false);
        setGraphVales(dataSets, bid_priceEntries, "", AppConstants.POWER_COLOR_BIDPRICE, false);
        setGraphVales(dataSets, clr_quantEntries, " ", AppConstants.POWER_COLOR_CLEAREDQUANT, false);
        setGraphVales(dataSets, iex_clrEntries, "", AppConstants.POWER_COLOR_IEXCLEAR, false);
        setGraphVales(dataSets, cdEntries, " ", AppConstants.POWER_COLOR_CD, false);

        globalLinedata = new LineData(labels, dataSets);


        globalLinedata.setHighlightEnabled(true);
        graph_landscapepower.setData(globalLinedata);
        graph_landscapepower.animateY(5000);
        graph_landscapepower.setDrawGridBackground(false);
        graph_landscapepower.setDescription(axisval.getAxisname());
        graph_landscapepower.getLegend().setEnabled(false);

        lvpowerprocureddata.setVisibility(View.VISIBLE);
        lvpowerprocureddataone.setVisibility(View.VISIBLE);

    }
    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }

    private void updateGraphValues(int position) {
        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < powerGraphBoolean.length; index++) {
            if (index == position) {
                powerGraphBoolean[index] = !powerGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < powerGraphBoolean.length; index++) {
            if (powerGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_landscapepower.setData(temp);
        graph_landscapepower.animateY(5000);
    }
    private void resetGraphValues() {
        for(int index=0;index<powerGraphBoolean.length;index++){
            powerGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<powerGraphBoolean.length;index++){
            if(powerGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        if(view == llpowerbackarrow){
            finish();
        }

    }
}

