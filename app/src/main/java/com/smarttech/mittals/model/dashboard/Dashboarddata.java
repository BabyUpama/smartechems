
package com.smarttech.mittals.model.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dashboarddata implements Parcelable {

    @SerializedName("meters")
    @Expose
    private List<Meter> meters = null;
    @SerializedName("rec")
    @Expose
    private Rec rec;
    @SerializedName("graph_data")
    @Expose
    private GraphData graphData;
    @SerializedName("stats")
    @Expose
    private List<Stat_> stats = null;

    public List<Meter> getMeters() {
        return meters;
    }

    public void setMeters(List<Meter> meters) {
        this.meters = meters;
    }

    public Rec getRec() {
        return rec;
    }

    public void setRec(Rec rec) {
        this.rec = rec;
    }

    public GraphData getGraphData() {
        return graphData;
    }

    public void setGraphData(GraphData graphData) {
        this.graphData = graphData;
    }

    public List<Stat_> getStats() {
        return stats;
    }

    public void setStats(List<Stat_> stats) {
        this.stats = stats;
    }


    protected Dashboarddata(Parcel in) {
        if (in.readByte() == 0x01) {
            meters = new ArrayList<Meter>();
            in.readList(meters, Meter.class.getClassLoader());
        } else {
            meters = null;
        }
        rec = (Rec) in.readValue(Rec.class.getClassLoader());
        graphData = (GraphData) in.readValue(GraphData.class.getClassLoader());
        if (in.readByte() == 0x01) {
            stats = new ArrayList<Stat_>();
            in.readList(stats, Stat_.class.getClassLoader());
        } else {
            stats = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (meters == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(meters);
        }
        dest.writeValue(rec);
        dest.writeValue(graphData);
        if (stats == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(stats);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Dashboarddata> CREATOR = new Parcelable.Creator<Dashboarddata>() {
        @Override
        public Dashboarddata createFromParcel(Parcel in) {
            return new Dashboarddata(in);
        }

        @Override
        public Dashboarddata[] newArray(int size) {
            return new Dashboarddata[size];
        }
    };
}