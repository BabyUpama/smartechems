package com.smarttech.mittals.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.appbase.AppBaseActivity;
import com.smarttech.mittals.db.SharedPrefHandler;
import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;
import com.smarttech.mittals.utils.DeviceInfoHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


/**
 * Created by babyu on 29-02-2016.
 */
public class LoginActivity extends AppBaseActivity implements View.OnClickListener/*,GoogleApiClient.OnConnectionFailedListener*/ {

    private CheckBox saveloginchkbox;
    private EditText etusername, etpassword;
    private ImageView iv_hide_eye, iv_visible_eye;

    /*private CallbackManager callbackManager;
    private LoginManager fbLoginManager;*/
   // LoginButton login;
    ImageView logingmail;
    private LoginData data = null;
    //private GoogleApiClient mGoogleApiClient = null;
    private int RC_SIGN_IN = 2800;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);*/
        setContentView(R.layout.activity_login);
        /*login = (LoginButton)findViewById(R.id.login_button);
        login.setOnClickListener(this);*/
        //logingmail = (ImageView) findViewById(R.id.logingmail);
        //logingmail.setOnClickListener(this);
        etusername = (EditText) findViewById(R.id.etusername);
        etpassword = (EditText) findViewById(R.id.etpassword);
        Button btnsign = (Button) findViewById(R.id.btnsign);
        iv_visible_eye = (ImageView) findViewById(R.id.iv_visible_eye);
        iv_hide_eye = (ImageView) findViewById(R.id.iv_hide_eye);
        iv_hide_eye.setOnClickListener(this);
        iv_visible_eye.setOnClickListener(this);

        saveloginchkbox = (CheckBox) findViewById(R.id.saveloginchkbox);

        btnsign.setOnClickListener(this);
        setLoginCred();
        //fbInit();

    }
   /* private void googleLoginClick(View view) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }*/
    /*private void fbLoginClick(View view) {

        fbLoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
    }

   private void fbInit() {
        callbackManager = CallbackManager.Factory.create();
        fbLoginManager = com.facebook.login.LoginManager.getInstance();
        fbLoginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                if(AccessToken.getCurrentAccessToken() != null){
                    getDataAfterLogin(loginResult);
                }
            }

            @Override
            public void onCancel() {
                AppLogger.showToastLarge(getApplicationContext(), "Operation Canceled.");
            }

            @Override
            public void onError(FacebookException exception) {
                if (exception instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                        AppLogger.showToastSmall(getApplicationContext(), "Please Again Click to Login.");
                    } else {
                        AppLogger.showToastSmall(getApplicationContext(), exception.getMessage());
                    }
                }
            }
        });
    }

    private void getDataAfterLogin(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {

                            String email = object.getString("email");
                            String name = object.getString("name");

                            AppLogger.showToastSmall(getApplicationContext(), name + "  " + email);

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(etusername.getWindowToken(), 0);

                            String username = etusername.getText().toString().trim();
                            String password = etpassword.getText().toString().trim();

                            if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                                AppLogger.showToastLarge(getBaseContext(), "Username / Password can not be empty.");
                                return;
                            }


                            doLogin(username, password);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,name,first_name,last_name,age_range,link");
        request.setParameters(parameters);
        request.executeAsync();
    }
*/


    private void setLoginCred() {
        boolean isLogin = SharedPrefHandler.getBoolean(this, AppConstants.DB_IS_LOGIN);

        if (isLogin) {
            etusername.setText(SharedPrefHandler.getString(this, "username"));
            etpassword.setText(SharedPrefHandler.getString(this, "password"));
            saveloginchkbox.setChecked(true);
        } else {

        }
    }

    private void doLogin(String username, String password) {

        JSONObject param = new JSONObject();
        try {
            param.put("username", username);
            param.put("password", password);
            param.put("device_id", DeviceInfoHelper.getDeviceId(this));
            param.put("version", "1.0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String[] str = new String[2];

        str[0] = AppConstants.URL_BASE + AppConstants.SUBURL_LOGIN;
        str[1] = param.toString();

        new NetworkHandlerModel(LoginActivity.this, callback, LoginData.class, 1).execute(str);
    }

    private INetworkCallback callback = new INetworkCallback() {
        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getBaseContext(), "Some Error Occurred");
                return;
            }

             data = (LoginData) obj;
            String str = data.getValue().getValue().getLogintype();

            if (str.equalsIgnoreCase("CLIENT")) {
                moveToNext(data);
            }
        }
    };

    private void moveToNext(LoginData data) {
        Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
        i.putExtra("data", data);

        startActivity(i);
        overridePendingTransition(R.anim.animation,
                R.anim.animation2);

        finish();
    }


    @Override
    public void onClick(View view) {

        if (view == iv_hide_eye) {

            etpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            iv_hide_eye.setVisibility(View.GONE);
            iv_visible_eye.setVisibility(View.VISIBLE);

        } else if (view == iv_visible_eye) {

            etpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            iv_visible_eye.setVisibility(View.GONE);
            iv_hide_eye.setVisibility(View.VISIBLE);

        } else if (view.getId() == R.id.btnsign) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etusername.getWindowToken(), 0);

            String username = etusername.getText().toString().trim();
            String password = etpassword.getText().toString().trim();

            if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                AppLogger.showToastLarge(getBaseContext(), "Username / Password can not be empty.");
                return;
            }

            if (saveloginchkbox.isChecked()) {
                SharedPrefHandler.saveBoolean(this, AppConstants.DB_IS_LOGIN, true);
                SharedPrefHandler.saveLoginCreadential(this, new String[]{username, password});
            } else {
                SharedPrefHandler.clearSharedPre(this);
            }
            doLogin(username, password);
        }
      /*if(view == login){
          fbLoginClick(view);
       }*/
    }


  /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
*/
   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
//            return;
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }*/
   /* private void handleSignInResult(GoogleSignInResult result) {
       // AppLogger.showMsg(result.isSuccess() + "");
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            AppLogger.showToastLarge(this, acct.getDisplayName() + "  " + acct.getEmail());

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etusername.getWindowToken(), 0);

            String username = etusername.getText().toString().trim();
            String password = etpassword.getText().toString().trim();

            if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                AppLogger.showToastLarge(getBaseContext(), "Username / Password can not be empty.");
                return;
            }


            doLogin(username, password);


        } else {

        }
    }*/

  /*  @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLogger.showToastLarge(this, "Gmail Failed");
    }*/
/*
    @Override
    public void onDestroy() {
        super.onDestroy();
        disConnectGmail();
    }

    private void disConnectGmail() {
        if (mGoogleApiClient != null) {

            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }*/
}

