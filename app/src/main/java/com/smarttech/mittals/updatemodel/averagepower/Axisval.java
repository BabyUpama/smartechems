
package com.smarttech.mittals.updatemodel.averagepower;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Axisval implements Parcelable {

    @SerializedName("per_exc")
    @Expose
    private List<String> perExc = null;
    @SerializedName("per_bil")
    @Expose
    private List<String> perBil = null;
    @SerializedName("per_seb")
    @Expose
    private List<String> perSeb = null;
    @SerializedName("block")
    @Expose
    private List<String> block = null;
    @SerializedName("avg_cost")
    @Expose
    private List<String> avgCost = null;

    public List<String> getPerExc() {
        return perExc;
    }

    public void setPerExc(List<String> perExc) {
        this.perExc = perExc;
    }

    public List<String> getPerBil() {
        return perBil;
    }

    public void setPerBil(List<String> perBil) {
        this.perBil = perBil;
    }

    public List<String> getPerSeb() {
        return perSeb;
    }

    public void setPerSeb(List<String> perSeb) {
        this.perSeb = perSeb;
    }

    public List<String> getBlock() {
        return block;
    }

    public void setBlock(List<String> block) {
        this.block = block;
    }

    public List<String> getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(List<String> avgCost) {
        this.avgCost = avgCost;
    }


    protected Axisval(Parcel in) {
        if (in.readByte() == 0x01) {
            perExc = new ArrayList<String>();
            in.readList(perExc, String.class.getClassLoader());
        } else {
            perExc = null;
        }
        if (in.readByte() == 0x01) {
            perBil = new ArrayList<String>();
            in.readList(perBil, String.class.getClassLoader());
        } else {
            perBil = null;
        }
        if (in.readByte() == 0x01) {
            perSeb = new ArrayList<String>();
            in.readList(perSeb, String.class.getClassLoader());
        } else {
            perSeb = null;
        }
        if (in.readByte() == 0x01) {
            block = new ArrayList<String>();
            in.readList(block, String.class.getClassLoader());
        } else {
            block = null;
        }
        if (in.readByte() == 0x01) {
            avgCost = new ArrayList<String>();
            in.readList(avgCost, String.class.getClassLoader());
        } else {
            avgCost = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (perExc == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(perExc);
        }
        if (perBil == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(perBil);
        }
        if (perSeb == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(perSeb);
        }
        if (block == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(block);
        }
        if (avgCost == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(avgCost);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Axisval> CREATOR = new Parcelable.Creator<Axisval>() {
        @Override
        public Axisval createFromParcel(Parcel in) {
            return new Axisval(in);
        }

        @Override
        public Axisval[] newArray(int size) {
            return new Axisval[size];
        }
    };
}