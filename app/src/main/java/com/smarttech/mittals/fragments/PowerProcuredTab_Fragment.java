package com.smarttech.mittals.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.gkravas.meterview.MeterView;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.activities.Powerprocured_landscape_activity;
import com.smarttech.mittals.adapters.Adapter_PowerMeters;
import com.smarttech.mittals.adapters.PowerProcuredAdapter;
import com.smarttech.mittals.appbase.AppBaseFragment;
import com.smarttech.mittals.handler.IDataPasser;
import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.updatemodel.powerprocured.PowerProcureddata;
import com.smarttech.mittals.updatemodel.powerprocured.MeterDetail;
import com.smarttech.mittals.updatemodel.powerprocured.Meters;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;


public class PowerProcuredTab_Fragment extends AppBaseFragment implements View.OnClickListener{

    View rowView;
    TextView dpResult;
    PowerProcuredAdapter horizontalAdptrpowerprocured;

    private int year;
    private int month;
    private int day;

    MeterView meterView;
    CheckBox cbx1, cbx2, cbx3, cbx4;

    private LoginData mData = null;
    private int fragPosition = -1;

    private RadioGroup materialradiogroup;
    private RadioButton materialradiokw,materialradiokwh;
    private String modeSend,datesend;
    private RecyclerView recyclerviewpower;
    private LineChart graph_power;
    private LinearLayout lvpowerprocureddata,lvpowerprocureddataone;
    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] powerGraphBoolean = {true,true,true,true,true,true,true};
    String[] colors_array = {"#fbc1d9", "#7cb5ec", "#90ed7d", "#0000ff", "#434348","#306824","#ff0000"};
    private LineData globalLinedata;
    private Button viewgraph_power;
    private PowerProcureddata data = null;
    private TextView tvdatatpc,tvdatatpr,tvdataclrd,tvdataiex;
    private String currentHospitalId=null;

    private RecyclerView rv_powermeters;
    private LinearLayoutManager mLayoutManager;
    private Adapter_PowerMeters mAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        currentHospitalId = args.getString(AppConstants.DATA_CURRENTID);

        fragPosition = getArguments().getInt(AppConstants.DATA_POSITION);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");

        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");

            return;
        }
        if(currentHospitalId==null){
            currentHospitalId=mData.getValue().getValue().getUserId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rowView= inflater.inflate(R.layout.fragment_powerprocured_tab, container, false);

//        cbx1 = (CheckBox) rowView.findViewById(R.id.cbx1);
//        cbx2 = (CheckBox) rowView.findViewById(R.id.cbx2);
//        cbx3 = (CheckBox) rowView.findViewById(R.id.cbx3);
//        cbx4 = (CheckBox) rowView.findViewById(R.id.cbx4);
//
//        cbx1.setOnClickListener(this);
//        cbx2.setOnClickListener(this);
//        cbx3.setOnClickListener(this);
//        cbx4.setOnClickListener(this);
        //cbx1.setBackground(getActivity().getResources().getDrawable(R.drawable.selectable_button));

        meterView=(MeterView) rowView.findViewById(R.id.meter_view);

        materialradiogroup = (RadioGroup) rowView.findViewById(R.id.materialradiogroup);
        materialradiokw = (RadioButton) rowView.findViewById(R.id.materialradiokw);
        materialradiogroup.check(materialradiokw.getId());
        modeSend="kw";
        materialradiokw.setOnClickListener(this);

        materialradiokwh = (RadioButton) rowView.findViewById(R.id.materialradiokwh);
        materialradiokwh.setOnClickListener(this);

        dpResult=(TextView) rowView.findViewById(R.id.datePicker_realtime);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 0);
        Date result = c.getTime();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        datesend = df.format(c.getTime());
        dpResult.setText(datesend);
        dpResult.setOnClickListener(this);

        recyclerviewpower = (RecyclerView) rowView.findViewById(R.id.recyclerviewpower);

        graph_power=(LineChart) rowView.findViewById(R.id.graph_power);

        lvpowerprocureddata = (LinearLayout) rowView.findViewById(R.id.lvpowerprocureddata);
        LinearLayout lvpowerprocuredtpc = (LinearLayout) rowView.findViewById(R.id.lvpowerprocuredtpc);
        TextView tvcolortpc = (TextView) rowView.findViewById(R.id.tvcolortpc);
        tvdatatpc = (TextView) rowView.findViewById(R.id.tvdatatpc);

        LinearLayout lvpowerprocuredtpr = (LinearLayout) rowView.findViewById(R.id.lvpowerprocuredtpr);
        TextView tvcolortpr = (TextView) rowView.findViewById(R.id.tvcolortpr);
         tvdatatpr = (TextView) rowView.findViewById(R.id.tvdatatpr);

        LinearLayout lvpowerprocuredbq = (LinearLayout) rowView.findViewById(R.id.lvpowerprocuredbq);
        TextView tvcolorbq = (TextView) rowView.findViewById(R.id.tvcolorbq);
        TextView tvdatabq = (TextView) rowView.findViewById(R.id.tvdatabq);

        LinearLayout lvpowerprocuredbp = (LinearLayout) rowView.findViewById(R.id.lvpowerprocuredbp);
        TextView tvcolorbp = (TextView) rowView.findViewById(R.id.tvcolorbp);
        TextView tvdatabp = (TextView) rowView.findViewById(R.id.tvdatabp);

         lvpowerprocureddataone = (LinearLayout) rowView.findViewById(R.id.lvpowerprocureddataone);
        LinearLayout lvpowerprocuredclrd = (LinearLayout) rowView.findViewById(R.id.lvpowerprocuredclrd);
        TextView tvcolorclrd = (TextView) rowView.findViewById(R.id.tvcolorclrd);
         tvdataclrd = (TextView) rowView.findViewById(R.id.tvdataclrd);

        LinearLayout lvpowerprocurediex = (LinearLayout) rowView.findViewById(R.id.lvpowerprocurediex);
        TextView tvcoloriex = (TextView) rowView.findViewById(R.id.tvcoloriex);
         tvdataiex = (TextView) rowView.findViewById(R.id.tvdataiex);

        LinearLayout lvpowerprocuredcd = (LinearLayout) rowView.findViewById(R.id.lvpowerprocuredcd);
        TextView tvcolorcd = (TextView) rowView.findViewById(R.id.tvcolorcd);
        TextView tvdatacd = (TextView) rowView.findViewById(R.id.tvdatacd);

        rv_powermeters = (RecyclerView) rowView.findViewById(R.id.rv_powermeters);

        viewgraph_power=(Button) rowView.findViewById(R.id.viewgraph_power);
        viewgraph_power.setOnClickListener(this);

        graph_legendLayout.add(0, lvpowerprocuredtpc);
        graph_legendLayout.add(1, lvpowerprocuredtpr);
        graph_legendLayout.add(2, lvpowerprocuredbq);
        graph_legendLayout.add(3, lvpowerprocuredbp);
        graph_legendLayout.add(4, lvpowerprocuredclrd);
        graph_legendLayout.add(5, lvpowerprocurediex);
        graph_legendLayout.add(6, lvpowerprocuredcd);

        tv_legendarray.add(0, tvcolortpc);
        tv_legendarray.add(1, tvcolortpr);
        tv_legendarray.add(2, tvcolorbq);
        tv_legendarray.add(3, tvcolorbp);
        tv_legendarray.add(4, tvcolorclrd);
        tv_legendarray.add(5, tvcoloriex);
        tv_legendarray.add(6, tvcolorcd);

        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (powerGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        setPowerprocured(null);

        return rowView;
    }



    private void setPowerprocured(JSONObject object) {

        JSONObject param = new JSONObject();
        try {
            param.put("graph_id", mData.getValue().getMenu().get(fragPosition).getGraphId());
            param.put("user_id", currentHospitalId);
            param.put("access_key", mData.getValue().getValue().getAccessKey());
            param.put("date", datesend);
            param.put("mode",modeSend);
            if (object == null) {
                param.put("meters_id", "");
            } else {
                param.put("meters_id", object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = AppConstants.URL_BASE + AppConstants.SUBURL_COMMON;
        str[1] = param.toString();

        new NetworkHandlerModel(getActivity(), callback, PowerProcureddata.class, 1).execute(str);

    }
    private void setPowerprocured() throws JSONException {

//        JSONObject object = new JSONObject();
//        JSONArray aray = new JSONArray();
//        JSONObject meterObject = new JSONObject();
//        meterObject.put("id", "1");
//        meterObject.put("meter_id", "FMRI - 001");
//        meterObject.put("checked", true);
//        meterObject.put("status", "checked");
//        aray.put(meterObject);
//
//        object.put("meters", aray);
//
//        setPowerprocured(object);

        JSONObject paramObject = new JSONObject();

        Meters currentMeters=data.getMeters();

        if (currentMeters != null) {
            JSONArray powerArray = new JSONArray();

            if (currentMeters.getPower() != null && !currentMeters.getPower().isEmpty()) {

                List<MeterDetail> pList = currentMeters.getPower();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId());
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }


                    powerArray.put(tmpObject);
                }
                paramObject.put("power", powerArray);
            }

            if (currentMeters.getSolar() != null && !currentMeters.getSolar().isEmpty()) {
                List<MeterDetail> pList = currentMeters.getSolar();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId());
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }

                    powerArray.put(tmpObject);
                }
                paramObject.put("solar", powerArray);
            }

            setPowerprocured(paramObject);

        } else {
            AppLogger.showToastSmall(getActivity().getApplicationContext(), "Current Meter is NULL.");
        }
    }
    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getActivity(), "Some Error Occurred");
                return;
            }

             data = (PowerProcureddata) obj;

             updateViewAfterFetch(data);

             setCheckBox(data);


        }

    };


    private void setCheckBox(PowerProcureddata data) {

//       CheckBox[] checkArr={cbx1,cbx2,cbx3,cbx4};
//
//        List<Meter> variable = data.getMeters();
//
//        int siz = Math.min(variable.size(),checkArr.length);
//
//        for(int i = 0;i<siz; i++){
//
//            Meter mtr = variable.get(i);
//
//            if (mtr.getChecked() == true) {
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setBackgroundColor(AppConstants.CHECK_COLOR);
//                checkArr[i].setTextColor(Color.WHITE);
//                checkArr[i].setChecked(true);
//                checkArr[i].setVisibility(View.VISIBLE);
//            } else if(mtr.getChecked() == false) {
//                checkArr[i].setBackgroundColor(Color.WHITE);
//                //checkArr[i].setTextColor(Color.parseColor("#737272"));
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setChecked(false);
//                checkArr[i].setVisibility(View.VISIBLE);
//            }
//            else{
//                checkArr[i].setVisibility(View.GONE);
//            }
//        }

        rv_powermeters.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_powermeters.setLayoutManager(mLayoutManager);

        if(!data.getMeters().getPower().isEmpty()){
            mAdapter = new Adapter_PowerMeters(dataPasser, data.getMeters().getPower());
            rv_powermeters.setAdapter(mAdapter);
        }
        else if(!data.getMeters().getSolar().isEmpty()){
            mAdapter = new Adapter_PowerMeters(dataPasser, data.getMeters().getSolar());
            rv_powermeters.setAdapter(mAdapter);
        }
    }
    private void updateViewAfterFetch(PowerProcureddata data) {

        horizontalAdptrpowerprocured = new PowerProcuredAdapter(data.getGraphData().getStats());
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerviewpower.setLayoutManager(horizontalLayoutManagaer);
        recyclerviewpower.setAdapter(horizontalAdptrpowerprocured);

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }

        tvdatatpc.setText("Total MeterDetail Consumption ("+ modeSend +")");
        tvdatatpr.setText("Total MeterDetail Recived at meter pherphery ("+ modeSend +")");
        tvdataclrd.setText("Cleared Quantum From Exchange ("+ modeSend +")");
        tvdataiex.setText("IEX Area Clearing Price ("+ modeSend +")" );

        updategraph(data);
    }



    private void updategraph(PowerProcureddata data) {

        ArrayList<Entry> ttl_pcEntries = new ArrayList<>();
        ArrayList<Entry> ttl_recEntries = new ArrayList<>();
        ArrayList<Entry> bid_quntEntries = new ArrayList<>();
        ArrayList<Entry> bid_priceEntries = new ArrayList<>();
        ArrayList<Entry> clr_quantEntries = new ArrayList<>();
        ArrayList<Entry> iex_clrEntries = new ArrayList<>();
        ArrayList<Entry> cdEntries = new ArrayList<>();


        int blockLen = data.getGraphData().getAxisData().getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(data.getGraphData().getAxisData().getAxisval().getBlock().get(index));
            ttl_pcEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getTtlPc().get(index))), index));
            ttl_recEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getTtlRec().get(index))), index));
            bid_quntEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getBidQunt().get(index))), index));
            bid_priceEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getBidPrice().get(index))), index));
            clr_quantEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getClrQuant().get(index))), index));
            iex_clrEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getIexClr().get(index))), index));
            cdEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getCd().get(index))), index));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, ttl_pcEntries, "", AppConstants.POWER_COLOR_TOTALPOWERCONSUMPTION, true);
        setGraphVales(dataSets, ttl_recEntries, "", AppConstants.POWER_COLOR_TOTALPOWERRECIEVED, false);
        setGraphVales(dataSets, bid_quntEntries, "", AppConstants.POWER_COLOR_BIDQUANT, false);
        setGraphVales(dataSets, bid_priceEntries, "", AppConstants.POWER_COLOR_BIDPRICE, false);
        setGraphVales(dataSets, clr_quantEntries, " ", AppConstants.POWER_COLOR_CLEAREDQUANT, false);
        setGraphVales(dataSets, iex_clrEntries, "", AppConstants.POWER_COLOR_IEXCLEAR, false);
        setGraphVales(dataSets, cdEntries, " ", AppConstants.POWER_COLOR_CD, false);

        globalLinedata = new LineData(labels, dataSets);


        globalLinedata.setHighlightEnabled(true);
        graph_power.setData(globalLinedata);
        graph_power.animateY(5000);
        graph_power.setDrawGridBackground(false);
        graph_power.setDescription(data.getGraphData().getAxisData().getAxisname());
        graph_power.getLegend().setEnabled(false);
        YAxis axisRight = graph_power.getAxisRight();
        axisRight.setDrawLabels(false);

        /*YAxis axisLeft = graph_power.getAxisLeft();
        axisLeft.setDrawLabels(false);

        XAxis axis = graph_power.getXAxis();
        axis.setDrawLabels(false);
*/
        lvpowerprocureddata.setVisibility(View.VISIBLE);
        lvpowerprocureddataone.setVisibility(View.VISIBLE);

    }


    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }
    private void updateGraphValues(int position) {

        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < powerGraphBoolean.length; index++) {
            if (index == position) {
                powerGraphBoolean[index] = !powerGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < powerGraphBoolean.length; index++) {
            if (powerGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_power.setData(temp);
        graph_power.animateY(5000);
    }
    private void resetGraphValues() {
        for(int index=0;index<powerGraphBoolean.length;index++){
            powerGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<powerGraphBoolean.length;index++){
            if(powerGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        if(view == materialradiokw){
            modeSend="kw";
            try {
                setPowerprocured();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(view == materialradiokwh){
            modeSend="kwh";
            try {
                setPowerprocured();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        if(view == dpResult){

            Calendar mcurrentDate=Calendar.getInstance();
            year=mcurrentDate.get(Calendar.YEAR);
            month=mcurrentDate.get(Calendar.MONTH);
            day=mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    year = selectedyear;
                    month = selectedmonth;
                    day = selectedday;

                    // set selected date into textview
                    dpResult.setText(new StringBuilder().append(day)
                            .append("-").append(month + 1).append("-").append(year)
                            .append(" "));

                    // set selected date into datepicker also
                    datesend = dpResult.getText().toString();
                    try {
                        setPowerprocured();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();

        }
        if(view == viewgraph_power){
            Intent i=new Intent(getActivity(), Powerprocured_landscape_activity.class);
            i.putExtra("modeSend",modeSend);
            i.putExtra(AppConstants.APP_DATA,data.getGraphData().getAxisData());
            startActivity(i);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        final float min = 0;
        final float max = 54;
        final Random random = new Random();

        Observable.interval(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<Long, Float>() {
                    @Override
                    public Float call(Long aLong) {
                        return min + (random.nextFloat() * (max - min));
                    }
                })
                .subscribe(new Action1<Float>() {
                    @Override
                    public void call(Float value) {
                        meterView.setValue(value);
                    }
                });
    }

    private IDataPasser dataPasser=new IDataPasser() {

        @Override
        public void callApi(Object obj) {
            try {
                setPowerprocured();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    }
