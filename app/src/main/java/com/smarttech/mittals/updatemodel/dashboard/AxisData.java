
package com.smarttech.mittals.updatemodel.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AxisData implements Parcelable {

    @SerializedName("axisname")
    @Expose
    private String axisname;
    @SerializedName("axisval")
    @Expose
    private Axisval axisval;

    public String getAxisname() {
        return axisname;
    }

    public void setAxisname(String axisname) {
        this.axisname = axisname;
    }

    public Axisval getAxisval() {
        return axisval;
    }

    public void setAxisval(Axisval axisval) {
        this.axisval = axisval;
    }


    protected AxisData(Parcel in) {
        axisname = in.readString();
        axisval = (Axisval) in.readValue(Axisval.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(axisname);
        dest.writeValue(axisval);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AxisData> CREATOR = new Parcelable.Creator<AxisData>() {
        @Override
        public AxisData createFromParcel(Parcel in) {
            return new AxisData(in);
        }

        @Override
        public AxisData[] newArray(int size) {
            return new AxisData[size];
        }
    };
}
