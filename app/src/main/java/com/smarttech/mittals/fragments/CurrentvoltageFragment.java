package com.smarttech.mittals.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.gkravas.meterview.MeterView;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.activities.CurrentVolt_landscape_activity;
import com.smarttech.mittals.adapters.Adapter_CurrentVoltageMeters;
import com.smarttech.mittals.adapters.CurrentVoltageAdapter;
import com.smarttech.mittals.appbase.AppBaseFragment;
import com.smarttech.mittals.handler.IDataPasser;
import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.updatemodel.currentvoltage.CurrentVoltageData;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.updatemodel.currentvoltage.MeterDetail;
import com.smarttech.mittals.updatemodel.currentvoltage.Meters;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by upama on 16/12/16.
 */

public class CurrentvoltageFragment extends AppBaseFragment implements View.OnClickListener{

    Button btn_date;
    View rowView;
    private TextView dpResult;
    ArrayList<String> horizontalList = new ArrayList<String>();
    CurrentVoltageAdapter currentvolatgeadapter;

    private int year;
    private int month;
    private int day;
    static final int DATE_DIALOG_ID = 999;

    MeterView meterView;
    CheckBox cbx1, cbx2, cbx3, cbx4;
    private Button viewgraph_current;
    private LineChart graph_currentvoltage;
    private RecyclerView recyclrview_current;
    private RadioGroup materialradiogroup;
    private RadioButton materialradiovoltage,materialradiocurrent;
    private String modeSend,datesend;

    private LoginData mData = null;
    private int fragPosition = -1;

    private CurrentVoltageData data = null;
    private LinearLayout lvcurrvoltdata;
    private TextView tvdataphasea,tvdataphaseb,tvdataphasec;

    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] currvoltGraphBoolean = {true,true,true,true,true};
    String[] colors_array = {"#99ff99", "#00b050", "#047739", "#ff0000", "#8c0707"};
    private LineData globalLinedata;
    private String currentHospitalId=null;

    private RecyclerView rv_currentmeters;
    private LinearLayoutManager mLayoutManager;
    private Adapter_CurrentVoltageMeters mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        currentHospitalId = args.getString(AppConstants.DATA_CURRENTID);

        fragPosition = getArguments().getInt(AppConstants.DATA_POSITION);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");

        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");

            return;
        }
        if(currentHospitalId==null){
            currentHospitalId=mData.getValue().getValue().getUserId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rowView= inflater.inflate(R.layout.fragment_currentvoltage, container, false);

        meterView=(MeterView) rowView.findViewById(R.id.meter_view);

//        cbx1 = (CheckBox) rowView.findViewById(R.id.cbx1);
//        cbx2 = (CheckBox) rowView.findViewById(R.id.cbx2);
//        cbx3 = (CheckBox) rowView.findViewById(R.id.cbx3);
//        cbx4 = (CheckBox) rowView.findViewById(R.id.cbx4);

        materialradiogroup = (RadioGroup) rowView.findViewById(R.id.materialradiogroup);
        materialradiovoltage = (RadioButton) rowView.findViewById(R.id.materialradiovoltage);
        materialradiogroup.check(materialradiovoltage.getId());
        modeSend="VOLTAGE";
        materialradiovoltage.setOnClickListener(this);

        materialradiocurrent = (RadioButton) rowView.findViewById(R.id.materialradiocurrent);
        materialradiocurrent.setOnClickListener(this);

        dpResult=(TextView) rowView.findViewById(R.id.datePicker_realtime);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 0);
        Date result = c.getTime();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        datesend = df.format(c.getTime());
        dpResult.setText(datesend);
        dpResult.setOnClickListener(this);

        recyclrview_current = (RecyclerView) rowView.findViewById(R.id.recyclrview_current);

        viewgraph_current=(Button) rowView.findViewById(R.id.viewgraph_current);
        viewgraph_current.setOnClickListener(this);

        graph_currentvoltage=(LineChart) rowView.findViewById(R.id.graph_currentvoltage);

        lvcurrvoltdata = (LinearLayout) rowView.findViewById(R.id.lvcurrvoltdata);
        LinearLayout lvcurrvoltphasea = (LinearLayout) rowView.findViewById(R.id.lvcurrvoltphasea);
        TextView tvcolorphasea = (TextView) rowView.findViewById(R.id.tvcolorphasea);
         tvdataphasea = (TextView) rowView.findViewById(R.id.tvdataphasea);

        LinearLayout lvcurrvoltphaseb = (LinearLayout) rowView.findViewById(R.id.lvcurrvoltphaseb);
        TextView tvcolorphaseb = (TextView) rowView.findViewById(R.id.tvcolorphaseb);
         tvdataphaseb = (TextView) rowView.findViewById(R.id.tvdataphaseb);

        LinearLayout lvcurrvoltphasec = (LinearLayout) rowView.findViewById(R.id.lvcurrvoltphasec);
        TextView tvcolorphasec = (TextView) rowView.findViewById(R.id.tvcolorphasec);
         tvdataphasec = (TextView) rowView.findViewById(R.id.tvdataphasec);

        LinearLayout lvcurrvoltfreq = (LinearLayout) rowView.findViewById(R.id.lvcurrvoltfreq);
        TextView tvcolorfreq = (TextView) rowView.findViewById(R.id.tvcolorfreq);
        TextView tvdatafreq = (TextView) rowView.findViewById(R.id.tvdatafreq);

        LinearLayout lvcurrvoltpf = (LinearLayout) rowView.findViewById(R.id.lvcurrvoltpf);
        TextView tvcolorpf = (TextView) rowView.findViewById(R.id.tvcolorpf);
        TextView tvdatapf = (TextView) rowView.findViewById(R.id.tvdatapf);

        rv_currentmeters = (RecyclerView) rowView.findViewById(R.id.rv_currentmeters);

        graph_legendLayout.add(0, lvcurrvoltphasea);
        graph_legendLayout.add(1, lvcurrvoltphaseb);
        graph_legendLayout.add(2, lvcurrvoltphasec);
        graph_legendLayout.add(3, lvcurrvoltfreq);
        graph_legendLayout.add(4, lvcurrvoltpf);
        tv_legendarray.add(0, tvcolorphasea);
        tv_legendarray.add(1, tvcolorphaseb);
        tv_legendarray.add(2, tvcolorphasec);
        tv_legendarray.add(3, tvcolorfreq);
        tv_legendarray.add(4, tvcolorpf);

        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currvoltGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }


        setCurrentVoltage(null);

        return rowView;
    }


    private void setCurrentVoltage(JSONObject object) {

        JSONObject param = new JSONObject();
        try {
            param.put("graph_id", mData.getValue().getMenu().get(fragPosition).getGraphId());
            param.put("user_id", currentHospitalId);
            param.put("access_key", mData.getValue().getValue().getAccessKey());
            param.put("date", datesend);
            param.put("mode",modeSend);
            if (object == null) {
                param.put("meters_id", "");
            } else {
                param.put("meters_id", object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = AppConstants.URL_BASE + AppConstants.SUBURL_COMMON;
        str[1] = param.toString();

        new NetworkHandlerModel(getActivity(), callback, CurrentVoltageData.class, 1).execute(str);
    }
    private void setCurrentVoltage() throws JSONException {

//        JSONObject object = new JSONObject();
//        JSONArray aray = new JSONArray();
//        JSONObject meterObject = new JSONObject();
//        meterObject.put("id", "1");
//        meterObject.put("meter_id", "FMRI - 001");
//        meterObject.put("checked", true);
//        meterObject.put("status", "checked");
//        aray.put(meterObject);
//
//        object.put("meters", aray);
//
//        setCurrentVoltage(object);

        JSONObject paramObject = new JSONObject();

        Meters currentMeters=data.getMeters();

        if (currentMeters != null) {
            JSONArray powerArray = new JSONArray();

            if (currentMeters.getPower() != null && !currentMeters.getPower().isEmpty()) {

                List<MeterDetail> pList = currentMeters.getPower();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId().replaceAll("&", ""));
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }


                    powerArray.put(tmpObject);
                }
                paramObject.put("power", powerArray);
            }

            if (currentMeters.getSolar() != null && !currentMeters.getSolar().isEmpty()) {
                List<MeterDetail> pList = currentMeters.getSolar();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId().replaceAll("&", ""));
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }

                    powerArray.put(tmpObject);
                }
                paramObject.put("solar", powerArray);
            }

            setCurrentVoltage(paramObject);

        } else {
            AppLogger.showToastSmall(getActivity().getApplicationContext(), "Current Meter is NULL.");
        }
    }
    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getActivity(), "Some Error Occurred");
                return;
            }

            data = (CurrentVoltageData) obj;
            updateViewAfterFetch(data);
            setCheckBox(data);

        }

    };

    private void setCheckBox(CurrentVoltageData data) {

//        CheckBox[] checkArr={cbx1,cbx2,cbx3,cbx4};
//
//        List<com.smarttech.mittals.model.currentvoltage.Meter> variable = data.getMeters();
//
//        int siz = Math.min(variable.size(),checkArr.length);
//
//        for(int i = 0;i<siz; i++){
//
//            com.smarttech.mittals.model.currentvoltage.Meter mtr = variable.get(i);
//
//            if (mtr.getChecked() == true) {
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setBackgroundColor(AppConstants.CHECK_COLOR);
//                checkArr[i].setTextColor(Color.WHITE);
//                checkArr[i].setChecked(true);
//                checkArr[i].setVisibility(View.VISIBLE);
//            } else if(mtr.getChecked() == false) {
//                checkArr[i].setBackgroundColor(Color.WHITE);
//                //checkArr[i].setTextColor(Color.parseColor("#737272"));
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setChecked(false);
//                checkArr[i].setVisibility(View.VISIBLE);
//            }
//            else{
//                checkArr[i].setVisibility(View.GONE);
//            }
//        }

        rv_currentmeters.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_currentmeters.setLayoutManager(mLayoutManager);

        if(!data.getMeters().getPower().isEmpty()){
            mAdapter = new Adapter_CurrentVoltageMeters(dataPasser, data.getMeters().getPower());
            rv_currentmeters.setAdapter(mAdapter);
        }
        else if(!data.getMeters().getSolar().isEmpty()){
            mAdapter = new Adapter_CurrentVoltageMeters(dataPasser, data.getMeters().getSolar());
            rv_currentmeters.setAdapter(mAdapter);
        }
    }
    private void updateViewAfterFetch(CurrentVoltageData data) {
        if(data.getGraphData().getStats()!=null){
            currentvolatgeadapter = new CurrentVoltageAdapter(data.getGraphData().getStats());
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclrview_current.setLayoutManager(horizontalLayoutManagaer);
            recyclrview_current.setAdapter(currentvolatgeadapter);

            for (int index = 0; index < colors_array.length; index++) {
                tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
            }

            tvdataphasea.setText(modeSend+" Phase-A");
            tvdataphaseb.setText(modeSend+" Phase-B");
            tvdataphasec.setText(modeSend+" Phase-C");

            updategraph(data);
        }


    }

    private void updategraph(CurrentVoltageData data) {

        if(data.getGraphData().getAxisData().getAxisval()!=null){
            ArrayList<Entry> phase_aEntries = new ArrayList<>();
            ArrayList<Entry> phase_bEntries = new ArrayList<>();
            ArrayList<Entry> phase_cEntries = new ArrayList<>();
            ArrayList<Entry> freqEntries = new ArrayList<>();
            ArrayList<Entry> pfEntries = new ArrayList<>();

            int blockLen = data.getGraphData().getAxisData().getAxisval().getBlock().size();

            ArrayList<String> labels = new ArrayList<String>();

            for (int index = 0; index < blockLen; index++) {
                labels.add(data.getGraphData().getAxisData().getAxisval().getBlock().get(index));
                phase_aEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPhaseA().get(index))), index));
                phase_bEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPhaseB().get(index))), index));
                phase_cEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPhaseC().get(index))), index));
                freqEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getFreq().get(index))), index));
                pfEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPf().get(index))), index));

            }

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            setGraphVales(dataSets, phase_aEntries, "", AppConstants.CURRVOLT_COLOR_PHASEA, false);
            setGraphVales(dataSets, phase_bEntries, "", AppConstants.CURRVOLT_COLOR_PHASEB, false);
            setGraphVales(dataSets, phase_cEntries, "", AppConstants.CURRVOLT_COLOR_PHASEC, false);
            setGraphVales(dataSets, freqEntries, "", AppConstants.CURRVOLT_COLOR_FREQ, false);
            setGraphVales(dataSets, pfEntries, " ", AppConstants.CURRVOLT_COLOR_PF, false);


            globalLinedata = new LineData(labels, dataSets);


            globalLinedata.setHighlightEnabled(true);
            graph_currentvoltage.setData(globalLinedata);
            graph_currentvoltage.animateY(5000);
            graph_currentvoltage.setDrawGridBackground(false);
            graph_currentvoltage.setDescription(data.getGraphData().getAxisData().getAxisname());
            graph_currentvoltage.getLegend().setEnabled(false);
            YAxis axisRight = graph_currentvoltage.getAxisRight();
            axisRight.setDrawLabels(false);

       /*  YAxis axisLeft = graph_power.getAxisLeft();
        axisLeft.setDrawLabels(false);

        XAxis axis = graph_power.getXAxis();
        axis.setDrawLabels(false);*/

            lvcurrvoltdata.setVisibility(View.VISIBLE);
        }



    }
    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }
    private void updateGraphValues(int position) {

        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < currvoltGraphBoolean.length; index++) {
            if (index == position) {
                currvoltGraphBoolean[index] = !currvoltGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < currvoltGraphBoolean.length; index++) {
            if (currvoltGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_currentvoltage.setData(temp);
        graph_currentvoltage.animateY(5000);
    }
    private void resetGraphValues() {
        for(int index=0;index<currvoltGraphBoolean.length;index++){
            currvoltGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<currvoltGraphBoolean.length;index++){
            if(currvoltGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        final float min = 0;
        final float max = 54;
        final Random random = new Random();

        Observable.interval(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<Long, Float>() {
                    @Override
                    public Float call(Long aLong) {
                        return min + (random.nextFloat() * (max - min));
                    }
                })
                .subscribe(new Action1<Float>() {
                    @Override
                    public void call(Float value) {
                        meterView.setValue(value);
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if(view == materialradiovoltage){
            modeSend="VOLTAGE";
            try {
                setCurrentVoltage();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(view == materialradiocurrent){
            modeSend="CURRENT";
            try {
                setCurrentVoltage();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(view == dpResult){

            Calendar mcurrentDate=Calendar.getInstance();
            year=mcurrentDate.get(Calendar.YEAR);
            month=mcurrentDate.get(Calendar.MONTH);
            day=mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    year = selectedyear;
                    month = selectedmonth;
                    day = selectedday;

                    // set selected date into textview
                    dpResult.setText(new StringBuilder().append(day)
                            .append("-").append(month + 1).append("-").append(year)
                            .append(" "));

                    // set selected date into datepicker also
                    datesend = dpResult.getText().toString();
                    try {
                        setCurrentVoltage();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();

        }
        if (view == viewgraph_current){
            Intent i=new Intent(getActivity(), CurrentVolt_landscape_activity.class);
            i.putExtra("modeSend",modeSend);
            i.putExtra(AppConstants.APP_DATA,data.getGraphData().getAxisData());
            startActivity(i);
        }

    }
    private IDataPasser dataPasser=new IDataPasser() {

        @Override
        public void callApi(Object obj) {
            try {
                setCurrentVoltage();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}