
package com.smarttech.mittals.updatemodel.averagepower;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meters {

    @SerializedName("power")
    @Expose
    private List<MeterDetail> power = null;
    @SerializedName("solar")
    @Expose
    private List<MeterDetail> solar = null;

    public List<MeterDetail> getPower() {
        return power;
    }

    public void setPower(List<MeterDetail> power) {
        this.power = power;
    }

    public List<MeterDetail> getSolar() {
        return solar;
    }

    public void setSolar(List<MeterDetail> solar) {
        this.solar = solar;
    }

}
