package com.smarttech.mittals.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.gkravas.meterview.MeterView;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.activities.Average_landscape_activity;
import com.smarttech.mittals.adapters.Adapter_AverageMeters;
import com.smarttech.mittals.adapters.AveragePowerCostAdapter;
import com.smarttech.mittals.appbase.AppBaseFragment;
import com.smarttech.mittals.handler.IDataPasser;
import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.updatemodel.averagepower.AveragePowerCost;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.updatemodel.averagepower.MeterDetail;
import com.smarttech.mittals.updatemodel.averagepower.Meters;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

public class AveragePowerCostFragments extends AppBaseFragment implements View.OnClickListener{

    View rowView;
    AveragePowerCostAdapter averagePowerCostAdapter;
    CheckBox cbx1, cbx2, cbx3, cbx4;
    MeterView meterView;
    private int fragPosition=-1;
    private Button viewgraph_average;
    private RecyclerView recyclrview_avg;
    private LoginData mData = null;

    private RadioGroup materialradiogroup;
    private RadioButton materialradiokw,materialradiokwh;
    private String modeSend;
    private AveragePowerCost data=null;
    private LinearLayout lvavgdata;
    private TextView tvdataexc,tvdatabil,tvdataseb,tvcolorexc,tvcolorbil,tvcolorseb,tvcoloravgcost;
    private CombinedChart graph_average;
    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] avgGraphBoolean = {true,true,true,true};
    private String[] colors_array = {"#056832", "#00b050", "#99ff99", "#ff0000"};
    private String COLOR_SEB="#056832";
    private String COLOR_BIL="#00b050";
    private String COLOR_EXC="#99ff99";
    private DecimalFormat mFormat;
    private String currentHospitalId=null;

    private RecyclerView rv_averagemeters;
    private LinearLayoutManager mLayoutManager;
    private Adapter_AverageMeters mAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        currentHospitalId = args.getString(AppConstants.DATA_CURRENTID);
        fragPosition = getArguments().getInt(AppConstants.DATA_POSITION);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");

        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");

            return;
        }
        if(currentHospitalId==null){
            currentHospitalId=mData.getValue().getValue().getUserId();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rowView= inflater.inflate(R.layout.fragment_average_power_cost, container, false);

        meterView=(MeterView) rowView.findViewById(R.id.meter_view);
//        cbx1 = (CheckBox) rowView.findViewById(R.id.cbx1);
//        cbx2 = (CheckBox) rowView.findViewById(R.id.cbx2);
//        cbx3 = (CheckBox) rowView.findViewById(R.id.cbx3);
//        cbx4 = (CheckBox) rowView.findViewById(R.id.cbx4);

        materialradiogroup = (RadioGroup) rowView.findViewById(R.id.materialradiogroup);
        materialradiokw = (RadioButton) rowView.findViewById(R.id.materialradiokw);
        materialradiogroup.check(materialradiokw.getId());
        modeSend="kw";
        materialradiokw.setOnClickListener(this);

        materialradiokwh = (RadioButton) rowView.findViewById(R.id.materialradiokwh);
        materialradiokwh.setOnClickListener(this);

        recyclrview_avg = (RecyclerView) rowView.findViewById(R.id.recyclrview_avg);


        viewgraph_average=(Button) rowView.findViewById(R.id.viewgraph_average);
        viewgraph_average.setOnClickListener(this);

        graph_average = (CombinedChart) rowView.findViewById(R.id.graph_average);

        lvavgdata = (LinearLayout) rowView.findViewById(R.id.lvavgdata);
        LinearLayout lvavgexc = (LinearLayout) rowView.findViewById(R.id.lvavgexc);
         tvcolorexc = (TextView) rowView.findViewById(R.id.tvcolorexc);
         tvdataexc = (TextView) rowView.findViewById(R.id.tvdataexc);

        LinearLayout lvavgbil = (LinearLayout) rowView.findViewById(R.id.lvavgbil);
         tvcolorbil = (TextView) rowView.findViewById(R.id.tvcolorbil);
         tvdatabil = (TextView) rowView.findViewById(R.id.tvdatabil);

        LinearLayout lvavgseb = (LinearLayout) rowView.findViewById(R.id.lvavgseb);
         tvcolorseb = (TextView) rowView.findViewById(R.id.tvcolorseb);
         tvdataseb = (TextView) rowView.findViewById(R.id.tvdataseb);

        LinearLayout lvavgcost = (LinearLayout) rowView.findViewById(R.id.lvavgcost);
         tvcoloravgcost = (TextView) rowView.findViewById(R.id.tvcoloravgcost);
        TextView tvdataavgcost = (TextView) rowView.findViewById(R.id.tvdataavgcost);

        rv_averagemeters = (RecyclerView) rowView.findViewById(R.id.rv_averagemeters);

        graph_legendLayout.add(0, lvavgexc);
        graph_legendLayout.add(1, lvavgbil);
        graph_legendLayout.add(2, lvavgseb);
        graph_legendLayout.add(3, lvavgcost);
        tv_legendarray.add(0, tvcolorexc);
        tv_legendarray.add(1, tvcolorbil);
        tv_legendarray.add(2, tvcolorseb);
        tv_legendarray.add(3, tvcoloravgcost);


        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (avgGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }
        setAveragePowerCost(null);
        return rowView;
    }


    private void setAveragePowerCost(JSONObject object) {

        JSONObject param = new JSONObject();
        try {
            param.put("graph_id", mData.getValue().getMenu().get(fragPosition).getGraphId());
            param.put("user_id", currentHospitalId);
            param.put("access_key", mData.getValue().getValue().getAccessKey());
            param.put("mode",modeSend);
            if(object == null){
                param.put("meters_id", "");
            }else{
                param.put("meters_id", object);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = AppConstants.URL_BASE + AppConstants.SUBURL_COMMON;
        str[1] = param.toString();

        new NetworkHandlerModel(getActivity(), callback, AveragePowerCost.class, 1).execute(str);
    }
    private void setAveragePowerCost() throws JSONException {

//        JSONObject object = new JSONObject();
//        JSONArray aray = new JSONArray();
//        JSONObject meterObject = new JSONObject();
//        meterObject.put("id", "1");
//        meterObject.put("meter_id", "FMRI - 001");
//        meterObject.put("checked", true);
//        meterObject.put("status", "checked");
//        aray.put(meterObject);
//
//        object.put("meters", aray);
//
//        setAveragePowerCost(object);
        JSONObject paramObject = new JSONObject();

        Meters currentMeters=data.getMeters();

        if (currentMeters != null) {
            JSONArray powerArray = new JSONArray();

            if (currentMeters.getPower() != null && !currentMeters.getPower().isEmpty()) {

                List<MeterDetail> pList = currentMeters.getPower();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId());
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }


                    powerArray.put(tmpObject);
                }
                paramObject.put("power", powerArray);
            }

            if (currentMeters.getSolar() != null && !currentMeters.getSolar().isEmpty()) {
                List<MeterDetail> pList = currentMeters.getSolar();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId());
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }

                    powerArray.put(tmpObject);
                }
                paramObject.put("solar", powerArray);
            }

            setAveragePowerCost(paramObject);

        } else {
            AppLogger.showToastSmall(getActivity().getApplicationContext(), "Current Meter is NULL.");
        }
    }
    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getActivity(), "Some Error Occurred");
                return;
            }

            data = (AveragePowerCost) obj;
            updateViewAfterFetch(data);
            setCheckBox(data);

        }

    };

    private void setCheckBox(AveragePowerCost data) {
//        CheckBox[] checkArr={cbx1,cbx2,cbx3,cbx4};
//
//        List<Meter> variable = data.getMeters();
//
//        int siz = Math.min(variable.size(),checkArr.length);
//
//        for(int i = 0;i<siz; i++){
//
//            Meter mtr = variable.get(i);
//
//            if (mtr.getChecked() == true) {
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setBackgroundColor(AppConstants.CHECK_COLOR);
//                checkArr[i].setTextColor(Color.WHITE);
//                checkArr[i].setChecked(true);
//                checkArr[i].setVisibility(View.VISIBLE);
//            } else if(mtr.getChecked() == false) {
//                checkArr[i].setBackgroundColor(Color.WHITE);
//                //checkArr[i].setTextColor(Color.parseColor("#737272"));
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setChecked(false);
//                checkArr[i].setVisibility(View.VISIBLE);
//            }
//            else{
//                checkArr[i].setVisibility(View.GONE);
//            }
//        }

        rv_averagemeters.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_averagemeters.setLayoutManager(mLayoutManager);


        if(!data.getMeters().getPower().isEmpty()){
            mAdapter = new Adapter_AverageMeters(dataPasser, data.getMeters().getPower());
            rv_averagemeters.setAdapter(mAdapter);
        }
        else if(!data.getMeters().getSolar().isEmpty()){
            mAdapter = new Adapter_AverageMeters(dataPasser, data.getMeters().getSolar());
            rv_averagemeters.setAdapter(mAdapter);
        }

    }

    private void updateViewAfterFetch(AveragePowerCost data) {

        averagePowerCostAdapter = new AveragePowerCostAdapter(data.getGraphData().getStats());
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclrview_avg.setLayoutManager(horizontalLayoutManagaer);
        recyclrview_avg.setAdapter(averagePowerCostAdapter);

        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
        tvdataexc.setText("Percentage of power source from Exchange ("+ modeSend +")");
        tvdatabil.setText("Percentage of power source from Bilateral ("+ modeSend +")");
        tvdataseb.setText("Percentage of power source from SEB ("+ modeSend +")");

        updateGraph(data);

    }

    private void updateGraph(AveragePowerCost data) {

        ArrayList<String> xAxisValues=getXAxisValues();

        BarData barData = new BarData(xAxisValues, getDataSet());

        CombinedData cdata = new CombinedData(xAxisValues);
        cdata.setData(barData);
        cdata.setData(generatelineData());

        graph_average.setData(cdata);
        graph_average.setDescription(data.getGraphData().getAxisData().getAxisname());
        graph_average.animateXY(2000, 2000);
        graph_average.invalidate();
        graph_average.getLegend().setEnabled(false);
        YAxis axisRight = graph_average.getAxisRight();
        axisRight.setDrawLabels(false);

        lvavgdata.setVisibility(View.VISIBLE);
    }


    private void updateGraphValues(int position) {

        for (int index = 0; index < avgGraphBoolean.length; index++) {
            if (index == position) {
                avgGraphBoolean[index] = !avgGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        ArrayList<String> xAxisValues=getXAxisValues();
        BarData barData;

        CombinedData cdata = new CombinedData(getXAxisValues());

        if(avgGraphBoolean[0] && avgGraphBoolean[2] && avgGraphBoolean[3]){
            barData = new BarData(xAxisValues, getDataSet());
            cdata.setData(barData);
        }else if(avgGraphBoolean[0]){
            barData = new BarData(xAxisValues, updateExchangeArray());
            cdata.setData(barData);
        }else if(avgGraphBoolean[2]){
            barData = new BarData(xAxisValues, updateBilateralArray());
            cdata.setData(barData);
        }else if(avgGraphBoolean[3]){
            barData = new BarData(xAxisValues, updateSebArray());
            cdata.setData(barData);
        }

        if(avgGraphBoolean[1]){
            cdata.setData(generatelineData());
        }
        graph_average.setData(cdata);
        graph_average.setDescription(data.getGraphData().getAxisData().getAxisname());
        graph_average.animateXY(2000, 2000);
        graph_average.invalidate();
        graph_average.getLegend().setEnabled(false);


    }
    private void resetGraphValues() {
        for(int index=0;index<avgGraphBoolean.length;index++){
            avgGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }

    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<avgGraphBoolean.length;index++){
            if(avgGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }
    private ArrayList<BarDataSet> updateSebArray(){

        ArrayList<BarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        for(int index=0;index<data.getGraphData().getAxisData().getAxisval().getBlock().size();index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPerSeb().get(index))) }, index);
            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 3");
        barDataSet1.setColors(new int[]{Color.parseColor(COLOR_SEB)});

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);

        return dataSets;
    }
    private ArrayList<BarDataSet> updateBilateralArray(){

        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        for(int index=0;index<data.getGraphData().getAxisData().getAxisval().getBlock().size();index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPerBil().get(index))) }, index);
            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 2");
        barDataSet1.setColors(new int[]{Color.parseColor(COLOR_BIL)});

        dataSets.add(barDataSet1);

        return dataSets;
    }
    private ArrayList<BarDataSet> updateExchangeArray(){

        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        for(int index=0;index<data.getGraphData().getAxisData().getAxisval().getBlock().size();index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPerExc().get(index))) }, index);
            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 1");
        barDataSet1.setColors(new int[]{Color.parseColor(COLOR_EXC)});

        dataSets.add(barDataSet1);

        return dataSets;
    }
    private LineData generatelineData() {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        for(int index=0;index<data.getGraphData().getAxisData().getAxisval().getAvgCost().size();index++) {
            labels.add(data.getGraphData().getAxisData().getAxisval().getBlock().get(index));
            entries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getAvgCost().get(index))),index));
        }

        LineDataSet dataset = new LineDataSet(entries, "# of Calls");
        dataset.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

                mFormat = new DecimalFormat("###,###,###,##0.000" + "");
                return mFormat.format(value);
            }
        });
        dataset.setColor(Color.parseColor("#ff0000"));
        dataset.setCircleColor(Color.parseColor("#ff0000"));
        dataset.setFillColor(Color.parseColor("#ff0000"));
        dataset.setCircleColorHole(Color.parseColor("#ff0000"));
        dataset.setCircleSize(3f);

        LineData data = new LineData(labels, dataset);

        return data;
    }

    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

            for(int index=0;index<data.getGraphData().getAxisData().getAxisval().getBlock().size();index++) {
                BarEntry v1e1 = new BarEntry(new float[]{ Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPerSeb().get(index))),
                        Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPerExc().get(index))),
                        Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getPerBil().get(index))) }, index);
                valueSet1.add(v1e1);
            }



        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 3");

        if(!data.getGraphData().getAxisData().getAxisval().getPerSeb().isEmpty() & !data.getGraphData().getAxisData().getAxisval().getPerBil().isEmpty() & !data.getGraphData().getAxisData().getAxisval().getPerExc().isEmpty()) {
            barDataSet1.setColors(new int[]{Color.parseColor("#056832"), Color.parseColor("#00b050"),Color.parseColor("#99ff99")});
        }
        else if(!data.getGraphData().getAxisData().getAxisval().getPerSeb().isEmpty() & data.getGraphData().getAxisData().getAxisval().getPerBil().isEmpty()& data.getGraphData().getAxisData().getAxisval().getPerExc().isEmpty()) {
            barDataSet1.setColors(new int[]{Color.parseColor("#056832")});
        }
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        for(int index=0;index<data.getGraphData().getAxisData().getAxisval().getBlock().size();index++) {
            xAxis.add(data.getGraphData().getAxisData().getAxisval().getBlock().get(index));
        }
        return xAxis;
    }

    @Override
    public void onResume() {
        super.onResume();
        final float min = 0;
        final float max = 54;
        final Random random = new Random();

        Observable.interval(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<Long, Float>() {
                    @Override
                    public Float call(Long aLong) {
                        return min + (random.nextFloat() * (max - min));
                    }
                })
                .subscribe(new Action1<Float>() {
                    @Override
                    public void call(Float value) {
                        meterView.setValue(value);
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if(view == materialradiokw){
            modeSend="kw";
            try {
                setAveragePowerCost();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(view == materialradiokwh){
            modeSend="kwh";
            try {
                setAveragePowerCost();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (view == viewgraph_average){
            Intent i=new Intent(getActivity(), Average_landscape_activity.class);
            i.putExtra("modeSend",modeSend);
            i.putExtra(AppConstants.APP_DATA,data.getGraphData().getAxisData());
            startActivity(i);
        }

    }
    private IDataPasser dataPasser=new IDataPasser() {

        @Override
        public void callApi(Object obj) {
            try {
                setAveragePowerCost();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}