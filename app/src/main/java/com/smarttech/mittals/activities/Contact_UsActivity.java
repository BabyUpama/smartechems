package com.smarttech.mittals.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.appbase.BaseActivity;
import com.smarttech.mittals.handler.NetworkGetHandler;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by upama on 3/1/17.
 */

public class Contact_UsActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout lvback;
    LoginData mData=null;

    private TextView tv_address,tvphonenumber,tvextensionnumber,tvtoolfreenubernumber,tvemail,tvwebsite;
    String address,caption,icon,captionone,captiontwo,email,icontwo,captionthree,website,iconthree;
    ArrayList<String> numberArray = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            setbackclick();
        } else {
            AppLogger.showToastSmall(this, "No Data Found");
            finish();
            return;
        }
        tv_address =(TextView)findViewById(R.id.tv_address);
        tv_address.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_address.setOnClickListener(this);

        tvphonenumber = (TextView)findViewById(R.id.tvphonenumber);
        tvphonenumber.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvphonenumber.setOnClickListener(this);

        tvextensionnumber = (TextView)findViewById(R.id.tvextensionnumber);
        tvextensionnumber.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvextensionnumber.setOnClickListener(this);

        tvtoolfreenubernumber = (TextView)findViewById(R.id.tvtoolfreenubernumber);
        tvtoolfreenubernumber.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvtoolfreenubernumber.setOnClickListener(this);

        tvemail = (TextView)findViewById(R.id.tvemail);
        tvemail.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvemail.setOnClickListener(this);

        tvwebsite = (TextView)findViewById(R.id.tvwebsite);
        tvwebsite.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvwebsite.setOnClickListener(this);


        setContactus();

    }

    private void setContactus() {

        String url = AppConstants.URL_BASE + AppConstants.SUBURL_CONTACT_US;
        new NetworkGetHandler(Contact_UsActivity.this, url, this).execute(url);
    }


    public void passDataFromNetworkGetHandler(Object object) {
        if(object == null){
            AppLogger.showToastSmall(getApplicationContext(), "Some Error Occurred");
            return;
        }
        String str = (String) object;
        try {
            numberArray.clear();
            JSONObject mainObject = new JSONObject(str);
            JSONObject addressdetails = mainObject.getJSONObject("addressdetails");
            address = addressdetails.getString("address");
            caption = addressdetails.getString("caption");
            icon = addressdetails.getString("icon");

            JSONObject phone = mainObject.getJSONObject("phone");
            captionone = phone.getString("caption");
            icon = phone.getString("icon");
            JSONArray number = phone.getJSONArray("number");
            for (int i = 0; i < number.length(); i++) {
                String numberdata = number.getString(i);
                numberArray.add(numberdata);
            }

            JSONObject emaildetails = mainObject.getJSONObject("emaildetails");
            captiontwo = emaildetails.getString("caption");
            email = emaildetails.getString("email");
            icontwo = emaildetails.getString("icon");

            JSONObject websitedetails = mainObject.getJSONObject("websitedetails");
            captionthree = websitedetails.getString("caption");
            website = websitedetails.getString("website");
            iconthree = websitedetails.getString("icon");
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        updateViewAfterFetch();

    }


    private void updateViewAfterFetch() {

        tv_address.setText(address);
        tvemail.setText(email);
        tvwebsite.setText(website);
        tvphonenumber.setText(numberArray.get(0));
        tvextensionnumber.setText(numberArray.get(1));
        tvtoolfreenubernumber.setText(numberArray.get(2));


    }

    private void setbackclick() {
         lvback = (LinearLayout) findViewById(R.id.lvback);
         lvback.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view == lvback){
            Intent i = new Intent(Contact_UsActivity.this, DashboardActivity.class);
            i.putExtra(AppConstants.LOGIN_DATA,mData);
            startActivity(i);
            overridePendingTransition(R.anim.animation,
                    R.anim.animation2);

            finish();
        }

        if(view == tvphonenumber){
            String tvphonenumbershow = tvphonenumber.getText().toString().trim();
            Intent dial = new Intent();
            dial.setAction("android.intent.action.DIAL");
            dial.setData(Uri.parse("tel:"+tvphonenumbershow));
            startActivity(dial);

        }
        if(view == tvextensionnumber){
            String tvextensionnumbershow = tvextensionnumber.getText().toString().trim();
            Intent dial = new Intent();
            dial.setAction("android.intent.action.DIAL");
            dial.setData(Uri.parse("tel:"+tvextensionnumbershow));
            startActivity(dial);

        }
        if(view == tvtoolfreenubernumber){
            String tvtollfreenubernumbershow = tvtoolfreenubernumber.getText().toString().trim();
            Intent dial = new Intent();
            dial.setAction("android.intent.action.DIAL");
            dial.setData(Uri.parse("tel:"+tvtollfreenubernumbershow));
            startActivity(dial);

        }
        if(view == tvemail){
            String tvemailshow = tvemail.getText().toString().trim();
            Intent intent=new Intent(Intent.ACTION_SEND);
            String[] recipients={tvemailshow};
            intent.putExtra(Intent.EXTRA_EMAIL, recipients);
            intent.putExtra(Intent.EXTRA_SUBJECT,"");
            intent.setType("text/html");
            startActivity(Intent.createChooser(intent, "Send mail"));

        }
        if(view == tvwebsite){
            String tvwebsiteshow = tvwebsite.getText().toString().trim();
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+tvwebsiteshow));
            startActivity(myIntent);

        }


    }
}
