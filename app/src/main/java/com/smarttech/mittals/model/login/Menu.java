
package com.smarttech.mittals.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Menu implements Parcelable{

    @SerializedName("graph_id")
    @Expose
    private String graphId;
    @SerializedName("graph_name")
    @Expose
    private String graphName;
    @SerializedName("app_icon")
    @Expose
    private String appIcon;

    public String getGraphId() {
        return graphId;
    }

    public void setGraphId(String graphId) {
        this.graphId = graphId;
    }

    public String getGraphName() {
        return graphName;
    }

    public void setGraphName(String graphName) {
        this.graphName = graphName;
    }

    public String getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(String appIcon) {
        this.appIcon = appIcon;
    }

    protected Menu(Parcel in) {
        graphId = in.readString();
        graphName = in.readString();
        appIcon = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(graphId);
        dest.writeString(graphName);
        dest.writeString(appIcon);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Menu> CREATOR = new Parcelable.Creator<Menu>() {
        @Override
        public Menu createFromParcel(Parcel in) {
            return new Menu(in);
        }

        @Override
        public Menu[] newArray(int size) {
            return new Menu[size];
        }
    };

}
