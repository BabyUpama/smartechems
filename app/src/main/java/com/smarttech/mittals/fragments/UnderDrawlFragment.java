package com.smarttech.mittals.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.gkravas.meterview.MeterView;
import com.smartech.mittals.invetech.R;
import com.smarttech.mittals.activities.Underdrawl_landscape_activity;
import com.smarttech.mittals.adapters.Adapter_UnderdrawlMeters;
import com.smarttech.mittals.adapters.UnderDrawlAdapter;
import com.smarttech.mittals.appbase.AppBaseFragment;
import com.smarttech.mittals.handler.IDataPasser;
import com.smarttech.mittals.handler.INetworkCallback;
import com.smarttech.mittals.handler.NetworkHandlerModel;
import com.smarttech.mittals.model.login.LoginData;
import com.smarttech.mittals.updatemodel.underdrawl.Underdrawldata;
import com.smarttech.mittals.updatemodel.underdrawl.MeterDetail;
import com.smarttech.mittals.updatemodel.underdrawl.Meters;
import com.smarttech.mittals.utils.AppConstants;
import com.smarttech.mittals.utils.AppLogger;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;


public class UnderDrawlFragment extends AppBaseFragment implements View.OnClickListener {

    Button btn_date;
    View rowView;
    private TextView dpResult;
    ArrayList<String> horizontalList = new ArrayList<String>();
    UnderDrawlAdapter underdrawladapter;

    private int year;
    private int month;
    private int day;

    MeterView meterView;

    private CheckBox cbx1, cbx2, cbx3, cbx4;
    private LoginData mData = null;
    private int fragPosition = -1;
    private Underdrawldata data = null;
    private RadioGroup materialradiogroup;
    private RadioButton materialradiokw,materialradiokwh,materialradiokva;
    private String datesend,modeSend;
    private RecyclerView recyclrviewunderdrawl;
    private LineChart graph_underdrawl;
    private LineData globalLinedata;
    private LinearLayout lvunderdrawldata;
    private  ArrayList<TextView> tv_legendarray = new ArrayList<>();
    private  ArrayList<LinearLayout> graph_legendLayout = new ArrayList<>();
    private boolean[] underdrawlGraphBoolean = {true,true,true,true};
    String[] colors_array = {"#7cb5ec", "#424247", "#90ed7d", "#f7a35c"};
    private TextView tvdataunderdrawl,tvdatattl_power,tvdataoa_power,tvdatattl_oa;
    private Button viewgraph_underdrawl;
    private String currentHospitalId=null;

    private RecyclerView rv_underdrawlmeters;
    private LinearLayoutManager mLayoutManager;
    private Adapter_UnderdrawlMeters mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        currentHospitalId = args.getString(AppConstants.DATA_CURRENTID);

        fragPosition = getArguments().getInt(AppConstants.DATA_POSITION);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");

        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");

            return;
        }
        if(currentHospitalId==null){
            currentHospitalId=mData.getValue().getValue().getUserId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rowView= inflater.inflate(R.layout.fragment_under_drawl, container, false);

        meterView=(MeterView) rowView.findViewById(R.id.meter_view);
//        cbx1 = (CheckBox) rowView.findViewById(R.id.cbx1);
//        cbx2 = (CheckBox) rowView.findViewById(R.id.cbx2);
//        cbx3 = (CheckBox) rowView.findViewById(R.id.cbx3);
//        cbx4 = (CheckBox) rowView.findViewById(R.id.cbx4);
        materialradiogroup = (RadioGroup) rowView.findViewById(R.id.materialradiogroup);
        materialradiokw = (RadioButton) rowView.findViewById(R.id.materialradiokw);
        materialradiogroup.check(materialradiokw.getId());
        modeSend="kw";
        materialradiokw.setOnClickListener(this);

        materialradiokwh = (RadioButton) rowView.findViewById(R.id.materialradiokwh);
        materialradiokwh.setOnClickListener(this);

        materialradiokva = (RadioButton) rowView.findViewById(R.id.materialradiokva);
        materialradiokva.setOnClickListener(this);

        dpResult=(TextView) rowView.findViewById(R.id.datePicker_realtime);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 0);
        Date result = c.getTime();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        datesend = df.format(c.getTime());
        dpResult.setText(datesend);
        dpResult.setOnClickListener(this);


         viewgraph_underdrawl=(Button) rowView.findViewById(R.id.viewgraph_underdrawl);
        viewgraph_underdrawl.setOnClickListener(this);


        recyclrviewunderdrawl = (RecyclerView) rowView.findViewById(R.id.recyclrviewunderdrawl);

        graph_underdrawl=(LineChart) rowView.findViewById(R.id.graph_underdrawl);

        lvunderdrawldata = (LinearLayout) rowView.findViewById(R.id.lvunderdrawldata);
        LinearLayout lvunderdrawl = (LinearLayout) rowView.findViewById(R.id.lvunderdrawl);
        TextView tvcolorunderdrawl = (TextView) rowView.findViewById(R.id.tvcolorunderdrawl);
         tvdataunderdrawl = (TextView) rowView.findViewById(R.id.tvdataunderdrawl);

        LinearLayout lvunderdrawlttl_power = (LinearLayout) rowView.findViewById(R.id.lvunderdrawlttl_power);
        TextView tvcolorttl_power = (TextView) rowView.findViewById(R.id.tvcolorttl_power);
         tvdatattl_power = (TextView) rowView.findViewById(R.id.tvdatattl_power);

        LinearLayout lvunderdrawloa_power = (LinearLayout) rowView.findViewById(R.id.lvunderdrawloa_power);
        TextView tvcoloroa_power = (TextView) rowView.findViewById(R.id.tvcoloroa_power);
         tvdataoa_power = (TextView) rowView.findViewById(R.id.tvdataoa_power);

        LinearLayout lvunderdrawlttl_oa = (LinearLayout) rowView.findViewById(R.id.lvunderdrawlttl_oa);
        TextView tvcolorttl_oa = (TextView) rowView.findViewById(R.id.tvcolorttl_oa);
         tvdatattl_oa = (TextView) rowView.findViewById(R.id.tvdatattl_oa);

        rv_underdrawlmeters = (RecyclerView) rowView.findViewById(R.id.rv_underdrawlmeters);

        graph_legendLayout.add(0, lvunderdrawl);
        graph_legendLayout.add(1, lvunderdrawlttl_power);
        graph_legendLayout.add(2, lvunderdrawloa_power);
        graph_legendLayout.add(3, lvunderdrawlttl_oa);
        tv_legendarray.add(0, tvcolorunderdrawl);
        tv_legendarray.add(1, tvcolorttl_power);
        tv_legendarray.add(2, tvcoloroa_power);
        tv_legendarray.add(3, tvcolorttl_oa);


        for (int i = 0; i < graph_legendLayout.size(); i++) {
            final int j = i;
            graph_legendLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (underdrawlGraphBoolean[j]) {
                        tv_legendarray.get(j).setBackgroundColor(Color.GRAY);
                    } else {
                        tv_legendarray.get(j).setBackgroundColor(Color.parseColor(colors_array[j]));
                    }
                    updateGraphValues(j);
                }
            });
        }

        setUnderdrawl(null);
        return rowView;
    }



    private void setUnderdrawl(JSONObject object) {

        JSONObject param = new JSONObject();
        try {
            param.put("graph_id", mData.getValue().getMenu().get(fragPosition).getGraphId());
            param.put("user_id", currentHospitalId);
            param.put("access_key", mData.getValue().getValue().getAccessKey());
            param.put("date", datesend);
            param.put("mode",modeSend);
            if (object == null) {
                param.put("meters_id", "");
            } else {
                param.put("meters_id", object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = AppConstants.URL_BASE + AppConstants.SUBURL_COMMON;
        str[1] = param.toString();

        new NetworkHandlerModel(getActivity(), callback, Underdrawldata.class, 1).execute(str);

    }
    private void setUnderdrawl() throws JSONException {

//        JSONObject object = new JSONObject();
//        JSONArray aray = new JSONArray();
//        JSONObject meterObject = new JSONObject();
//        meterObject.put("id", "1");
//        meterObject.put("meter_id", "FMRI - 001");
//        meterObject.put("checked", true);
//        meterObject.put("status", "checked");
//        aray.put(meterObject);
//
//        object.put("meters", aray);
//
//        setUnderdrawl(object);

        JSONObject paramObject = new JSONObject();

        Meters currentMeters=data.getMeters();

        if (currentMeters != null) {
            JSONArray powerArray = new JSONArray();

            if (currentMeters.getPower() != null && !currentMeters.getPower().isEmpty()) {

                List<MeterDetail> pList = currentMeters.getPower();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId());
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }


                    powerArray.put(tmpObject);
                }
                paramObject.put("power", powerArray);
            }

            if (currentMeters.getSolar() != null && !currentMeters.getSolar().isEmpty()) {
                List<MeterDetail> pList = currentMeters.getSolar();

                for (MeterDetail cMeterDetail : pList) {
                    JSONObject tmpObject = new JSONObject();

                    tmpObject.put("id", cMeterDetail.getId());
                    tmpObject.put("meter_id", cMeterDetail.getMeterId());
                    tmpObject.put("checked", cMeterDetail.getChecked());
                    if(cMeterDetail.getChecked()){
                        tmpObject.put("status", "checked");
                    }else{
                        tmpObject.put("status", "");
                    }

                    powerArray.put(tmpObject);
                }
                paramObject.put("solar", powerArray);
            }

            setUnderdrawl(paramObject);

        } else {
            AppLogger.showToastSmall(getActivity().getApplicationContext(), "Current Meter is NULL.");
        }
    }
    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getActivity(), "Some Error Occurred");
                return;
            }

            data = (Underdrawldata) obj;

            updateViewAfterFetch(data);

            setCheckBox(data);


        }

    };


    private void setCheckBox(Underdrawldata data) {
//        CheckBox[] checkArr={cbx1,cbx2,cbx3,cbx4};
//
//        List<com.smarttech.mittals.model.underdrawl.Meter> variable = data.getMeters();
//
//        int siz = Math.min(variable.size(),checkArr.length);
//
//        for(int i = 0;i<siz; i++){
//
//            com.smarttech.mittals.model.underdrawl.Meter mtr = variable.get(i);
//
//            if (mtr.getChecked() == true) {
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setBackgroundColor(AppConstants.CHECK_COLOR);
//                checkArr[i].setTextColor(Color.WHITE);
//                checkArr[i].setChecked(true);
//                checkArr[i].setVisibility(View.VISIBLE);
//            } else if(mtr.getChecked() == false) {
//                checkArr[i].setBackgroundColor(Color.WHITE);
//                //checkArr[i].setTextColor(Color.parseColor("#737272"));
//                checkArr[i].setText(mtr.getMeterId());
//                checkArr[i].setChecked(false);
//                checkArr[i].setVisibility(View.VISIBLE);
//            }
//            else{
//                checkArr[i].setVisibility(View.GONE);
//            }
//        }

        rv_underdrawlmeters.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_underdrawlmeters.setLayoutManager(mLayoutManager);

        if(!data.getMeters().getPower().isEmpty()){
            mAdapter = new Adapter_UnderdrawlMeters(dataPasser, data.getMeters().getPower());
            rv_underdrawlmeters.setAdapter(mAdapter);
        }
        else if(!data.getMeters().getSolar().isEmpty()){
            mAdapter = new Adapter_UnderdrawlMeters(dataPasser, data.getMeters().getSolar());
            rv_underdrawlmeters.setAdapter(mAdapter);
        }


    }
    private void updateViewAfterFetch(Underdrawldata data) {

        underdrawladapter = new UnderDrawlAdapter(data.getGraphData().getStats());
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclrviewunderdrawl.setLayoutManager(horizontalLayoutManagaer);
        recyclrviewunderdrawl.setAdapter(underdrawladapter);


        for (int index = 0; index < colors_array.length; index++) {
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }

        tvdataunderdrawl.setText("Underdrawl ("+ modeSend +")");
        tvdatattl_power.setText("Total MeterDetail Consumption ("+ modeSend +")");
        tvdataoa_power.setText("Open Access MeterDetail Recieved at meter periphery ("+ modeSend +")");
        tvdatattl_oa.setText("Total Open Access MeterDetail Scheduled ("+ modeSend +")");
        updategraph(data);
    }

    private void updategraph(Underdrawldata data) {

        ArrayList<Entry> underdrawlEntries = new ArrayList<>();
        ArrayList<Entry> ttl_powerEntries = new ArrayList<>();
        ArrayList<Entry> oa_powerEntries = new ArrayList<>();
        ArrayList<Entry> ttl_oaEntries = new ArrayList<>();


        int blockLen = data.getGraphData().getAxisData().getAxisval().getBlock().size();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < blockLen; index++) {
            labels.add(data.getGraphData().getAxisData().getAxisval().getBlock().get(index));
            underdrawlEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getUnderdrawl().get(index))), index));
            ttl_powerEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getTtlPower().get(index))), index));
            oa_powerEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getOaPower().get(index))), index));
            ttl_oaEntries.add(new Entry(Float.parseFloat(String.valueOf(data.getGraphData().getAxisData().getAxisval().getTtlOa().get(index))), index));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        setGraphVales(dataSets, underdrawlEntries, "", AppConstants.UNDERDRAWL, false);
        setGraphVales(dataSets, ttl_powerEntries, "", AppConstants.UNDERDRAWL_TTLPOWER, false);
        setGraphVales(dataSets, oa_powerEntries, "", AppConstants.UNDERDRAWL_OAPOWER, false);
        setGraphVales(dataSets, ttl_oaEntries, "", AppConstants.UNDERDRAWL_TTLOA, false);

        globalLinedata = new LineData(labels, dataSets);

        globalLinedata.setHighlightEnabled(true);
        graph_underdrawl.setData(globalLinedata);
        graph_underdrawl.animateY(5000);
        graph_underdrawl.setDrawGridBackground(false);
        graph_underdrawl.setDescription(data.getGraphData().getAxisData().getAxisname());
        graph_underdrawl.getLegend().setEnabled(false);
        YAxis axisRight = graph_underdrawl.getAxisRight();
        axisRight.setDrawLabels(false);

        /*YAxis axisLeft = graph_underdrawl.getAxisLeft();
        axisLeft.setDrawLabels(false);

        XAxis axis = graph_underdrawl.getXAxis();
        axis.setDrawLabels(false);*/

        lvunderdrawldata.setVisibility(View.VISIBLE);
       //
    }

    private void setGraphVales(ArrayList<LineDataSet> dataSets, ArrayList<Entry> mEntry, String string, int color, boolean isShow) {
        LineDataSet mDataSet = new LineDataSet(mEntry, string);
        mDataSet.setColor(color);
        mDataSet.setCircleColor(color);
        mDataSet.setCircleColorHole(color);
        mDataSet.setCircleSize(2f);
        mDataSet.setValueTextColor(color);
        if (isShow) {
            mDataSet.setDrawFilled(true);
        } else {
            mDataSet.setDrawFilled(false);
        }
        mDataSet.setDrawCubic(false);
        dataSets.add(mDataSet);
    }
    private void updateGraphValues(int position) {

        LineData tempLineData=globalLinedata;

        List<LineDataSet> dataSets = tempLineData.getDataSets();
        List<String> labels = tempLineData.getXVals();

        LineData temp = new LineData(labels);
        temp.setHighlightEnabled(true);

        for (int index = 0; index < underdrawlGraphBoolean.length; index++) {
            if (index == position) {
                underdrawlGraphBoolean[index] = !underdrawlGraphBoolean[index];
                break;
            }
        }

        if(isEmpty()){
            resetGraphValues();
        }

        for (int index = 0; index < underdrawlGraphBoolean.length; index++) {
            if (underdrawlGraphBoolean[index]) {
                temp.addDataSet(dataSets.get(index));
            }
        }
        graph_underdrawl.setData(temp);
        graph_underdrawl.animateY(5000);

    }
    private void resetGraphValues() {
        for(int index=0;index<underdrawlGraphBoolean.length;index++){
            underdrawlGraphBoolean[index]=true;
            tv_legendarray.get(index).setBackgroundColor(Color.parseColor(colors_array[index]));
        }
    }
    private boolean isEmpty(){
        int counter=0;
        for(int index=0;index<underdrawlGraphBoolean.length;index++){
            if(underdrawlGraphBoolean[index]){
                counter=counter+1;
            }
        }
        if(counter>0){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        if(view == materialradiokw){
                modeSend="kw";
                try {
                    setUnderdrawl();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }
        if(view == materialradiokwh){
            modeSend="kwh";
            try {
                setUnderdrawl();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(view == materialradiokva){
            modeSend="kva";
            try {
                setUnderdrawl();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(view == dpResult){

            Calendar mcurrentDate=Calendar.getInstance();
            year=mcurrentDate.get(Calendar.YEAR);
            month=mcurrentDate.get(Calendar.MONTH);
            day=mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    year = selectedyear;
                    month = selectedmonth;
                    day = selectedday;

                    // set selected date into textview
                    dpResult.setText(new StringBuilder().append(day)
                            .append("-").append(month + 1).append("-").append(year)
                            .append(" "));

                    // set selected date into datepicker also
                    datesend = dpResult.getText().toString();
                    setUnderdrawl(null);
                }
            },year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();

        }
        if(view == viewgraph_underdrawl){
            Intent i=new Intent(getActivity(), Underdrawl_landscape_activity.class);
            i.putExtra("modeSend",modeSend);
            i.putExtra(AppConstants.APP_DATA,data.getGraphData().getAxisData());
            startActivity(i);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        final float min = 0;
        final float max = 54;
        final Random random = new Random();

        Observable.interval(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<Long, Float>() {
                    @Override
                    public Float call(Long aLong) {
                        return min + (random.nextFloat() * (max - min));
                    }
                })
                .subscribe(new Action1<Float>() {
                    @Override
                    public void call(Float value) {
                        meterView.setValue(value);
                    }
                });
    }

    private IDataPasser dataPasser=new IDataPasser() {

        @Override
        public void callApi(Object obj) {
            try {
                setUnderdrawl();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}
