
package com.smarttech.mittals.updatemodel.currentvoltage;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentVoltageData implements Parcelable {

    @SerializedName("meters")
    @Expose
    private Meters meters;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("graph_data")
    @Expose
    private GraphData graphData;

    public Meters getMeters() {
        return meters;
    }

    public void setMeters(Meters meters) {
        this.meters = meters;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public GraphData getGraphData() {
        return graphData;
    }

    public void setGraphData(GraphData graphData) {
        this.graphData = graphData;
    }


    protected CurrentVoltageData(Parcel in) {
        meters = (Meters) in.readValue(Meters.class.getClassLoader());
        mode = in.readString();
        graphData = (GraphData) in.readValue(GraphData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(meters);
        dest.writeString(mode);
        dest.writeValue(graphData);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CurrentVoltageData> CREATOR = new Parcelable.Creator<CurrentVoltageData>() {
        @Override
        public CurrentVoltageData createFromParcel(Parcel in) {
            return new CurrentVoltageData(in);
        }

        @Override
        public CurrentVoltageData[] newArray(int size) {
            return new CurrentVoltageData[size];
        }
    };
}