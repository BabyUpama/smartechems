package com.smarttech.mittals.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.smartech.mittals.invetech.R;


public class UserAccount extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);

        LinearLayout notification=(LinearLayout) findViewById(R.id.layout_notification);
        LinearLayout dashboard=(LinearLayout) findViewById(R.id.layout_dashboard);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),DashboardActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation,R.anim.animation2);
                finish();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),Notifications.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation,R.anim.animation2);
                finish();
            }
        });

    }


    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        //backButtonHandler();
        Intent in = new Intent(UserAccount.this, DashboardActivity.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }
}
