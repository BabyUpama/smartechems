
package com.smarttech.mittals.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value implements Parcelable{

    @SerializedName("menu")
    @Expose
    private List<Menu> menu = null;
    @SerializedName("value")
    @Expose
    private Value_ value;

    public List<Menu> getMenu() {
        return menu;
    }

    public void setMenu(List<Menu> menu) {
        this.menu = menu;
    }

    public Value_ getValue() {
        return value;
    }

    public void setValue(Value_ value) {
        this.value = value;
    }

    protected Value(Parcel in) {
        if (in.readByte() == 0x01) {
            menu = new ArrayList<Menu>();
            in.readList(menu, Menu.class.getClassLoader());
        } else {
            menu = null;
        }
        value = (Value_) in.readValue(Value_.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (menu == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(menu);
        }
        dest.writeValue(value);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Value> CREATOR = new Parcelable.Creator<Value>() {
        @Override
        public Value createFromParcel(Parcel in) {
            return new Value(in);
        }

        @Override
        public Value[] newArray(int size) {
            return new Value[size];
        }
    };

}
