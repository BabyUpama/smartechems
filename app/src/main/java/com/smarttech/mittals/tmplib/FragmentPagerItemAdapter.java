package com.smarttech.mittals.tmplib;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.collection.SparseArrayCompat;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.smarttech.mittals.utils.AppConstants;

import java.lang.ref.WeakReference;

/**
 * Created by upama on 18/1/17.
 */

public class FragmentPagerItemAdapter extends FragmentStatePagerAdapter {

    private final FragmentPagerItems pages;
    private final SparseArrayCompat<WeakReference<Fragment>> holder;
    private String fragmentBundle;

    public FragmentPagerItemAdapter(FragmentManager fm, FragmentPagerItems pages) {
        super(fm);
        this.pages = pages;
        this.holder = new SparseArrayCompat<>(pages.size());
    }

    public FragmentPagerItemAdapter(FragmentManager fm, FragmentPagerItems pages,String bundle){
        this(fm, pages);
        fragmentBundle=bundle;
    }

    public void updateBundle(String bundle){
        fragmentBundle=bundle;
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag=getPagerItem(position).instantiate(pages.getContext(), position);
        frag.getArguments().putString(AppConstants.DATA_CURRENTID,fragmentBundle);
        //AppLogger.showError("Arguments ",frag.getArguments()+"");
        return frag;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object item = super.instantiateItem(container, position);
        if (item instanceof Fragment) {
            holder.put(position, new WeakReference<Fragment>((Fragment) item));
        }
        return item;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        holder.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return getPagerItem(position).getTitle();
    }

    @Override
    public float getPageWidth(int position) {
        return super.getPageWidth(position);
    }

    public Fragment getPage(int position) {
        final WeakReference<Fragment> weakRefItem = holder.get(position);
        return (weakRefItem != null) ? weakRefItem.get() : null;
    }

    public FragmentPagerItem getPagerItem(int position) {
        return pages.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
