
package com.smarttech.mittals.model.powerprocured;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PowerProcureddata implements Parcelable {

    @SerializedName("meters")
    @Expose
    private List<Meter> meters = null;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("graph_data")
    @Expose
    private GraphData graphData;

    public List<Meter> getMeters() {
        return meters;
    }

    public void setMeters(List<Meter> meters) {
        this.meters = meters;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public GraphData getGraphData() {
        return graphData;
    }

    public void setGraphData(GraphData graphData) {
        this.graphData = graphData;
    }


    protected PowerProcureddata(Parcel in) {
        if (in.readByte() == 0x01) {
            meters = new ArrayList<Meter>();
            in.readList(meters, Meter.class.getClassLoader());
        } else {
            meters = null;
        }
        mode = in.readString();
        graphData = (GraphData) in.readValue(GraphData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (meters == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(meters);
        }
        dest.writeString(mode);
        dest.writeValue(graphData);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PowerProcureddata> CREATOR = new Parcelable.Creator<PowerProcureddata>() {
        @Override
        public PowerProcureddata createFromParcel(Parcel in) {
            return new PowerProcureddata(in);
        }

        @Override
        public PowerProcureddata[] newArray(int size) {
            return new PowerProcureddata[size];
        }
    };
}